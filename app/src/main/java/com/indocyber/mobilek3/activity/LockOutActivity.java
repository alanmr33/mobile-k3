package com.indocyber.mobilek3.activity;

import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.adapter.MessageLockoutAdapter;
import com.indocyber.mobilek3.lib.SwipeController;
import com.indocyber.mobilek3.model.Assignment;


public class LockOutActivity extends K3ProsedureActivity implements View.OnClickListener {

    Button button_sudah_dipahami;
    TextView text_jumlah_langkah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        button_sudah_dipahami = findViewById(R.id.button_sudah_dipahami);
        text_jumlah_langkah = findViewById(R.id.text_jumlah_langkah);
        button_sudah_dipahami.setOnClickListener(this);

        final Assignment assignment = getActiveAssignment();

        text_jumlah_langkah.setText("Total " + assignment.getDaftar_langkah().size() + " langkah");

        RecyclerView recyclerViewLockout = findViewById(R.id.recyclerViewLockout);
        recyclerViewLockout.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewLockout.setItemAnimator(new DefaultItemAnimator());
        final MessageLockoutAdapter adapter = new MessageLockoutAdapter(this, assignment.getDaftar_lockout());
        recyclerViewLockout.setAdapter(adapter);
        recyclerViewLockout.setNestedScrollingEnabled(false);

        /*final SwipeController swipeController = new SwipeController(new SwipeController.SwipeControllerActions() {

            @Override
            public void onSwiped(int position) {
                getDb().beginTransaction();
                assignment.getDaftar_lockout().get(position).setChecked(assignment.getDaftar_lockout().get(position).getChecked() == 0 ? 1 : 0);
                getDb().commitTransaction();
                adapter.notifyItemChanged(position);
                //Toast.makeText(LockOutActivity.this, "Swiped", Toast.LENGTH_SHORT).show();
            }
        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);*/
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(new ItemTouchHelper.Callback() {

            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                return makeMovementFlags(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                getDb().beginTransaction();
                assignment.getDaftar_lockout().get(position).setChecked(assignment.getDaftar_lockout().get(position).getChecked() == 0 ? 1 : 0);
                getDb().commitTransaction();
                adapter.notifyItemChanged(position);
            }

        });

        if (assignment.isCompleted()) {
            button_sudah_dipahami.setText("Selajutnya");
        } else {
            itemTouchhelper.attachToRecyclerView(recyclerViewLockout);
            button_sudah_dipahami.setText("Sudah Dipahami");
        }

        /*recyclerViewLockout.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });*/
    }

    @Override
    public int appLayout() {
        return R.layout.activity_proses_aplikasi_lockout;
    }

    @Override
    public String appTitle() {
        return "Proses Aplikasi Lockout";
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_sudah_dipahami:
                Intent main = new Intent(this, DetilPekerjaanActivity.class);
                startActivity(main);
                break;
        }
    }
}
