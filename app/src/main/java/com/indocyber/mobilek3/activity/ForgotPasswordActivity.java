package com.indocyber.mobilek3.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.config.Global;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForgotPasswordActivity extends K3Activity implements View.OnClickListener{
    private EditText textUsername;
    private TextView text_back_to_login;
    private Button buttonForgotPassword;
    private ProgressDialog loading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        textUsername=findViewById(R.id.editTextUsername);
        buttonForgotPassword=findViewById(R.id.buttonForgotPassword);
        buttonForgotPassword.setOnClickListener(this);
        text_back_to_login=findViewById(R.id.text_back_to_login);
        text_back_to_login.setOnClickListener(this);
        loading = new ProgressDialog(this);
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loading.setTitle("Lupa Kata Sandi");
        loading.setCancelable(false);
        loading.setMessage("Mengirim data email ke server..");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonForgotPassword :
                doForgotPassword();
                //DUMMY
                //dummyLogin();
                break;
            case R.id.text_back_to_login:
                finish();
                break;
        }
    }
    public void doForgotPassword(){
        String validation="";
        if(textUsername.getText().toString().equals("")){
            validation="Nama Pengguna Harus Diisi";
        }

        if(!validation.equals("")){
            AlertDialog dialog=new AlertDialog.Builder(this)
                    .setTitle(Global.DIALOG_TITLE)
                    .setMessage(validation)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create();
            dialog.show();
            return;
        }
        loading.show();
        getWebservice().requestNewPassword(textUsername.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    try {
                        String body=response.body().string();
                        final JSONObject object=new JSONObject(body);
                        AlertDialog dialog=new AlertDialog.Builder(ForgotPasswordActivity.this)
                                .setTitle(Global.DIALOG_TITLE)
                                .setMessage(object.getString("message"))
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        try{
                                            if(object.getBoolean("status")){
                                                finish();
                                            }
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                }).create();
                        dialog.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getBaseContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    loading.dismiss();
                    try {
                        String error=response.errorBody().string();
                        Log.e("ForgotPasswordActivity",error);
                        JSONObject object=new JSONObject(error);
                        if(object.getString("error").equals("invalid_grant")){
                            AlertDialog dialog=new AlertDialog.Builder(ForgotPasswordActivity.this)
                                    .setTitle(Global.DIALOG_TITLE)
                                    .setMessage("Nama User/Kata Sandi Tidak Sesuai")
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).create();
                            dialog.show();
                        }else{
                            AlertDialog dialog=new AlertDialog.Builder(ForgotPasswordActivity.this)
                                    .setTitle(Global.DIALOG_TITLE)
                                    .setMessage(object.getString("error"))
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).create();
                            dialog.show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getBaseContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                loading.dismiss();
                Toast.makeText(getBaseContext(),"Cek koneksi anda.",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
