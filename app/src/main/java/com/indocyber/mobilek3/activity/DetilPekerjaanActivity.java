package com.indocyber.mobilek3.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.adapter.DetilPekerjaanImageAdapter;
import com.indocyber.mobilek3.adapter.ToolAdapter;
import com.indocyber.mobilek3.lib.GridDividerDecoration;
import com.indocyber.mobilek3.lib.GridSpacingDecoration;
import com.indocyber.mobilek3.model.Assignment;
import com.indocyber.mobilek3.model.ToolSafety;
import com.indocyber.mobilek3.utils.DownloadUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class DetilPekerjaanActivity extends K3ProsedureActivity implements View.OnClickListener {

    Button button_selanjutnya;
    Button button_buku_panduan;
    Button button_preview_prosedur;
    EditText edit_judul_prosedur;
    EditText edit_aset;
    EditText edit_kode_prosedur;
    EditText edit_area;
    EditText edit_ditugaskan_oleh;
    EditText edit_hari;
    EditText edit_jam;

    Assignment assignment;

    DownloadUtil downloadUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        button_selanjutnya = findViewById(R.id.button_selanjutnya);
        button_buku_panduan = findViewById(R.id.button_buku_panduan);
        button_buku_panduan.setOnClickListener(this);
        button_preview_prosedur = findViewById(R.id.button_preview_prosedur);
        button_preview_prosedur.setOnClickListener(this);

        assignment = getActiveAssignment();
        if(Calendar.getInstance().getTimeInMillis()<(assignment.getTanggal_assign().getTime()-(60*60*1000))){
            button_selanjutnya.setText("Kembali");
        }
        button_selanjutnya.setOnClickListener(this);
        RecyclerView recyclerViewImage = findViewById(R.id.recyclerViewImage);
        recyclerViewImage.setLayoutManager(
                new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
        recyclerViewImage.setItemAnimator(new DefaultItemAnimator());
        DetilPekerjaanImageAdapter detilPekerjaanImageAdapter = new DetilPekerjaanImageAdapter(this, assignment.getDaftar_lampiran());
        recyclerViewImage.addItemDecoration(new GridSpacingDecoration(
                getResources().getDimensionPixelSize(R.dimen.card_insets),
                2));
        recyclerViewImage.setAdapter(detilPekerjaanImageAdapter);
        recyclerViewImage.setNestedScrollingEnabled(false);



        edit_judul_prosedur = findViewById(R.id.edit_judul_prosedur);
        edit_aset = findViewById(R.id.edit_aset);
        edit_kode_prosedur = findViewById(R.id.edit_kode_prosedur);
        edit_area = findViewById(R.id.edit_area);
        edit_ditugaskan_oleh = findViewById(R.id.edit_ditugaskan_oleh);
        edit_hari = findViewById(R.id.edit_hari);
        edit_jam = findViewById(R.id.edit_jam);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
        SimpleDateFormat olddateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        edit_judul_prosedur.setText(assignment.getJudul());
        edit_aset.setText(assignment.getInstalasi());
        edit_kode_prosedur.setText(assignment.getKode_prosedur());
        edit_area.setText(assignment.getArea());
        edit_ditugaskan_oleh.setText(assignment.getDitugaskan_oleh());
        edit_hari.setText(dateFormat.format(assignment.getTanggal_assign()));
        edit_jam.setText(timeFormat.format(assignment.getTanggal_assign()));
    }



    @Override
    public int appLayout() {
        return R.layout.activity_detil_pekerjaan;
    }

    @Override
    public String appTitle() {
        return "Detil Pekerjaan";
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_selanjutnya:
                if(Calendar.getInstance().getTimeInMillis()>(assignment.getTanggal_assign().getTime()-(60*60*1000))) {
                    Intent main = new Intent(this, ToolSafetyActivity.class);
                    startActivity(main);
                }else{
                    finish();
                }
                break;
            case R.id.button_buku_panduan:

                downloadUtil = new DownloadUtil(this);
                downloadUtil.enqueueDownload(assignment.getUrl_panduan(), assignment.getId() + ".pdf");

                //downloadUtil.registerBroadcastReciver();
                //downloadUtil.showDownload();
                break;
            case R.id.button_preview_prosedur:
                Intent preview = new Intent(this, DetailProsedurPreviewActivity.class);
                startActivity(preview);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        if (downloadUtil != null)
            downloadUtil.unregisterBroadcastReceiver();
        super.onDestroy();
    }
}
