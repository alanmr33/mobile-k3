package com.indocyber.mobilek3.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.adapter.AssignmentAdapter;
import com.indocyber.mobilek3.adapter.ToolAdapter;
import com.indocyber.mobilek3.model.Assignment;

import io.realm.RealmResults;


public class ToolActivity extends K3ProsedureActivity implements View.OnClickListener {

    Button button_selanjutnya;
    Assignment assignment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        button_selanjutnya = findViewById(R.id.button_selanjutnya);
        button_selanjutnya.setOnClickListener(this);

        assignment = getActiveAssignment();

        RecyclerView recyclerViewTools = findViewById(R.id.recyclerViewTools);
        recyclerViewTools.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewTools.setItemAnimator(new DefaultItemAnimator());
        ToolAdapter toolAdapter = new ToolAdapter(this, assignment.getDaftar_perlengkapan());
        toolAdapter.setEditable(!assignment.isCompleted());
        recyclerViewTools.setAdapter(toolAdapter);
        recyclerViewTools.setNestedScrollingEnabled(false);

    }

    @Override
    public int appLayout() {
        return R.layout.activity_perangkat_dan_kunci;
    }

    @Override
    public String appTitle() {
        return "Perangkat dan Kunci";
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_selanjutnya:
                if (assignment.getDaftar_perlengkapan().where().equalTo("sudah_digunakan", 0).findAll().size() > 0) {
                    //Toast.makeText(this, "Ada perangkat dan kunci yang belum dibawa",Toast.LENGTH_SHORT).show();
                    Snackbar.make(v, "Ada perangkat dan kunci yang belum dibawa", Snackbar.LENGTH_SHORT).show();
                } else {
                    Intent main = new Intent(this, PeringatanBahayaActivity.class);
                    startActivity(main);
                }
                break;
        }
    }
}