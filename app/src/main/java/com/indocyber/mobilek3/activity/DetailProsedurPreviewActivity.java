package com.indocyber.mobilek3.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.adapter.DetailProsedurPreviewAdapter;
import com.indocyber.mobilek3.model.Assignment;


public class DetailProsedurPreviewActivity extends K3ProsedureActivity {

    Assignment assignment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        assignment = getActiveAssignment();

        RecyclerView recyclerViewTools = findViewById(R.id.recyclerView);
        recyclerViewTools.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewTools.setItemAnimator(new DefaultItemAnimator());
        DetailProsedurPreviewAdapter adapter = new DetailProsedurPreviewAdapter(this, assignment.getDaftar_langkah());
        recyclerViewTools.setAdapter(adapter);

    }

    @Override
    public int appLayout() {
        return R.layout.activity_detail_prosedur_preview;
    }

    @Override
    public String appTitle() {
        return "Preview Prosedur";
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

}
