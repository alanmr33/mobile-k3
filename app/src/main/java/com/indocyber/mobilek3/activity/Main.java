package com.indocyber.mobilek3.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.adapter.AssignmentAdapter;
import com.indocyber.mobilek3.config.Global;
import com.indocyber.mobilek3.fragment.AssignmentCalendar;
import com.indocyber.mobilek3.fragment.CalendarFragment;
import com.indocyber.mobilek3.fragment.Dashboard;
import com.indocyber.mobilek3.fragment.DashboardFragment;
import com.indocyber.mobilek3.fragment.MyAssignments;
import com.indocyber.mobilek3.lib.MD5;
import com.indocyber.mobilek3.model.Assignment;
import com.indocyber.mobilek3.model.UserData;
import com.indocyber.mobilek3.utils.FrescoUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main extends K3Activity
        implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {
    private FragmentManager fragmentManager;
    private TextView textTitle,textUsername;
    private ImageView imgClose;
    private SimpleDraweeView imageViewPhoto;
    private TextView txt_notification;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        txt_notification=(TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.nav_assignment));
        View layoutHeader = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        textTitle = findViewById(R.id.textViewTitle);
        imgClose = layoutHeader.findViewById(R.id.imageViewClose);
        textUsername = layoutHeader.findViewById(R.id.textUsername);
        imageViewPhoto = layoutHeader.findViewById(R.id.imageViewPhoto);

        imageViewPhoto.setImageURI(getSession().getUserdata().getPhoto());
        int color = getResources().getColor(R.color.colorAccent);
        RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
        roundingParams.setBorder(color, 6.0f);
        roundingParams.setRoundAsCircle(true);
        imageViewPhoto.getHierarchy().setRoundingParams(roundingParams);

        Log.i("Photo",getSession().getUserdata().getPhoto());
        Log.i("Token",getSession().getToken());
        textUsername.setText(getSession().getUserdata().getUsername());
        imgClose.setOnClickListener(this);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.colorAccent));
        fragmentManager=getSupportFragmentManager();
        getData();
        initHome();
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        FirebaseMessaging.getInstance().subscribeToTopic("all");
        FirebaseMessaging.getInstance().subscribeToTopic(MD5.encrypt(getSession().getUserdata().getUser_id()));
    }
    public void getData(){
        deleteAssignmentBeforeToday();
        prefetchImageAllAssignment();
        final ProgressDialog loading = ProgressDialog.show(this, Global.DIALOG_TITLE,
                "Meminta Data Tugas Terbaru", true, false);
        getWebServiceWithToken().getAssignments().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loading.dismiss();
                if(response.isSuccessful()){
                    try {
                        String body=response.body().string();
                        Log.i("DATA",body);
                        JSONObject returnAPI=new JSONObject(body);
                        Realm realm= Realm.getDefaultInstance();
                        realm.beginTransaction();
                        for (int i=0;i<returnAPI.getJSONArray("data").length();i++){
                            try {
                                RealmQuery<Assignment> assignmentRealmQuery = realm
                                        .where(Assignment.class)
                                        .equalTo("id", returnAPI.getJSONArray("data").getJSONObject(i).getString("id"));
                                Assignment assignment = assignmentRealmQuery.findFirst();
                                if(assignment!=null){
                                    if(!assignment.isCompleted()){
                                        realm.createOrUpdateObjectFromJson(Assignment.class,returnAPI.getJSONArray("data").getJSONObject(i));
                                    }
                                }else{
                                    realm.createOrUpdateObjectFromJson(Assignment.class,returnAPI.getJSONArray("data").getJSONObject(i));
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                        realm.commitTransaction();
                        realm.close();
                        Toast.makeText(Main.this,"Anda memilik "+returnAPI.getJSONArray("data").length()
                        +" yang harus dikerjakan",Toast.LENGTH_SHORT).show();
                        prefetchImageAllAssignment();
                        RealmQuery<Assignment> assignmentRealmQuery = realm
                                .where(Assignment.class)
                                .equalTo("status", "COMPLETE");
                        RealmResults<Assignment> assignmentNotif = assignmentRealmQuery.findAll();
                        txt_notification.setGravity(Gravity.CENTER_VERTICAL);
                        txt_notification.setTypeface(null, Typeface.BOLD);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            txt_notification.setTextColor(getResources().getColor(R.color.colorWhite,null));
                        }else{
                            txt_notification.setTextColor(getResources().getColor(R.color.colorWhite));
                        }
                        txt_notification.setText(String.valueOf(assignmentNotif.size()));
                    }catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(getBaseContext(),"Terjadi Kesalahan",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    try {
                        String error=response.errorBody().string();
                        Log.e("Profile",error);
                        JSONObject object=new JSONObject(error);
                        if(object.getString("error").equals("invalid_token")) {
                            getSession().setLogin(false);
                            Intent login=new Intent(Main.this,Login.class);
                            startActivity(login);
                            finish();
                        }else {
                            AlertDialog dialog = new AlertDialog.Builder(Main.this)
                                    .setTitle(Global.DIALOG_TITLE)
                                    .setMessage(object.getString("error"))
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).create();
                            dialog.show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getBaseContext(),"Terjadi Kesalahan",Toast.LENGTH_SHORT).show();
                    }
                }
                //initHome();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(getBaseContext(),"Cek koneksi anda.",Toast.LENGTH_SHORT).show();
                //initHome();
            }
        });
    }
    private void initHome(){
         setActivePage(new DashboardFragment(),"Dashboard");
    }
    private void setActivePage(Fragment fragment,String title){
        try {
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
            textTitle.setText(title);
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id){
            case R.id.nav_assignment :
                setActivePage(new MyAssignments(),"Tugas Saya");
                break;
            case R.id.nav_dashboard_assignment :
                setActivePage(new DashboardFragment(),"Dashboard");
                break;
            case R.id.nav_dashboard :
                setActivePage(new Dashboard(),"Dashboard");
                break;

            case R.id.nav_calendar_assignment :
                setActivePage(new CalendarFragment(),"Kalender");
                break;
            case R.id.nav_logout :
                getSession().setLogin(false);
                Intent splash=new Intent(this,Splash.class);
                startActivity(splash);
                finish();
                break;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageViewClose :
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                break;
        }
    }

    private void deleteAssignmentBeforeToday()
    {
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar toDayCalendar=Calendar.getInstance();
        toDayCalendar.add(Calendar.HOUR,-24);
        String today=dateFormat.format(toDayCalendar.getTime());
        SimpleDateFormat dateFormatWhere=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Realm realm= Realm.getDefaultInstance();
        RealmQuery<Assignment> assignmentRealmQuery= null;
        try {
            realm.beginTransaction();
            assignmentRealmQuery = realm
                    .where(Assignment.class)
                    .equalTo("sending",false)
                    .and()
                    .lessThan("tanggal_assign", dateFormatWhere.parse(today))
                    .and()
                    .equalTo("user_id", getSession().getUserdata().getUser_id());
            assignmentRealmQuery.findAll().deleteAllFromRealm();
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prefetchImageAllAssignment()
    {
        RealmResults<Assignment> assignments = getDb().where(Assignment.class).findAll();
        new FrescoUtil().prefetchImageAllAssignment(getApplicationContext(), assignments);
    }
}
