package com.indocyber.mobilek3.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.adapter.MessageAdapter;
import com.indocyber.mobilek3.adapter.ToolAdapter;
import com.indocyber.mobilek3.dialog.AssignmentFinishAlertDialog;
import com.indocyber.mobilek3.dialog.DetilPekerjaanImageAlertDialog;
import com.indocyber.mobilek3.model.Assignment;

import java.util.Calendar;


public class PeringatanBahayaActivity extends K3ProsedureActivity implements View.OnClickListener {

    Button button_scan;
    Assignment assignment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        button_scan = findViewById(R.id.button_scan);
        button_scan.setOnClickListener(this);

        assignment = getActiveAssignment();

        RecyclerView recyclerViewPeringatanBahaya = findViewById(R.id.recyclerViewPeringatanBahaya);
        recyclerViewPeringatanBahaya.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewPeringatanBahaya.setItemAnimator(new DefaultItemAnimator());
        MessageAdapter adapter = new MessageAdapter(this, assignment.getDaftar_peringatan_bahaya());
        recyclerViewPeringatanBahaya.setAdapter(adapter);
        recyclerViewPeringatanBahaya.setNestedScrollingEnabled(false);
        if (assignment.isCompleted()) {
            button_scan.setText("Selanjutnya");
        } else if (assignment.getElektronik_id() == null) {
            button_scan.setText("Proses");
        } else {
            button_scan.setText("Scan untuk diproses");
        }
    }

    @Override
    public int appLayout() {
        return R.layout.activity_peringatan_bahaya;
    }

    @Override
    public String appTitle() {
        return "Peringatan Bahaya";
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_scan:
                if (assignment.isCompleted()) {
                    openAssignmentProcedure();
                }
                else if (assignment.getElektronik_id() != null) {
                    new IntentIntegrator(this).setCaptureActivity(CustomBarcodeScannerActivity.class).initiateScan();
                } else {
                    saveStartProcedure();
                    openAssignmentProcedure();
                }
    /*            Intent main = new Intent(this, DetailProsedurActivity.class);
                main.putExtra(DetailProsedurActivity.INDEX,0);
                startActivity(main);
    */            //AssignmentFinishAlertDialog a = AssignmentFinishAlertDialog.newInstance(this, assignment.getTanggal_mulai(), assignment.getTanggal_selesai());
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Log.e(this.getLocalClassName(), "Cancelled scan");
                Toast.makeText(this, "Cancelled scan", Toast.LENGTH_LONG).show();
            } else {
                Log.e(this.getLocalClassName(), "Scanned: " + result.getContents());
                checkScanResultValid(result.getContents());
            }
        } else {
            //super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private boolean checkScanResultValid(String qrcode) {

        Log.d(this.getLocalClassName(), assignment.getElektronik_id());
        if (qrcode.equalsIgnoreCase(assignment.getElektronik_id())) {
            saveStartProcedure();
            openAssignmentProcedure();
            return true;
        } else {
            Toast.makeText(this, "QR Code not valid", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private void saveStartProcedure() {
        getDb().beginTransaction();
        assignment.setTanggal_mulai(Calendar.getInstance().getTime());
        saveAssignment(getActiveAssignment());
        getDb().commitTransaction();
    }

    private void openAssignmentProcedure() {
        Intent main = new Intent(this, DetailProsedurActivity.class);
        main.putExtra(DetailProsedurActivity.INDEX, 0);
        startActivity(main);

    }
}
