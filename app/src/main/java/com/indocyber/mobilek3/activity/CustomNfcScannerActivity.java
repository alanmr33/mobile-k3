package com.indocyber.mobilek3.activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcV;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.common.util.Hex;
import com.google.gson.Gson;
import com.indocyber.mobilek3.lib.NfcVLib.DDMTag;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.UUID;

public class CustomNfcScannerActivity extends K3Activity {

    private NfcAdapter nfcAdapter;
    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            finish();
        }
        //readFromIntent(getIntent());

        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
    }

    private void readFromIntent(Intent intent) {
        String action = intent.getAction();
        Bundle b = intent.getExtras();
        String text = "";

        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            intent.getParcelableExtra(NfcAdapter.EXTRA_ID);

            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs = null;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            }
            buildTagViews(msgs);
        }

    }

    private void buildTagViews(NdefMessage[] msgs) {
        String text = "";
        for (NdefMessage ndefMessage : msgs) {
            text += "Ndef Message : " + new Gson().toJson(ndefMessage) + "\n";
        }
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (nfcAdapter != null) {
            nfcAdapter.disableForegroundDispatch(this);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        getTagInfo(intent);
    }

    private void getTagInfo(Intent intent) {
        try {
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String result = "";
            NfcV nfcvTag = NfcV.get(tag);
            byte[] tagID = nfcvTag.getTag().getId();
            Log.d("Hex ID", getHex(tagID));

            try {
                nfcvTag.connect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {


                // Get system information (0x2B)
                byte[] cmd = new byte[]{
                        (byte) 0x00, // Flags
                        (byte) 0x2B // Command: Get system information
                };
                byte[] systeminfo = nfcvTag.transceive(cmd);

                // Chop off the initial 0x00 byte:
                systeminfo = Arrays.copyOfRange(systeminfo, 1, 15);

                // Read first 8 blocks containing the 32 byte of userdata defined in the Danish model
                cmd = new byte[]{
                        (byte) 0x00, // Flags
                        (byte) 0x23, // Command: Read multiple blocks
                        (byte) 0x00, // First block (offset)
                        (byte) 0x08  // Number of blocks
                };
                byte[] userdata = nfcvTag.transceive(cmd);

                // Chop off the initial 0x00 byte:
                userdata = Arrays.copyOfRange(userdata, 1, 32);

                // Parse the data using the DDMTag class:
                DDMTag danishTag = new DDMTag();
                danishTag.addSystemInformation(systeminfo);
                danishTag.addUserData(userdata);
                result += "Barcode : " + danishTag.Barcode();
                Toast.makeText(this, result, Toast.LENGTH_LONG).show();

            } catch (Exception ex) {
                ex.printStackTrace();
            }

            //new NfcVReaderTask().execute(tag);


            NfcV nfcV = NfcV.get(tag);
            NdefFormatable ndefFormatable = NdefFormatable.get(tag);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int x = 0; x < bytes.length; x++) {
            int bit = bytes[x] & 0xFF;
            if (bit < 0x10) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(bit));
            sb.append(" ");
        }
        return sb.toString();
    }

    private String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("0x");
        if (src == null || src.length <= 0) {
            return null;
        }

        char[] buffer = new char[2];
        for (int i = 0; i < src.length; i++) {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
            System.out.println(buffer);
            stringBuilder.append(buffer);
        }

        return stringBuilder.toString();
    }

    private class NfcVReaderTask extends AsyncTask<Tag, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Tag... params) {
            String result = "";
            Tag tag = params[0];
            NfcV nfcvTag = NfcV.get(tag);
            byte[] tagID = nfcvTag.getTag().getId();
            Log.d("Hex ID", getHex(tagID));

            try {
                nfcvTag.connect();
                int offset = 0;
                int blocks = 19;
                byte[] id = nfcvTag.getTag().getId();
                //READ ALL BYTES
                byte[] cmd = new byte[]{
                        (byte) 0x20,
                        (byte) 0x2B,
                        (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00
                };
                //READ RANGE
//                byte[] cmd = new byte[]{
//                        (byte)0x22,
//                        (byte)0x23,
//                        (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00 ,
//                        (byte) 0x00,
//                        (byte) 0x07
//                };
                System.arraycopy(id, 0, cmd, 2, 8);
                byte[] response = nfcvTag.transceive(cmd);

                Log.i("RFID 1", new String(response, StandardCharsets.UTF_16));
                Log.i("RFID 2", new String(response, StandardCharsets.UTF_16BE));
                Log.i("RFID 3", new String(response, StandardCharsets.UTF_16LE));
                Log.i("RFID 4", new String(response, StandardCharsets.ISO_8859_1));
                Log.i("RFID 5", new String(response, StandardCharsets.US_ASCII));
                Log.i("RFID 6", new String(response, StandardCharsets.UTF_8));
                Log.i("HEX", getHex(response));
                nfcvTag.close();
                Bitmap bitmap = BitmapFactory.decodeByteArray(response, 0, response.length);
                return bitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {


                // Get system information (0x2B)
                byte[] cmd = new byte[]{
                        (byte) 0x00, // Flags
                        (byte) 0x2B // Command: Get system information
                };
                byte[] systeminfo = nfcvTag.transceive(cmd);

                // Chop off the initial 0x00 byte:
                systeminfo = Arrays.copyOfRange(systeminfo, 1, 15);

                // Read first 8 blocks containing the 32 byte of userdata defined in the Danish model
                cmd = new byte[]{
                        (byte) 0x00, // Flags
                        (byte) 0x23, // Command: Read multiple blocks
                        (byte) 0x00, // First block (offset)
                        (byte) 0x08  // Number of blocks
                };
                byte[] userdata = nfcvTag.transceive(cmd);

                // Chop off the initial 0x00 byte:
                userdata = Arrays.copyOfRange(userdata, 1, 32);

                // Parse the data using the DDMTag class:
                DDMTag danishTag = new DDMTag();
                danishTag.addSystemInformation(systeminfo);
                danishTag.addUserData(userdata);
                result += "Barcode : " + danishTag.Barcode();

            } catch (Exception ex) {
                ex.printStackTrace();
            }


            return null;//result;//Arrays.toString(tagID);
        }


        private String getHex(byte[] bytes) {
            StringBuilder sb = new StringBuilder();
            for (int x = 0; x < bytes.length; x++) {
                int bit = bytes[x] & 0xFF;
                if (bit < 0x10) {
                    sb.append('0');
                }
                sb.append(Integer.toHexString(bit));
                sb.append(" ");
            }
            return sb.toString();
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                ImageView view = new ImageView(getBaseContext());
                view.setImageBitmap(result);
                CustomNfcScannerActivity.this.setContentView(view);
            }
        }
    }

    public static byte[] getByte(String str) {
        int length = str.length();
        byte[] bArr = new byte[(length / 2)];
        for (int i = 0; i < length; i += 2) {
            bArr[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
        }
        return bArr;
    }
}
