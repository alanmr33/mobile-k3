package com.indocyber.mobilek3.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.indocyber.mobilek3.R;

public class Splash extends K3Activity {
    private final static int INITIAL_REQUEST = 12;
    private static final String[] INITIAL_PERMS = {
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION   ,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (!hasPermissions(this, INITIAL_PERMS)) {
            ActivityCompat.requestPermissions(this, INITIAL_PERMS, INITIAL_REQUEST);
        }else{
            initSplash();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case INITIAL_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0) {
                    for(int i=0;i<INITIAL_PERMS.length;i++){
                        Log.v("REQUESTEXT","Permission: "+permissions[i]+ "was "+grantResults[i]);
                        if(grantResults[i] == PackageManager.PERMISSION_GRANTED){
                            Log.d("REQUESTEXT","true");
                        }else{
                            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                            Log.d("REQUESTEXT","false");
                        }
                    }
                    if(grantResults.length==permissions.length){
                       initSplash();
                    }else{
                        Toast.makeText(this,"Cek permission aplikasi.",Toast.LENGTH_SHORT).show();
                    }
                }
                return;
            }
        }
    }
    public void initSplash(){
       new Handler().postDelayed(new Runnable() {
           @Override
           public void run() {



              if(getSession().isLogin()){
                  Intent i=new Intent(Splash.this,Main.class);
                  startActivity(i);
                  finish();
              }else{
                  Intent i=new Intent(Splash.this,Login.class);
                  startActivity(i);
                  finish();
              }
           }
       },3000);
    }
}
