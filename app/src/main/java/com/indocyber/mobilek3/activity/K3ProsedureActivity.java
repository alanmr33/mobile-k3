package com.indocyber.mobilek3.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.model.Assignment;

public abstract class K3ProsedureActivity extends K3Activity {

    public static final String ASSIGNMENT_ID = "assignment_id";
    private ImageView imageViewBack;
    private TextView textViewTitle;
    public abstract int appLayout();
    public abstract String appTitle();

    private Assignment activeAssignment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(appLayout());
        imageViewBack=findViewById(R.id.imageViewBack);
        textViewTitle=findViewById(R.id.textViewTitle);
        textViewTitle.setText(appTitle());
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setAssignmentId();
    }

    private void setAssignmentId()
    {
        String assignmentId = getSession().getActiveAssignmentId(); //getIntent().getStringExtra(K3ProsedureActivity.ASSIGNMENT_ID);
        if(TextUtils.isEmpty(assignmentId) == false)
        {
            getSession().setActiveAssignmentId(assignmentId);
            Assignment assignment = getDb().where(Assignment.class).equalTo("id", assignmentId).findFirst();
            setActiveAssignment(assignment);
        }
    }

    public Assignment getActiveAssignment() {
        return activeAssignment;
    }

    public void setActiveAssignment(Assignment activeAssignment) {
        this.activeAssignment = activeAssignment;
    }

    public void saveAssignment(Assignment assignment)
    {
        //app.getRealm().beginTransaction();
        getDb().insertOrUpdate(assignment);
        //app.getRealm().commitTransaction();
    }
}
