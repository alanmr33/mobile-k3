package com.indocyber.mobilek3.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.indocyber.mobilek3.App;
import com.indocyber.mobilek3.model.Assignment;
import com.indocyber.mobilek3.utils.Session;
import com.indocyber.mobilek3.utils.WebService;

import io.realm.Realm;

public class K3Activity extends AppCompatActivity {
    private App app;
    private Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app=((App)getApplication());
        session=app.getSession();
        if(session.isLogin()){
            app.checkTask();
        }
    }

    public App getApp() {
        return app;
    }
    public Realm getDb(){
        return app.getRealm();
    }
    public WebService getWebservice(){
        return app.getWebService();
    }
    public WebService getWebServiceWithToken(){
        return app.getWebServiceWithToken();
    }

    public Session getSession() {
        return session;
    }



    @Override
    public void onBackPressed() {

    }
}
