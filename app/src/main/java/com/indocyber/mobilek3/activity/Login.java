package com.indocyber.mobilek3.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.info.ForwardingImageOriginListener;
import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.config.Global;
import com.indocyber.mobilek3.model.Assignment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends K3Activity implements View.OnClickListener{
    private EditText textUsername,textPassword;
    private TextView text_lupa_kata_sandi;
    private Button btnLogin;
    private CheckBox checkBoxPassword;
    private  ProgressDialog loading;
    private TextView text_version;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        textUsername=findViewById(R.id.editTextUsername);
        text_version=findViewById(R.id.text_version);
        textPassword=findViewById(R.id.editTextPassword);
        checkBoxPassword=findViewById(R.id.checkbox_lihat_kata_sandi);
        checkBoxPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    textPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else{
                    textPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    textPassword.setSelection(textPassword.length());
                }
            }
        });
        btnLogin=findViewById(R.id.buttonLogin);
        btnLogin.setOnClickListener(this);
        text_lupa_kata_sandi=findViewById(R.id.text_lupa_kata_sandi);
        text_lupa_kata_sandi.setOnClickListener(this);
        loading = new ProgressDialog(this);
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loading.setTitle("Login");
        loading.setCancelable(false);
        loading.setMessage("Mengirim data login ke server..");
        try {
            PackageInfo info=getPackageManager().getPackageInfo(getPackageName(), 0);
            text_version.setText("Versi "+info.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonLogin :
                doLogin();
                //DUMMY
                //dummyLogin();
                break;
            case R.id.text_lupa_kata_sandi:
                Intent intent=new Intent(getBaseContext(),ForgotPasswordActivity.class);
                startActivity(intent);
                break;
        }
    }
    public void doLogin(){
        String validation="";
        if(textUsername.getText().toString().equals("")){
             validation="Nama Pengguna Harus Diisi";
        }
        if(!textUsername.getText().toString().equals("") && textPassword.getText().toString().equals("")){
            validation="Kata Sandi Harus Diisi";
        }
        if(!validation.equals("")){
            AlertDialog dialog=new AlertDialog.Builder(this)
                    .setTitle(Global.DIALOG_TITLE)
                    .setMessage(validation)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create();
            dialog.show();
            return;
        }
        loading.show();
        getWebservice().doLogin(textUsername.getText().toString(),
                textPassword.getText().toString(),
                "password").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    try {
                        String body=response.body().string();
                        JSONObject object=new JSONObject(body);
                        getSession().setToken(object.getString("access_token"));
                        getProfile();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getBaseContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    loading.dismiss();
                    try {
                        String error=response.errorBody().string();
                        Log.e("Login",error);
                        JSONObject object=new JSONObject(error);
                        if(object.getString("error").equals("invalid_grant")){
                            AlertDialog dialog=new AlertDialog.Builder(Login.this)
                                    .setTitle(Global.DIALOG_TITLE)
                                    .setMessage("Nama User/Kata Sandi Tidak Sesuai")
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).create();
                            dialog.show();
                        }else{
                            AlertDialog dialog=new AlertDialog.Builder(Login.this)
                                    .setTitle(Global.DIALOG_TITLE)
                                    .setMessage(object.getString("error"))
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).create();
                            dialog.show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getBaseContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
               loading.dismiss();
                Toast.makeText(getBaseContext(),"Cek koneksi anda.",Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void getProfile(){
        getWebServiceWithToken().getProfile().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
               loading.dismiss();
               if(response.isSuccessful()){
                   try {
                       String body=response.body().string();
                       getSession().setUserData(body);
                       getSession().setLogin(true);
                       Intent main=new Intent(getBaseContext(),Main.class);
                       startActivity(main);
                       finish();
                   } catch (Exception e) {
                       e.printStackTrace();
                       Toast.makeText(getBaseContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                   }
               }else{
                   try {
                       String error=response.errorBody().string();
                       Log.e("Profile",error);
                       JSONObject object=new JSONObject(error);
                       if(object.getString("error").equals("invalid_grant")){
                           AlertDialog dialog=new AlertDialog.Builder(Login.this)
                                   .setTitle(Global.DIALOG_TITLE)
                                   .setMessage("Nama User/Kata Sandi Tidak Sesuai")
                                   .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                       @Override
                                       public void onClick(DialogInterface dialog, int which) {
                                           dialog.dismiss();
                                       }
                                   }).create();
                           dialog.show();
                       }else{
                           AlertDialog dialog=new AlertDialog.Builder(Login.this)
                                   .setTitle(Global.DIALOG_TITLE)
                                   .setMessage(object.getString("error"))
                                   .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                       @Override
                                       public void onClick(DialogInterface dialog, int which) {
                                           dialog.dismiss();
                                       }
                                   }).create();
                           dialog.show();
                       }
                   } catch (Exception e) {
                       e.printStackTrace();
                       Toast.makeText(getBaseContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                   }
               }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(getBaseContext(),"Cek koneksi anda.",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
