package com.indocyber.mobilek3.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.config.Global;
import com.indocyber.mobilek3.dialog.AssignmentFinishAlertDialog;
import com.indocyber.mobilek3.lib.OnGestureRegisterListener;
import com.indocyber.mobilek3.model.Assignment;
import com.indocyber.mobilek3.model.AssignmentSubmit;
import com.indocyber.mobilek3.model.DetailPekerjaan;
import com.indocyber.mobilek3.model.Step;
import com.indocyber.mobilek3.model.StepSubmit;
import com.indocyber.mobilek3.utils.AnimUtil;
import com.indocyber.mobilek3.utils.GPSTracker;
import com.indocyber.mobilek3.utils.WorkerUtil;
import com.indocyber.mobilek3.worker.SendWorker;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

//import ss.com.bannerslider.Slider;
import androidx.work.BackoffPolicy;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import id.zelory.compressor.Compressor;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
//import ss.com.bannerslider.event.OnSlideClickListener;
import ss.com.bannerslider.views.BannerSlider;

public class DetailProsedurActivity extends K3ProsedureActivity implements View.OnClickListener {

    public static final String INDEX = "INDEX";
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_TAKE_PHOTO = 2;

    String mCurrentPhotoPath;

    Button buttonTitikIsolasiBerikutnya;
    ViewGroup layoutProsedurSelesai;
    ScrollView layoutAll;
    ImageView imageViewProsedur, imageViewSelesai, imageViewBack;
    TextView textViewLangkah, textViewProsedur, textViewPerangkat, textViewTitle;

    Assignment assignment;
    Step step;
    int index = 0;

    GPSTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            imageViewBack=findViewById(R.id.imageViewBack);
            imageViewBack.setOnClickListener(this);
            index = getIntent().getIntExtra(DetailProsedurActivity.INDEX, 0);
            layoutAll = findViewById(R.id.layoutAll);
            layoutAll.setOnTouchListener(new OnGestureRegisterListener(this) {
                @Override
                public void onSwipeRight(View view) {

                }

                @Override
                public void onSwipeLeft(View view) {


                }

                @Override
                public void onSwipeBottom(View view) {
                    imageViewBack.performClick();
                }

                @Override
                public void onSwipeTop(View view) {

                    if(buttonTitikIsolasiBerikutnya.isEnabled())
                    {
                        buttonTitikIsolasiBerikutnya.performClick();
                    }
                }


            });
            buttonTitikIsolasiBerikutnya = findViewById(R.id.buttonTitikIsolasiBerikutnya);
            buttonTitikIsolasiBerikutnya.setOnClickListener(this);
            layoutProsedurSelesai = findViewById(R.id.layoutProsedurSelesai);
            layoutProsedurSelesai.setOnClickListener(this);
            imageViewSelesai = findViewById(R.id.imageViewSelesai);
            imageViewProsedur = findViewById(R.id.imageViewProsedur);
            imageViewProsedur.setOnClickListener(this);

            textViewLangkah = findViewById(R.id.textViewLangkah);
            textViewProsedur = findViewById(R.id.textViewProsedur);
            textViewPerangkat = findViewById(R.id.textViewPerangkat);
            //textViewNoKunci = findViewById(R.id.textViewNoKunci);
            textViewTitle = findViewById(R.id.textViewTitle);

            assignment = getActiveAssignment();

            step = assignment.getDaftar_langkah().get(index);

            textViewTitle.setText(step.getNama_pekerjaan());
            textViewLangkah.setText("LANGKAH " + step.getUrutan() + " dari " + assignment.getDaftar_langkah().size());
            textViewProsedur.setText(step.getDeskripsi_pekerjaan());

            textViewPerangkat.setText(Html.fromHtml(step.getPerangkat()));
            Log.d(this.getLocalClassName(), step.getPerangkat());
            if (index + 1 == assignment.getDaftar_langkah().size()) {
                buttonTitikIsolasiBerikutnya.setText("SELESAI");
            } else {
                buttonTitikIsolasiBerikutnya.setText("TITIK ISOLASI BERIKUTNYA");
            }


            if (TextUtils.isEmpty(step.getBukti_pekerjaan()) == false) {
                byte[] imageBytes = Base64.decode(step.getBukti_pekerjaan(), Base64.DEFAULT);
                Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                imageViewProsedur.setImageBitmap(decodedImage);
            }

            //        final Slider sliderCarousel = findViewById(R.id.sliderCarousel);
            //      sliderCarousel.setAdapter(new ProsedurSliderAdapter(this, step.getGambar()));
            BannerSlider bannerSlider = findViewById(R.id.sliderCarousel);
            List<Banner> banners = new ArrayList<>();
            for (DetailPekerjaan detailPekerjaan : step.getGambar()) {
                banners.add(new RemoteBanner(detailPekerjaan.getUrl_gambar()));
            }

            bannerSlider.setBanners(banners);
            bannerSlider.setMustAnimateIndicators(false);


            setStatusProsedurSelesai();
            if(assignment.isCompleted() == false)
            {
                if (step.getStart_date() == null) {
                    getDb().beginTransaction();
                    step.setStart_date(Calendar.getInstance().getTime());
                    saveAssignment(getActiveAssignment());
                    getDb().commitTransaction();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int appLayout() {
        return R.layout.activity_detail_prosedur;
    }

    @Override
    public String appTitle() {
        return "Title";
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        //saveProsedurLocation();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewBack:
                finish();
                AnimUtil.closeActivityFromBottom(this);
                break;
            case R.id.buttonTitikIsolasiBerikutnya:
                try {

                    final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    if (index + 1 == assignment.getDaftar_langkah().size()) {
                        if(assignment.isCompleted())
                        {
                            Intent main = new Intent(this, Main.class);
                            main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(main);
                        }
                        else {
                            getDb().beginTransaction();
                            if (step.getEnd_date() == null) {
                                step.setEnd_date(Calendar.getInstance().getTime());
                            }
                            assignment.setSending(true);
                            assignment.setTanggal_selesai(Calendar.getInstance().getTime());
                            assignment.setStatus("COMPLETE");
                            saveAssignment(getActiveAssignment());
                            getDb().commitTransaction();
                            AssignmentFinishAlertDialog a = AssignmentFinishAlertDialog
                                    .newInstance(DetailProsedurActivity.this,
                                            assignment,
                                            dateFormat.format(assignment.getTanggal_mulai())
                                            , dateFormat.format(assignment.getTanggal_selesai()));
                        }
                    } else {
                        if(!assignment.isCompleted()) {
                            getDb().beginTransaction();
                            if (step.getEnd_date() == null) {
                                step.setEnd_date(Calendar.getInstance().getTime());
                            }
                            saveAssignment(getActiveAssignment());
                            getDb().commitTransaction();
                            Log.e("SendWorker", "Start : "+step.getStart_date());
                            Log.e("SendWorker", "End : "+step.getEnd_date());

                        }
                        Intent main = new Intent(this, DetailProsedurActivity.class);
                        main.putExtra(DetailProsedurActivity.INDEX, index + 1);
                        startActivity(main);
                        AnimUtil.openActivityFromBottom(this);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            break;
            case R.id.layoutProsedurSelesai:
                if(assignment.isCompleted()) {
                    return;
                }
                if(TextUtils.isEmpty( step.getBukti_pekerjaan()))
                {
                    Toast.makeText(getBaseContext(),"Ambil foto terlebih dahulu",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(saveProsedurLocation() == false)
                {
                    Toast.makeText(getBaseContext(),"Location not found, please try again",Toast.LENGTH_SHORT).show();
                    return;
                }
                getDb().beginTransaction();
                step.setSudah_dikerjakan(!step.getSudah_dikerjakan());
                saveAssignment(getActiveAssignment());
                getDb().commitTransaction();

                setStatusProsedurSelesai();
                break;
            case R.id.imageViewProsedur:
                if(!assignment.isCompleted()) {
                    dispatchTakePictureIntent();
                }
                break;
        }
    }

    private void setStatusProsedurSelesai() {
        if (step.getSudah_dikerjakan()) {
            layoutProsedurSelesai.setBackgroundColor(getResources().getColor(R.color.colorBlueLighter));
            imageViewSelesai.setImageResource(R.drawable.icon_tick2);
            buttonTitikIsolasiBerikutnya.setEnabled(true);
        } else {
            layoutProsedurSelesai.setBackgroundColor(getResources().getColor(R.color.colorBackgroundGrayLighter));
            imageViewSelesai.setImageResource(R.drawable.circle_procedure);
            buttonTitikIsolasiBerikutnya.setEnabled(false);
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.indocyber.mobilek3.lib.ImageProvider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void setPic() {

        //File compressImage = compressImage(new File(mCurrentPhotoPath));
        //mCurrentPhotoPath = compressImage.getAbsolutePath();

        // Get the dimensions of the View
        int targetW = imageViewProsedur.getWidth();
        int targetH = imageViewProsedur.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);


        imageViewProsedur.setImageBitmap(bitmap);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 70, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        getDb().beginTransaction();
        step.setBukti_pekerjaan(encoded);
        saveAssignment(getActiveAssignment());
        getDb().commitTransaction();
    }

    private boolean saveProsedurLocation()
    {
        return true;
        /*boolean result = false;
        if(gpsTracker == null)
        {
            gpsTracker = new GPSTracker(this);
        }
        else
        {
            gpsTracker.getLocation();
        }
        if(gpsTracker.canGetLocation())
        {
            getDb().beginTransaction();
            step.setLatitude(gpsTracker.getLatitude());
            step.setLongitude(gpsTracker.getLongitude());
            Log.d(this.getLocalClassName(), gpsTracker.getLatitude() +", "+gpsTracker.getLongitude());
            saveAssignment(getActiveAssignment());
            getDb().commitTransaction();
        }
        if(gpsTracker.canGetLocation() == false || (gpsTracker.getLatitude() == 0.0 && gpsTracker.getLongitude() == 0.0))
        {
            result = false;
        }
        else
        {
            result = true;
        }
        return  result;
        */
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageViewProsedur.setImageBitmap(imageBitmap);
        } else if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            //galleryAddPic();
            setPic();
        }
    }

    private File compressImage(File actualImage)
    {
        File compressedImage = null;
        try {
            compressedImage = new Compressor(this)
                    .setMaxWidth(640)
                    .setMaxHeight(640)
                    .setQuality(75)
                    .setCompressFormat(Bitmap.CompressFormat.PNG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .compressToFile(actualImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return compressedImage;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(gpsTracker != null) {
            gpsTracker.stopUsingGPS();
            gpsTracker = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
