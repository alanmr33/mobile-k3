package com.indocyber.mobilek3.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.indocyber.mobilek3.R;


public class ToolSafetyHolder extends RecyclerView.ViewHolder {
    public SimpleDraweeView image_tool;
    public TextView text_tool_title;
    public ViewGroup layout_right, layout_tool;
    public ImageView image_status;
    public ToolSafetyHolder(View itemView) {
        super(itemView);
        image_tool=itemView.findViewById(R.id.image_tool);
        text_tool_title=itemView.findViewById(R.id.text_tool_title);
        layout_right=itemView.findViewById(R.id.layout_right);
        layout_tool=itemView.findViewById(R.id.layout_tool);
        image_status=itemView.findViewById(R.id.image_status);
    }
}