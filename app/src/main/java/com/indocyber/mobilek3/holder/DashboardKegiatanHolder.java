package com.indocyber.mobilek3.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.indocyber.mobilek3.R;


public class DashboardKegiatanHolder extends RecyclerView.ViewHolder {

    public TextView textViewNamaAlatDetailKegiatan,textViewTerselesaikanDetailKegiatan;
    public PieChart chartDetailKegiatan;
    public DashboardKegiatanHolder(View itemView) {
        super(itemView);
        textViewNamaAlatDetailKegiatan=itemView.findViewById(R.id.textViewNamaAlatDetailKegiatan);
        chartDetailKegiatan=itemView.findViewById(R.id.chartDetailKegiatan);
        textViewTerselesaikanDetailKegiatan=itemView.findViewById(R.id.textViewTerselesaikanDetailKegiatan);
    }
}