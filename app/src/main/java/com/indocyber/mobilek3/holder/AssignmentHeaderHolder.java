package com.indocyber.mobilek3.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.indocyber.mobilek3.R;


public class AssignmentHeaderHolder extends RecyclerView.ViewHolder {
    public TextView textViewAssignmentHeader;
    public AssignmentHeaderHolder(View itemView) {
        super(itemView);
        textViewAssignmentHeader=itemView.findViewById(R.id.textViewAssignmentHeader);
    }
}