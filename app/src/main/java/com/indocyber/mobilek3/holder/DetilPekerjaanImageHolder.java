package com.indocyber.mobilek3.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.indocyber.mobilek3.R;


public class DetilPekerjaanImageHolder extends RecyclerView.ViewHolder {
    public SimpleDraweeView image_detil_assignment;
    public DetilPekerjaanImageHolder(View itemView) {
        super(itemView);
        image_detil_assignment=itemView.findViewById(R.id.image_detil_assignment);
    }
}