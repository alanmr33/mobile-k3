package com.indocyber.mobilek3.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.indocyber.mobilek3.R;

import org.sufficientlysecure.htmltextview.HtmlTextView;


public class MessageHolder extends RecyclerView.ViewHolder {

    public TextView textNumber;
    public HtmlTextView textMessage;

    public MessageHolder(View itemView) {
        super(itemView);
        textNumber=itemView.findViewById(R.id.textNumber);
        textMessage =itemView.findViewById(R.id.textMessage);
    }
}
