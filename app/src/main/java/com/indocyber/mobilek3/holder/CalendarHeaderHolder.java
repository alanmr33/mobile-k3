package com.indocyber.mobilek3.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.indocyber.mobilek3.R;


public class CalendarHeaderHolder extends RecyclerView.ViewHolder {

    public TextView textViewCalendarHeader;
    public CalendarHeaderHolder(View itemView) {
        super(itemView);
        textViewCalendarHeader=itemView.findViewById(R.id.textViewCalendarHeader);
    }
}