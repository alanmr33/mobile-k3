package com.indocyber.mobilek3.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.indocyber.mobilek3.R;

public class AssignmentHolder extends RecyclerView.ViewHolder {
    public LinearLayout layout_tugas,layout_waktu;
    public SimpleDraweeView image_aset;
    public ImageView image_status;
    public TextView text_tugas_pekerjaan,text_tugas_waktu,text_tugas_title,text_tugas_aset;
    public AssignmentHolder(View itemView) {
        super(itemView);
        layout_tugas=itemView.findViewById(R.id.layout_tugas);
        layout_waktu=itemView.findViewById(R.id.layout_waktu);
        image_aset=itemView.findViewById(R.id.image_aset);
        image_status=itemView.findViewById(R.id.image_status);
        text_tugas_pekerjaan=itemView.findViewById(R.id.text_tugas_pekerjaan);
        text_tugas_waktu=itemView.findViewById(R.id.text_tugas_waktu);
        text_tugas_title=itemView.findViewById(R.id.text_tugas_title);
        text_tugas_aset=itemView.findViewById(R.id.text_tugas_aset);
    }
}
