package com.indocyber.mobilek3.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.indocyber.mobilek3.R;


public class DetailProsedurPreviewHolder extends RecyclerView.ViewHolder {
    public SimpleDraweeView imageViewProsedur;
    public TextView textViewProsedur,textViewPerangkat, textViewTitleProsedur;
    public DetailProsedurPreviewHolder(View itemView) {
        super(itemView);
        imageViewProsedur=itemView.findViewById(R.id.imageViewProsedur);
        textViewTitleProsedur=itemView.findViewById(R.id.textViewTitleProsedur);
        textViewProsedur=itemView.findViewById(R.id.textViewProsedur);
        textViewPerangkat=itemView.findViewById(R.id.textViewPerangkat);
        //textViewNoKunci=itemView.findViewById(R.id.textViewNoKunci);
    }
}