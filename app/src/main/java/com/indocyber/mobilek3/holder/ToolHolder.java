package com.indocyber.mobilek3.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.indocyber.mobilek3.R;

public class ToolHolder extends RecyclerView.ViewHolder {
    public SimpleDraweeView image_tool;
    public TextView text_tool_title,text_tool_deskripsi, text_tool_jumlah;
    public ViewGroup layout_right, layout_tool;
    public ImageView image_status;
    public ToolHolder(View itemView) {
        super(itemView);
        image_tool=itemView.findViewById(R.id.image_tool);
        text_tool_title=itemView.findViewById(R.id.text_tool_title);
        text_tool_deskripsi=itemView.findViewById(R.id.text_tool_deskripsi);
        text_tool_jumlah=itemView.findViewById(R.id.text_tool_jumlah);
        layout_right=itemView.findViewById(R.id.layout_right);
        layout_tool=itemView.findViewById(R.id.layout_tool);
        image_status=itemView.findViewById(R.id.image_status);
    }
}