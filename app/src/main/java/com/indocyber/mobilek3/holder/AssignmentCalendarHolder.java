package com.indocyber.mobilek3.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indocyber.mobilek3.R;

public class AssignmentCalendarHolder extends RecyclerView.ViewHolder {
    public TextView textViewAset,textViewTitle,textViewDate;
    public LinearLayout layout_calendar;
    public AssignmentCalendarHolder(View itemView) {
        super(itemView);
        textViewAset=itemView.findViewById(R.id.textViewAset);
        textViewTitle=itemView.findViewById(R.id.textViewTitle);
        textViewDate=itemView.findViewById(R.id.textViewDate);
        layout_calendar=itemView.findViewById(R.id.layout_calendar);
    }
}
