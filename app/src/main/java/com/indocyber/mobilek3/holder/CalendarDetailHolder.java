package com.indocyber.mobilek3.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indocyber.mobilek3.R;

import org.w3c.dom.Text;


public class CalendarDetailHolder extends RecyclerView.ViewHolder {

    public ViewGroup layoutCalendarDetail,layoutCustom;
    public TextView textViewTanggal, textViewTotal;
    public CalendarDetailHolder(View itemView) {
        super(itemView);
        layoutCalendarDetail=itemView.findViewById(R.id.layoutCalendarDetail);
        textViewTanggal=itemView.findViewById(R.id.textViewTanggal);
        layoutCustom=itemView.findViewById(R.id.layoutCustom);
        textViewTotal=itemView.findViewById(R.id.textViewTotal);
    }
}