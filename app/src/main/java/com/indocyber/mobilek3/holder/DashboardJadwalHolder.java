package com.indocyber.mobilek3.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.indocyber.mobilek3.R;


public class DashboardJadwalHolder extends RecyclerView.ViewHolder {

    public TextView textViewDetailJadwalAset,textViewDetailJadwalTanggal, textViewDetailJadwalPekerja;
    public DashboardJadwalHolder(View itemView) {
        super(itemView);
        textViewDetailJadwalAset=itemView.findViewById(R.id.textViewDetailJadwalAset);
        textViewDetailJadwalTanggal=itemView.findViewById(R.id.textViewDetailJadwalTanggal);
        textViewDetailJadwalPekerja=itemView.findViewById(R.id.textViewDetailJadwalPekerja);
    }
}