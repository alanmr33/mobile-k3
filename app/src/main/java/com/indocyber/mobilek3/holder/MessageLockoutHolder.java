package com.indocyber.mobilek3.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indocyber.mobilek3.R;


public class MessageLockoutHolder extends RecyclerView.ViewHolder {

    public TextView textNumber, textMessage;
    public ViewGroup layoutMessage;

    public MessageLockoutHolder(View itemView) {
        super(itemView);
        textNumber=itemView.findViewById(R.id.textNumber);
        textMessage =itemView.findViewById(R.id.textMessage);
        layoutMessage =itemView.findViewById(R.id.layoutMessage);
    }
}