package com.indocyber.mobilek3.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.activity.K3ProsedureActivity;
import com.indocyber.mobilek3.holder.DetailProsedurPreviewHolder;
import com.indocyber.mobilek3.holder.ToolHolder;
import com.indocyber.mobilek3.model.Step;
import com.indocyber.mobilek3.model.Tool;

import io.realm.RealmList;


public class DetailProsedurPreviewAdapter extends RecyclerView.Adapter<DetailProsedurPreviewHolder> {
    private Activity activity;
    private RealmList<Step> steps;

    public DetailProsedurPreviewAdapter(Activity activity, RealmList<Step> steps) {
        this.activity = activity;
        this.steps = steps;
    }

    @NonNull
    @Override
    public DetailProsedurPreviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_detail_prosedur_preview, parent, false);

        DetailProsedurPreviewHolder holder = new DetailProsedurPreviewHolder(v);
        /*
        final int position = holder.getAdapterPosition();
        final Tool tool = tools.get(position);

        holder.layout_tool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((K3Activity)activity).getDb().beginTransaction();
                tool.setSudah_digunakan(tool.getSudah_digunakan() == 0 ? 1 : 0);
                ((K3Activity)activity).saveAssignment(((K3Activity)activity).getApp().getActiveAssignment());
                ((K3Activity)activity).getDb().commitTransaction();
                notifyItemChanged(position);
            }
        });
        */
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DetailProsedurPreviewHolder holder, final int position) {

        final Step step = steps.get(position);
        holder.textViewTitleProsedur.setText(step.getNama_pekerjaan());
        holder.textViewProsedur.setText(step.getDeskripsi_pekerjaan());
        holder.textViewPerangkat.setText(Html.fromHtml( step.getPerangkat()));
        Log.d(this.activity.getLocalClassName(), step.getPerangkat());
        if(step.getGambar().size() > 0)
        {
            holder.imageViewProsedur.setImageURI(step.getGambar().get(0).getUrl_gambar());
            Log.i("IMAGE",step.getGambar().get(0).getUrl_gambar());
        }else{
            Log.i("NO_IMAGE","No Image");
        }

    }

    @Override
    public int getItemCount() {
        return steps.size();
    }
}