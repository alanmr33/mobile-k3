package com.indocyber.mobilek3.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.dialog.AssignmentCalendarPreviewDialog;
import com.indocyber.mobilek3.holder.CalendarDetailHolder;
import com.indocyber.mobilek3.holder.CalendarHeaderHolder;
import com.indocyber.mobilek3.model.CalendarDetail;
import com.indocyber.mobilek3.model.CalendarHeader;

import java.util.List;



public class CalendarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int CALENDAR_HEADER = 0;
    private static final int CALENDAR_DETAIL = 1;

    private Activity activity;
    private List<CalendarDetail> calendarDetails;

    public CalendarAdapter(Activity activity, List<CalendarDetail> calendarDetails) {
        this.activity = activity;
        this.calendarDetails = calendarDetails;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == CALENDAR_HEADER)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calendar_header, parent, false);
            return new CalendarHeaderHolder(v);
        }
        else
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calendar_detail, parent, false);
            return new CalendarDetailHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if(viewHolder instanceof CalendarHeaderHolder)
        {
            CalendarHeaderHolder holder = (CalendarHeaderHolder)viewHolder;
            CalendarHeader calendarHeader = (CalendarHeader) calendarDetails.get(position);
            holder.textViewCalendarHeader.setText(calendarHeader.getNamaHari());
            if(calendarHeader.isLibur())
            {
                holder.textViewCalendarHeader.setBackgroundColor(activity.getResources().getColor(R.color.colorRed));
                holder.textViewCalendarHeader.setTextColor(activity.getResources().getColor(R.color.colorWhite));
            }
            else
            {
                holder.textViewCalendarHeader.setBackgroundColor(activity.getResources().getColor(R.color.colorBackgroundGrayLighter));
                holder.textViewCalendarHeader.setTextColor(activity.getResources().getColor(R.color.colorBlack));
            }
        }
        else{
            CalendarDetailHolder holder = (CalendarDetailHolder)viewHolder;
            final CalendarDetail calendarDetail = calendarDetails.get(position);
            if( calendarDetail.getTanggal() == 0)
            {
                holder.layoutCalendarDetail.setBackgroundColor(activity.getResources().getColor(R.color.colorBackgroundGrayLighter));
                holder.layoutCustom.setVisibility(View.INVISIBLE);
                holder.textViewTanggal.setVisibility(View.INVISIBLE);
            }
            else {
                holder.layoutCalendarDetail.setBackgroundColor(activity.getResources().getColor(R.color.colorBackground));
                holder.textViewTanggal.setText(""+calendarDetail.getTanggal());
                holder.textViewTotal.setText(""+calendarDetail.getTotal());
                if (calendarDetail.isLibur()) {
                    holder.textViewTanggal.setTextColor(activity.getResources().getColor(R.color.colorRed));
                } else {
                    holder.textViewTanggal.setTextColor(activity.getResources().getColor(R.color.colorGray));
                }
                if(calendarDetail.getTotal() == 0)
                {
                    holder.layoutCustom.setVisibility(View.INVISIBLE);
                }
                else
                {
                    holder.layoutCustom.setVisibility(View.VISIBLE);
                    holder.layoutCalendarDetail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AssignmentCalendarPreviewDialog a = AssignmentCalendarPreviewDialog.newInstance(activity, calendarDetail.getDate());
                        }
                    });
                }
            }
        }

    }

    @Override
    public int getItemCount() {
        return calendarDetails.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(calendarDetails.get(position) instanceof CalendarHeader) return CALENDAR_HEADER;
        else return CALENDAR_DETAIL;
    }
}