package com.indocyber.mobilek3.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.activity.K3ProsedureActivity;
import com.indocyber.mobilek3.activity.LockOutActivity;
import com.indocyber.mobilek3.activity.Main;
import com.indocyber.mobilek3.holder.AssignmentHeaderHolder;
import com.indocyber.mobilek3.holder.AssignmentHolder;
import com.indocyber.mobilek3.model.Assignment;
import com.indocyber.mobilek3.model.AssignmentHeader;
import com.indocyber.mobilek3.model.AssignmentHeaderProvider;

import java.text.SimpleDateFormat;
import java.util.List;

import io.realm.RealmResults;

public class AssignmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ASSIGNMENT_HEADER = 0;
    private static final int ASSIGNMENT = 1;

    private Activity activity;
    private List<AssignmentHeaderProvider> assignments;

    public AssignmentAdapter(Activity activity, List<AssignmentHeaderProvider> assignments) {
        this.activity = activity;
        this.assignments = assignments;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ASSIGNMENT_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_section_header_main_tugas, parent, false);
            return new AssignmentHeaderHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_tugas, parent, false);
            return new AssignmentHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof AssignmentHolder) {
            AssignmentHolder holder = (AssignmentHolder) viewHolder;
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
                final Assignment assignment = (Assignment) assignments.get(position);
                holder.text_tugas_aset.setText(assignment.getInstalasi());
                holder.text_tugas_pekerjaan.setText(assignment.getPekerjaan());
                holder.text_tugas_title.setText(assignment.getJudul());
                holder.image_aset.setImageURI(assignment.getGambar());
                holder.text_tugas_waktu.setText(dateFormat.format(assignment.getTanggal_assign()));
                if (assignment.getStatus().equals("COMPLETE")) {
                    holder.layout_tugas.setBackgroundColor(activity.getResources().getColor(R.color.colorBlueLighter));
                    holder.layout_waktu.setBackgroundColor(activity.getResources().getColor(R.color.colorBlueLighter));
                    holder.text_tugas_title.setTextColor(activity.getResources().getColor(R.color.colorWhite));
                    holder.text_tugas_pekerjaan.setTextColor(activity.getResources().getColor(R.color.colorAccent));
                    holder.text_tugas_aset.setTextColor(activity.getResources().getColor(R.color.colorWhite));
                    holder.image_status.setImageResource(R.drawable.icon_workdone);
                } else {
                    holder.layout_tugas.setBackgroundColor(activity.getResources().getColor(R.color.colorWhite));
                    holder.layout_waktu.setBackgroundColor(activity.getResources().getColor(R.color.colorAccent));
                    holder.text_tugas_title.setTextColor(activity.getResources().getColor(R.color.colorBlueLighter));
                    holder.text_tugas_pekerjaan.setTextColor(activity.getResources().getColor(R.color.colorAccent));
                    holder.text_tugas_aset.setTextColor(activity.getResources().getColor(R.color.colorBlack));
                    holder.image_status.setImageResource(R.drawable.icon_workonprogress);
                }
                holder.layout_tugas.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //((Main)activity).getApp().setActiveAssignment(assignment);
                        Intent intent = new Intent(activity, LockOutActivity.class);
                        //intent.putExtra(K3ProsedureActivity.ASSIGNMENT_ID, assignment.getId());
                        ((Main) activity).getSession().setActiveAssignmentId(assignment.getId());
                        activity.startActivity(intent);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            AssignmentHeader assignmentHeader = (AssignmentHeader) assignments.get(position);
            AssignmentHeaderHolder holder = (AssignmentHeaderHolder) viewHolder;
            holder.textViewAssignmentHeader.setText(assignmentHeader.getHeader());
        }
    }

    @Override
    public int getItemCount() {
        return assignments.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (assignments.get(position) instanceof AssignmentHeader) return ASSIGNMENT_HEADER;
        else return ASSIGNMENT;
    }
}
