package com.indocyber.mobilek3.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.holder.MessageHolder;
import com.indocyber.mobilek3.holder.MessageLockoutHolder;
import com.indocyber.mobilek3.model.MessageLockout;

import io.realm.RealmList;


public class MessageLockoutAdapter extends RecyclerView.Adapter<MessageLockoutHolder> {
    private Activity activity;
    private RealmList<MessageLockout> messageLockouts;

    public MessageLockoutAdapter(Activity activity, RealmList<MessageLockout> messageLockouts) {
        this.activity = activity;
        this.messageLockouts = messageLockouts;
    }

    @NonNull
    @Override
    public MessageLockoutHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_lockout, parent, false);

        MessageLockoutHolder holder = new MessageLockoutHolder(v);
        /*
        final int position = holder.getAdapterPosition();
        final Tool tool = tools.get(position);

        holder.layout_tool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((K3Activity)activity).getDb().beginTransaction();
                tool.setSudah_digunakan(tool.getSudah_digunakan() == 0 ? 1 : 0);
                ((K3Activity)activity).saveAssignment(((K3Activity)activity).getApp().getActiveAssignment());
                ((K3Activity)activity).getDb().commitTransaction();
                notifyItemChanged(position);
            }
        });
        */
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MessageLockoutHolder holder, final int position) {

        final MessageLockout message = messageLockouts.get(position);
        holder.textNumber.setText(message.getNo()+". ");
        holder.textMessage.setText(message.getMessage());
        if(message.getChecked() == 0) {
            holder.textNumber.setTextColor(activity.getResources().getColor(R.color.colorBlack));
            holder.textMessage.setTextColor(activity.getResources().getColor(R.color.colorBlack));
        }else
        {
            holder.textNumber.setTextColor(activity.getResources().getColor(R.color.colorBlueLighter));
            holder.textMessage.setTextColor(activity.getResources().getColor(R.color.colorBlueLighter));
        }

    }

    @Override
    public int getItemCount() {
        return messageLockouts.size();
    }
}
