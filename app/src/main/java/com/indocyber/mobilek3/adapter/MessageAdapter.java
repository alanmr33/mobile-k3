package com.indocyber.mobilek3.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.holder.MessageHolder;
import com.indocyber.mobilek3.model.Message;

import io.realm.RealmList;


public class MessageAdapter extends RecyclerView.Adapter<MessageHolder> {
    private Activity activity;
    private RealmList<Message> messages;

    public MessageAdapter(Activity activity, RealmList<Message> messages) {
        this.activity = activity;
        this.messages = messages;
    }

    @NonNull
    @Override
    public MessageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false);

        MessageHolder holder = new MessageHolder(v);
        /*
        final int position = holder.getAdapterPosition();
        final Tool tool = tools.get(position);

        holder.layout_tool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((K3Activity)activity).getDb().beginTransaction();
                tool.setSudah_digunakan(tool.getSudah_digunakan() == 0 ? 1 : 0);
                ((K3Activity)activity).saveAssignment(((K3Activity)activity).getApp().getActiveAssignment());
                ((K3Activity)activity).getDb().commitTransaction();
                notifyItemChanged(position);
            }
        });
        */
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MessageHolder holder, final int position) {

        final Message message = messages.get(position);
        holder.textNumber.setText(message.getNo()+". ");
        holder.textMessage.setHtml(message.getMessage());

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }
}

