package com.indocyber.mobilek3.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.holder.DashboardJadwalHolder;
import com.indocyber.mobilek3.holder.MessageLockoutHolder;
import com.indocyber.mobilek3.model.DashboardJadwal;
import com.indocyber.mobilek3.model.MessageLockout;

import java.util.List;

import io.realm.RealmList;


public class DashboardJadwalAdapter extends RecyclerView.Adapter<DashboardJadwalHolder> {
    private Activity activity;
    private List<DashboardJadwal> dashboardJadwals;

    public DashboardJadwalAdapter(Activity activity, List<DashboardJadwal> dashboardJadwals) {
        this.activity = activity;
        this.dashboardJadwals = dashboardJadwals;
    }

    @NonNull
    @Override
    public DashboardJadwalHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard_jadwal, parent, false);

        DashboardJadwalHolder holder = new DashboardJadwalHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DashboardJadwalHolder holder, final int position) {

        final DashboardJadwal dashboardJadwal = dashboardJadwals.get(position);
        holder.textViewDetailJadwalAset.setText(dashboardJadwal.getAsset());
        holder.textViewDetailJadwalTanggal.setText(dashboardJadwal.getTanggal());
        holder.textViewDetailJadwalPekerja.setText(dashboardJadwal.getDitugaskan_oleh());

    }

    @Override
    public int getItemCount() {
        return dashboardJadwals.size();
    }
}
