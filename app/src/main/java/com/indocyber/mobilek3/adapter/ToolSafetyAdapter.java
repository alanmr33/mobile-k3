package com.indocyber.mobilek3.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.activity.K3ProsedureActivity;
import com.indocyber.mobilek3.holder.ToolSafetyHolder;
import com.indocyber.mobilek3.model.ToolSafety;

import io.realm.RealmList;


public class ToolSafetyAdapter extends RecyclerView.Adapter<ToolSafetyHolder> {
    private Activity activity;
    private RealmList<ToolSafety> toolSafeties;
    private boolean editable = true;

    public ToolSafetyAdapter(Activity activity, RealmList<ToolSafety> toolSafeties) {
        this.activity = activity;
        this.toolSafeties = toolSafeties;
    }

    @NonNull
    @Override
    public ToolSafetyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_peralatan_keamanan, parent, false);
        ToolSafetyHolder holder = new ToolSafetyHolder(v);
        /*final int position = holder.getAdapterPosition();

        final ToolSafety toolSafety = toolSafeties.get(position);

        holder.layout_tool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((K3Activity)activity).getDb().beginTransaction();
                toolSafety.setSudah_digunakan(toolSafety.getSudah_digunakan() == 0 ? 1 : 0);
                ((K3Activity)activity).saveAssignment(((K3Activity)activity).getApp().getActiveAssignment());
                ((K3Activity)activity).getDb().commitTransaction();
                notifyItemChanged(position);
            }
        });
        */
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ToolSafetyHolder holder, final int position) {

        final ToolSafety toolSafety = toolSafeties.get(position);
        holder.text_tool_title.setText(toolSafety.getNama_perlengkapan());
        holder.image_tool.setImageURI(toolSafety.getUrl_gambar());
        if(toolSafety.getSudah_digunakan() == 1){
            holder.layout_right.setBackgroundColor(activity.getResources().getColor(R.color.colorBlueLighter));
            holder.layout_tool.setBackgroundColor(activity.getResources().getColor(R.color.colorBlueLighter));
            holder.text_tool_title.setTextColor(activity.getResources().getColor(R.color.colorWhite));
            holder.image_status.setImageResource(R.drawable.icon_tick2);
        }
        else
        {
            holder.layout_right.setBackgroundColor(activity.getResources().getColor(R.color.colorBackgroundGrayLighter));
            holder.layout_tool.setBackgroundColor(activity.getResources().getColor(R.color.colorWhite));
            holder.text_tool_title.setTextColor(activity.getResources().getColor(R.color.colorBlack));
            holder.image_status.setImageResource(R.drawable.circle_procedure);
        }
        if(editable) {
            holder.layout_tool.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ((K3ProsedureActivity) activity).getDb().beginTransaction();
                    toolSafety.setSudah_digunakan(toolSafety.getSudah_digunakan() == 0 ? 1 : 0);
                    ((K3ProsedureActivity) activity).saveAssignment(((K3ProsedureActivity) activity).getActiveAssignment());
                    ((K3ProsedureActivity) activity).getDb().commitTransaction();
                    notifyItemChanged(position);
                }
            });
        }

        //holder.layout_tool.setBackgroundColor(tool.getItemBackgroundColor());
        //holder.layout_right.setBackgroundColor(tool.getSideBackgroundColor());

    }

    @Override
    public int getItemCount() {
        return toolSafeties.size();
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }


}