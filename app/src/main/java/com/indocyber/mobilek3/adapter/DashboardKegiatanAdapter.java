package com.indocyber.mobilek3.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.holder.DashboardJadwalHolder;
import com.indocyber.mobilek3.holder.DashboardKegiatanHolder;
import com.indocyber.mobilek3.model.DashboardJadwal;
import com.indocyber.mobilek3.model.DashboardKegiatan;

import java.util.ArrayList;
import java.util.List;


public class DashboardKegiatanAdapter extends RecyclerView.Adapter<DashboardKegiatanHolder> {

    private int colorBase = R.color.colorBackgroundBlueLighter;
    private int[] colors = new int[] {
            R.color.colorBlueLighter,
            R.color.colorAccent,
            R.color.colorGray
    };

    private Activity activity;
    private List<DashboardKegiatan> dashboardKegiatans;

    public DashboardKegiatanAdapter(Activity activity, List<DashboardKegiatan> dashboardKegiatans) {
        this.activity = activity;
        this.dashboardKegiatans = dashboardKegiatans;
    }

    @NonNull
    @Override
    public DashboardKegiatanHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard_kegiatan, parent, false);

        DashboardKegiatanHolder holder = new DashboardKegiatanHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DashboardKegiatanHolder holder, final int position) {

        final DashboardKegiatan dashboardKegiatan = dashboardKegiatans.get(position);
        holder.textViewNamaAlatDetailKegiatan.setText(dashboardKegiatan.getNama_kegiatan());
        holder.textViewTerselesaikanDetailKegiatan.setText("Terselesaikan : "+dashboardKegiatan.getTerselesaikan());
        holder.chartDetailKegiatan.getDescription().setEnabled(false);
        holder.chartDetailKegiatan.setDrawCenterText(true);
        float total = (float)(dashboardKegiatan.getBelum_terselesaikan()+dashboardKegiatan.getTerselesaikan());
        float percentageTerselesaikan = (float)dashboardKegiatan.getTerselesaikan()*100/total;
        holder.chartDetailKegiatan.setCenterText(String.format("%.0f",percentageTerselesaikan)+"%");
        holder.chartDetailKegiatan.setCenterTextColor(activity.getResources().getColor(R.color.colorAccent));
        holder.chartDetailKegiatan.setCenterTextSize(14);
        holder.chartDetailKegiatan.getLegend().setEnabled(false);
        holder.chartDetailKegiatan.setHoleRadius(80);
        holder.chartDetailKegiatan.setDrawEntryLabels(false);

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();


        entries.add(new PieEntry(dashboardKegiatan.getTerselesaikan(),"Terselesaikan"));
        entries.add(new PieEntry(dashboardKegiatan.getBelum_terselesaikan(),"Belum Terselesaikan"));


        PieDataSet dataSet = new PieDataSet(entries, dashboardKegiatan.getNama_kegiatan());
        dataSet.setDrawValues(false);
        int[] colorsWithBase = new int[] { colors[position%colors.length], colorBase};
        dataSet.setColors(colorsWithBase,activity);


        PieData data = new PieData(dataSet);

        holder.chartDetailKegiatan.setData(data);


        holder.chartDetailKegiatan.invalidate();

    }

    @Override
    public int getItemCount() {
        return dashboardKegiatans.size();
    }
}