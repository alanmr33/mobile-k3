package com.indocyber.mobilek3.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.activity.K3ProsedureActivity;
import com.indocyber.mobilek3.holder.ToolHolder;
import com.indocyber.mobilek3.model.Tool;
import io.realm.RealmList;


public class ToolAdapter extends RecyclerView.Adapter<ToolHolder> {
    private Activity activity;
    private RealmList<Tool> tools;
    private boolean editable = true;

    public ToolAdapter(Activity activity, RealmList<Tool> tools) {
        this.activity = activity;
        this.tools = tools;
    }

    @NonNull
    @Override
    public ToolHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_perangkat_dan_kunci, parent, false);

        ToolHolder holder = new ToolHolder(v);
        /*
        final int position = holder.getAdapterPosition();
        final Tool tool = tools.get(position);

        holder.layout_tool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((K3Activity)activity).getDb().beginTransaction();
                tool.setSudah_digunakan(tool.getSudah_digunakan() == 0 ? 1 : 0);
                ((K3Activity)activity).saveAssignment(((K3Activity)activity).getApp().getActiveAssignment());
                ((K3Activity)activity).getDb().commitTransaction();
                notifyItemChanged(position);
            }
        });
        */
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ToolHolder holder, final int position) {

        final Tool tool = tools.get(position);
        holder.text_tool_title.setText(tool.getNama_perlengkapan());
        if (tool.getDeskripsi() != null && tool.getDeskripsi().isEmpty() == false) {
            holder.text_tool_deskripsi.setText("No . Kunci : " + tool.getDeskripsi());
            holder.text_tool_deskripsi.setVisibility(View.VISIBLE);
        } else {
            holder.text_tool_deskripsi.setText("");
            holder.text_tool_deskripsi.setVisibility(View.GONE);
        }
        if (tool.getJumlah() > 0) {
            holder.text_tool_jumlah.setText("Jumlah : " + tool.getJumlah());
            holder.text_tool_jumlah.setVisibility(View.VISIBLE);
        } else {
            holder.text_tool_jumlah.setText("");
            holder.text_tool_jumlah.setVisibility(View.GONE);
        }
        holder.image_tool.setImageURI(tool.getUrl_gambar());
        if (tool.getSudah_digunakan() == 1) {
            holder.layout_right.setBackgroundColor(activity.getResources().getColor(R.color.colorBlueLighter));
            holder.layout_tool.setBackgroundColor(activity.getResources().getColor(R.color.colorBlueLighter));
            holder.text_tool_title.setTextColor(activity.getResources().getColor(R.color.colorWhite));
            holder.text_tool_deskripsi.setTextColor(activity.getResources().getColor(R.color.colorWhite));
            holder.text_tool_jumlah.setTextColor(activity.getResources().getColor(R.color.colorWhite));
            holder.image_status.setImageResource(R.drawable.icon_tick2);
        } else {
            holder.layout_right.setBackgroundColor(activity.getResources().getColor(R.color.colorBackgroundGrayLighter));
            holder.layout_tool.setBackgroundColor(activity.getResources().getColor(R.color.colorWhite));
            holder.text_tool_title.setTextColor(activity.getResources().getColor(R.color.colorBlack));
            holder.text_tool_deskripsi.setTextColor(activity.getResources().getColor(R.color.colorBlack));
            holder.text_tool_jumlah.setTextColor(activity.getResources().getColor(R.color.colorBlack));
            holder.image_status.setImageResource(R.drawable.circle_procedure);
        }
        if(editable) {
            holder.layout_tool.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ((K3ProsedureActivity) activity).getDb().beginTransaction();
                    tool.setSudah_digunakan(tool.getSudah_digunakan() == 0 ? 1 : 0);
                    ((K3ProsedureActivity) activity).saveAssignment(((K3ProsedureActivity) activity).getActiveAssignment());
                    ((K3ProsedureActivity) activity).getDb().commitTransaction();
                    notifyItemChanged(position);
                }
            });
        }
        //holder.layout_tool.setBackgroundColor(tool.getItemBackgroundColor());
        //holder.layout_right.setBackgroundColor(tool.getSideBackgroundColor());

    }

    @Override
    public int getItemCount() {
        return tools.size();
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
}

