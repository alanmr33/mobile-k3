package com.indocyber.mobilek3.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.IntDef;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.activity.LockOutActivity;
import com.indocyber.mobilek3.activity.Main;
import com.indocyber.mobilek3.holder.AssignmentCalendarHolder;
import com.indocyber.mobilek3.model.Assignment;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.SimpleDateFormat;

import io.realm.RealmResults;

import static android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP;

public class AssignmentCalendarAdapter extends RecyclerView.Adapter<AssignmentCalendarHolder> {

    public static final int TYPE_FRAGMENT = 0;
    public static final int TYPE_DIALOG = 1;


    @IntDef({TYPE_FRAGMENT, TYPE_DIALOG})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {}

    private Activity activity;
    private int type;
    private RealmResults<Assignment> assignments;

    public AssignmentCalendarAdapter(Activity activity, RealmResults<Assignment> assignments,@Type int type) {
        this.activity = activity;
        this.assignments = assignments;
        this.type = type;
    }

    @NonNull
    @Override
    public AssignmentCalendarHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_assignment_calendar, parent, false);
        return new AssignmentCalendarHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AssignmentCalendarHolder holder, int position) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
            SimpleDateFormat olddateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            final Assignment assignment = assignments.get(position);
            holder.textViewAset.setText(assignment.getInstalasi());

            holder.textViewTitle.setText(assignment.getJudul());
            holder.layout_calendar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, LockOutActivity.class);
                    ((Main) activity).getSession().setActiveAssignmentId(assignment.getId());
                    activity.startActivity(intent);
                }
            });

            if(type == TYPE_FRAGMENT) {
                holder.textViewDate.setText(dateFormat.format(assignment.getTanggal_assign()));
            }
            else if(type == TYPE_DIALOG)
            {
                holder.textViewDate.setText(timeFormat.format(assignment.getTanggal_assign()));

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return assignments.size();
    }
}
