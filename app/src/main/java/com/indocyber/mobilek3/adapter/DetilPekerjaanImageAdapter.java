package com.indocyber.mobilek3.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.dialog.DetilPekerjaanImageAlertDialog;
import com.indocyber.mobilek3.holder.DetilPekerjaanImageHolder;
import com.indocyber.mobilek3.model.Attachment;

import io.realm.RealmList;


public class DetilPekerjaanImageAdapter extends RecyclerView.Adapter<DetilPekerjaanImageHolder> {
    private Activity activity;
    private RealmList<Attachment> attachments;

    public DetilPekerjaanImageAdapter(Activity activity, RealmList<Attachment> attachments) {
        this.activity = activity;
        this.attachments = attachments;
    }

    @NonNull
    @Override
    public DetilPekerjaanImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_detil_assignment_image, parent, false);
        return new DetilPekerjaanImageHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DetilPekerjaanImageHolder holder, int position) {

        final Attachment attachment = attachments.get(position);
        holder.image_detil_assignment.setImageURI(attachment.getUrl_gambar());

        holder.image_detil_assignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*DetilPekerjaanImageDialog d = DetilPekerjaanImageDialog.newInstance(attachment);
                d.show(((DetilPekerjaanActivity)activity).getSupportFragmentManager(), "DetilPekerjaanImageDialog");
                */
                DetilPekerjaanImageAlertDialog a = DetilPekerjaanImageAlertDialog.newInstance(activity, attachment);


            }
        });

        //holder.layout_tool.setBackgroundColor(tool.getItemBackgroundColor());
        //holder.layout_right.setBackgroundColor(tool.getSideBackgroundColor());

    }

    @Override
    public int getItemCount() {
        return attachments.size();
    }
}
