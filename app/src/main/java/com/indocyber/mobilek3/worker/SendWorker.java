package com.indocyber.mobilek3.worker;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.text.format.Time;
import android.util.Log;

import com.google.gson.Gson;
import com.indocyber.mobilek3.App;
import com.indocyber.mobilek3.model.Assignment;
import com.indocyber.mobilek3.model.AssignmentSubmit;
import com.indocyber.mobilek3.model.Step;
import com.indocyber.mobilek3.model.StepSubmit;
import com.indocyber.mobilek3.utils.WorkerUtil;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import androidx.work.NetworkType;
import androidx.work.Worker;
import androidx.work.Configuration;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendWorker extends Worker {
    @NonNull
    @Override
    public Result doWork() {

        Realm realm;
        Result result;
        realm = Realm.getDefaultInstance();
        result = Result.RETRY;
        final String assignment_id = getInputData().getString("ASN_ID");
        try {
            Context applicationContext = getApplicationContext();
            App app = (App) applicationContext;
            Log.e(SendWorker.this.getClass().getSimpleName(), assignment_id);
            RealmQuery<Assignment> assignmentRealmQuery = realm
                    .where(Assignment.class)
                    .equalTo("id", assignment_id);
            RealmResults<Assignment> assignments = assignmentRealmQuery.findAll();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            final Assignment assignment = assignments.get(0);
            AssignmentSubmit submit = new AssignmentSubmit();
            submit.setId(assignment.getId());
            submit.setKode_area(assignment.getKode_area());
            submit.setKode_asset(assignment.getKode_asset());
            submit.setKode_prosedur(assignment.getKode_prosedur());
            submit.setTanggal_assign(dateFormat.format(assignment.getTanggal_assign()));
            if (assignment.getTanggal_mulai() != null) {
                submit.setTanggal_mulai(dateFormat.format(assignment.getTanggal_mulai()));
            }
            if (assignment.getTanggal_selesai() != null) {
                submit.setTanggal_selesai(dateFormat.format(assignment.getTanggal_selesai()));
            }
            ArrayList<StepSubmit> stepSubmits = new ArrayList<>();
            for (Step s : assignment.getDaftar_langkah()) {

                Log.e("SendWorker", "Start : " + s.getStart_date());
                Log.e("SendWorker", "End : " + s.getEnd_date());
                StepSubmit stepSubmit = new StepSubmit();
                stepSubmit.setId(s.getId());
                Date starDate=(s.getStart_date()==null)? new Date(): s.getStart_date();
                Date endDate=(s.getEnd_date()==null)? new Date(): s.getEnd_date();
                stepSubmit.setStart_date(dateFormat.format(starDate));
                stepSubmit.setEnd_date(dateFormat.format(endDate));
                stepSubmit.setBukti_pekerjaan(s.getBukti_pekerjaan());

                stepSubmits.add(stepSubmit);
            }
            submit.setStepSubmits(stepSubmits);

            String jsonRequest = new Gson().toJson(submit);
            Log.e(this.getClass().getSimpleName(), jsonRequest);
            RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonRequest);
            Response<ResponseBody> response = app.getWebServiceWithToken().saveAssignment(body).execute();
            if (response.isSuccessful()) {
                realm.beginTransaction();
                assignment.setSending(false);
                realm.insertOrUpdate(assignment);
                realm.commitTransaction();
                result = Result.SUCCESS;
                try {
                    Log.e("SENDER", response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                result = Result.RETRY;
                try {
                    Log.e("SENDER", response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
/*                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                Realm realm=Realm.getDefaultInstance();
                                realm.beginTransaction();
                                assignment.setSending(false);
                                realm.insertOrUpdate(assignment);
                                realm.commitTransaction();
                                result[0] = Result.SUCCESS;
                                try {
                                    Log.e("SENDER",response.body().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                result[0] = Result.RETRY;
                                try {
                                    Log.e("SENDER",response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            t.printStackTrace();
                            result[0] = Result.RETRY;
                        }
                    });*/


        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Log.e(this.getClass().getSimpleName(), "Exception : " + Result.RETRY.name());
            result = Result.RETRY;
        }

        if (realm != null && realm.isClosed() == false)
            realm.close();
        if (result == Result.RETRY) {
            try {
                Thread.sleep(1000*30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Log.e(this.getClass().getSimpleName(), "Enqueue Assignment Retry");
            WorkerUtil.enqueueAssignmentRetry(getApplicationContext(), assignment_id);
            result = Result.FAILURE;
        }
        Log.e(this.getClass().getSimpleName(), result.name());
        return result;
    }
}
