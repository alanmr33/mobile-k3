package com.indocyber.mobilek3;

import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Handler;
import android.support.multidex.MultiDex;
import android.support.v4.app.NotificationCompat;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.indocyber.mobilek3.config.Global;
import com.indocyber.mobilek3.model.Assignment;
import com.indocyber.mobilek3.utils.Session;
import com.indocyber.mobilek3.utils.TypefaceUtil;
import com.indocyber.mobilek3.utils.WebService;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;

public class App extends Application {
    private Realm realm;
    private Session session;
    private String willStartID;
    private String willEndID;
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        realm = Realm.getDefaultInstance();
        session=new Session(this);
        Fresco.initialize(this);
        //Slider.init(new PicassoImageLoadingService());
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF",
                "fonts/montserrat.ttf");
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                e.printStackTrace();
            }
        });
        FirebaseApp.initializeApp(this);
//        check(false);
    }
//    private void check(boolean delayed){
//        if(delayed) {
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if(session.isLogin()){
//                        checkTask();
//                    }
//                    check(true);
//                }
//            },60*1000);
//        }else{
//            if(session.isLogin()){
//                checkTask();
//            }
//            check(true);
//        }
//    }
    public void checkTask(){
        long toDay=Calendar.getInstance().getTimeInMillis();
        Assignment willStart=realm.where(Assignment.class)
                .equalTo("sending",false)
                .and()
                .between("tanggal_assign", new Date(toDay),new Date((toDay+(10*60*1000))))
                .and()
                .equalTo("user_id", getSession().getUserdata().getUser_id())
                .findFirst();
        if(willStart!=null){
            if(willStartID!=willStart.getId()) {
                willStartID=willStart.getId();
                final NotificationManager mNotifyManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());
                mBuilder.setOngoing(true);
                mBuilder.setContentTitle(willStart.getJudul()+" - "+willStart.getPekerjaan())
                        .setContentText("Akan segera dimulai")
                        .setSmallIcon(R.drawable.ic_notification);
                mNotifyManager.notify(4001378, mBuilder.build());
            }
        }
        Assignment willEnd=realm.where(Assignment.class)
                .equalTo("sending",false)
                .and()
                .between("tanggal_assign", new Date(toDay),new Date((toDay+(10*60*1000))))
                .and()
                .equalTo("user_id", getSession().getUserdata().getUser_id())
                .findFirst();
        if(willEnd!=null){
            if(willEndID!=willEnd.getId()) {
                willEndID=willEnd.getId();
                final NotificationManager mNotifyManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());
                mBuilder.setOngoing(true);
                mBuilder.setContentTitle(willEnd.getJudul()+" - "+willEnd.getPekerjaan())
                        .setContentText("Akan segera berakhir")
                        .setSmallIcon(R.drawable.ic_notification);
                mNotifyManager.notify(4001378, mBuilder.build());
            }
        }
    }
    public WebService getWebService() {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(300, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization",
                                "Basic bW9iaWxlazM6MU5kb2N5YmVyLg==")
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Global.URL)
                .client(client)
                .build();
        return retrofit.
                create(WebService.class);
    }
    public WebService getWebServiceWithToken(){
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(300, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization",
                                "Bearer "+session.getToken())
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Global.URL)
                .client(client)
                .build();
        return retrofit.
                create(WebService.class);
    }
    public Realm getRealm() {
        return realm;
    }

    /*public Assignment getActiveAssignment() {
        return activeAssignment;
    }

    public void setActiveAssignment(Assignment activeAssignment) {
        this.activeAssignment = activeAssignment;
    }
    */

    public Session getSession() {
        return session;
    }
}
