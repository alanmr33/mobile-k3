package com.indocyber.mobilek3.model;

import android.content.Context;
import android.graphics.Color;

import com.indocyber.mobilek3.R;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ToolSafety extends RealmObject {
    @PrimaryKey
    private String id;
    private String nama_perlengkapan;
    private String url_gambar;
    private int sudah_digunakan;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_perlengkapan() {
        return nama_perlengkapan;
    }

    public void setNama_perlengkapan(String nama_perlengkapan) {
        this.nama_perlengkapan = nama_perlengkapan;
    }

    public String getUrl_gambar() {
        return url_gambar;
    }

    public void setUrl_gambar(String url_gambar) {
        this.url_gambar = url_gambar;
    }

    public int getSudah_digunakan() {
        return sudah_digunakan;
    }

    public void setSudah_digunakan(int sudah_digunakan) {
        this.sudah_digunakan = sudah_digunakan;
    }

}
