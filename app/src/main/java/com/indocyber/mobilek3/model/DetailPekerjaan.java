package com.indocyber.mobilek3.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DetailPekerjaan extends RealmObject {
    @PrimaryKey
    private String id;
    private String url_gambar;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl_gambar() {
        return url_gambar;
    }

    public void setUrl_gambar(String url_gambar) {
        this.url_gambar = url_gambar;
    }
}
