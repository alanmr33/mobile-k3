package com.indocyber.mobilek3.model;

public class DashboardJadwal {
    private String asset;
    private String tanggal;
    private String ditugaskan_oleh;

    public DashboardJadwal(String asset, String tanggal, String ditugaskan_oleh) {
        this.asset = asset;
        this.tanggal = tanggal;
        this.ditugaskan_oleh = ditugaskan_oleh;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getDitugaskan_oleh() {
        return ditugaskan_oleh;
    }

    public void setDitugaskan_oleh(String ditugaskan_oleh) {
        this.ditugaskan_oleh = ditugaskan_oleh;
    }
}
