package com.indocyber.mobilek3.model;

import java.util.Calendar;
import java.util.Date;

public class CalendarDetail {
    private int tanggal;
    private long total;
    private boolean libur;
    private Date date;

    public CalendarDetail() {    }
    public CalendarDetail(boolean libur) {
        this.libur = libur;
    }

    public CalendarDetail(int tanggal, long total, boolean libur, Date date) {
        this.tanggal = tanggal;
        this.total = total;
        this.libur = libur;
        this.date = date;
    }

    public int getTanggal() {
        return tanggal;
    }

    public void setTanggal(int tanggal) {
        this.tanggal = tanggal;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public boolean isLibur() {
        return libur;
    }

    public void setLibur(boolean libur) {
        this.libur = libur;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
