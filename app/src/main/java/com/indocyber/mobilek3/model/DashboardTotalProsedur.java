package com.indocyber.mobilek3.model;

import java.util.ArrayList;
import java.util.List;

public class DashboardTotalProsedur {
    private ArrayList<Integer> tahun_sekarang;
    private ArrayList<Integer> tahun_sebelumnya;


    public ArrayList<Integer> getTahun_sekarang() {
        return tahun_sekarang;
    }

    public void setTahun_sekarang(ArrayList<Integer> tahun_sekarang) {
        this.tahun_sekarang = tahun_sekarang;
    }

    public ArrayList<Integer> getTahun_sebelumnya() {
        return tahun_sebelumnya;
    }

    public void setTahun_sebelumnya(ArrayList<Integer> tahun_sebelumnya) {
        this.tahun_sebelumnya = tahun_sebelumnya;
    }
}
