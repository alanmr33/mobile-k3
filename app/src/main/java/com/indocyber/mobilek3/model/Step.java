package com.indocyber.mobilek3.model;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Step extends RealmObject {
    @PrimaryKey
    private String id;
    private int urutan;
    private String nama_pekerjaan;
    private String deskripsi_pekerjaan;
    private String perangkat;
    private String nomor_kunci;
    private String bukti_pekerjaan;
    private Boolean sudah_dikerjakan;
    private RealmList<DetailPekerjaan> gambar;
    private Date start_date;
    private Date end_date;
    private double latitude;
    private double longitude;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getUrutan() {
        return urutan;
    }

    public void setUrutan(int urutan) {
        this.urutan = urutan;
    }

    public String getNama_pekerjaan() {
        return nama_pekerjaan;
    }

    public void setNama_pekerjaan(String nama_pekerjaan) {
        this.nama_pekerjaan = nama_pekerjaan;
    }

    public String getDeskripsi_pekerjaan() {
        return deskripsi_pekerjaan;
    }

    public void setDeskripsi_pekerjaan(String deskripsi_pekerjaan) {
        this.deskripsi_pekerjaan = deskripsi_pekerjaan;
    }

    public Boolean getSudah_dikerjakan() {
        return sudah_dikerjakan;
    }

    public void setSudah_dikerjakan(Boolean sudah_dikerjakan) {
        this.sudah_dikerjakan = sudah_dikerjakan;
    }

    public RealmList<DetailPekerjaan> getGambar() {
        return gambar;
    }

    public void setGambar(RealmList<DetailPekerjaan> gambar) {
        this.gambar = gambar;
    }

    public String getBukti_pekerjaan() {
        return bukti_pekerjaan;
    }

    public void setBukti_pekerjaan(String bukti_pekerjaan) {
        this.bukti_pekerjaan = bukti_pekerjaan;
    }

    public String getPerangkat() {
        return perangkat;
    }

    public void setPerangkat(String perangkat) {
        this.perangkat = perangkat;
    }

    public String getNomor_kunci() {
        return nomor_kunci;
    }

    public void setNomor_kunci(String nomor_kunci) {
        this.nomor_kunci = nomor_kunci;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }
}
