package com.indocyber.mobilek3.model;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Assignment extends RealmObject implements AssignmentHeaderProvider {
    @PrimaryKey
    private String id;
    private String judul;
    private String instalasi;
    private String pekerjaan;
    private String gambar;
    private Date tanggal_assign;
    private Date tanggal_mulai;
    private Date tanggal_selesai;
    private String elektronik_id;
    private String status;
    private String kode_asset;
    private String area;
    private String kode_prosedur;
    private String kode_area;
    private String ditugaskan_oleh;
    private String url_panduan;
    private String warepack;
    private RealmList<Attachment> daftar_lampiran;
    private RealmList<ToolSafety> daftar_perlengkapan_safety;
	private RealmList<Tool> daftar_perlengkapan;
    private RealmList<Step> daftar_langkah;
    private RealmList<MessageLockout> daftar_lockout;
    private RealmList<Message> daftar_peringatan_bahaya;
    private String user_id;
    private boolean sending;

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getInstalasi() {
        return instalasi;
    }

    public void setInstalasi(String instalasi) {
        this.instalasi = instalasi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKode_asset() {
        return kode_asset;
    }

    public void setKode_asset(String kode_asset) {
        this.kode_asset = kode_asset;
    }

    public String getKode_prosedur() {
        return kode_prosedur;
    }

    public void setKode_prosedur(String kode_prosedur) {
        this.kode_prosedur = kode_prosedur;
    }

    public String getKode_area() {
        return kode_area;
    }

    public void setKode_area(String kode_area) {
        this.kode_area = kode_area;
    }

    public String getDitugaskan_oleh() {
        return ditugaskan_oleh;
    }

    public void setDitugaskan_oleh(String ditugaskan_oleh) {
        this.ditugaskan_oleh = ditugaskan_oleh;
    }

    public String getUrl_panduan() {
        return url_panduan;
    }

    public void setUrl_panduan(String url_panduan) {
        this.url_panduan = url_panduan;
    }

    public RealmList<ToolSafety> getDaftar_perlengkapan_safety() {
        return daftar_perlengkapan_safety;
    }

    public void setDaftar_perlengkapan_safety(RealmList<ToolSafety> daftar_perlengkapan_safety) {
        this.daftar_perlengkapan_safety = daftar_perlengkapan_safety;
    }

    public RealmList<Tool> getDaftar_perlengkapan() {
        return daftar_perlengkapan;
    }

	public void setDaftar_perlengkapan(RealmList<Tool> daftar_perlengkapan) {
        this.daftar_perlengkapan = daftar_perlengkapan;
    }

    public Date getTanggal_assign() {
        return tanggal_assign;
    }

    public void setTanggal_assign(Date tanggal_assign) {
        this.tanggal_assign = tanggal_assign;
    }

    public Date getTanggal_selesai() {
        return tanggal_selesai;
    }

    public void setTanggal_selesai(Date tanggal_selesai) {
        this.tanggal_selesai = tanggal_selesai;
    }


    public RealmList<MessageLockout> getDaftar_lockout() {
        return daftar_lockout;
    }

    public void setDaftar_lockout(RealmList<MessageLockout> daftar_lockout) {
        this.daftar_lockout = daftar_lockout;
    }


    public RealmList<Message> getDaftar_peringatan_bahaya() {
        return daftar_peringatan_bahaya;
    }

    public void setDaftar_peringatan_bahaya(RealmList<Message> daftar_peringatan_bahaya) {
        this.daftar_peringatan_bahaya = daftar_peringatan_bahaya;
    }

    public RealmList<Step> getDaftar_langkah() {
        return daftar_langkah;
    }

    public void setDaftar_langkah(RealmList<Step> daftar_langkah) {
        this.daftar_langkah = daftar_langkah;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public RealmList<Attachment> getDaftar_lampiran() {
        return daftar_lampiran;
    }

    public void setDaftar_lampiran(RealmList<Attachment> daftar_lampiran) {
        this.daftar_lampiran = daftar_lampiran;
    }

    public JSONArray getWarepack() throws JSONException {
        return new JSONArray(warepack);
    }

    public void setWarepack(JSONArray warepack) {
        this.warepack = warepack.toString();
    }

    public Date getTanggal_mulai() {
        return tanggal_mulai;
    }

    public void setTanggal_mulai(Date tanggal_mulai) {
        this.tanggal_mulai = tanggal_mulai;
    }

    public String getElektronik_id() {
        return elektronik_id;
    }

    public void setElektronik_id(String elektronik_id) {
        this.elektronik_id = elektronik_id;
    }

    public void setWarepack(String warepack) {
        this.warepack = warepack;
    }


    public boolean isCompleted() {
        return (getStatus().equals("COMPLETE"));
    }
    public boolean isSending() {
        return sending;
    }

    public void setSending(boolean sending) {
        this.sending = sending;
    }
}
