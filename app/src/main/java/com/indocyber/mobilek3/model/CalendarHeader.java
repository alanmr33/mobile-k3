package com.indocyber.mobilek3.model;

public class CalendarHeader extends CalendarDetail{
    private String namaHari;

    public CalendarHeader(String namaHari, boolean libur) {
        super(libur);
        this.namaHari = namaHari;
    }

    public String getNamaHari() {
        return namaHari;
    }

    public void setNamaHari(String namaHari) {
        this.namaHari = namaHari;
    }


}
