package com.indocyber.mobilek3.model;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

public class AssignmentHeader implements AssignmentHeaderProvider {

    private String header;

    public AssignmentHeader(String header)
    {
        this.header = header;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }


}
