package com.indocyber.mobilek3.model;

import java.util.Date;

public class StepSubmit {
    private String id;
    private String start_date;
    private String end_date;
    private String bukti_pekerjaan;

    public StepSubmit() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getBukti_pekerjaan() {
        return bukti_pekerjaan;
    }

    public void setBukti_pekerjaan(String bukti_pekerjaan) {
        this.bukti_pekerjaan = bukti_pekerjaan;
    }
}
