package com.indocyber.mobilek3.model;

import java.util.List;

public class DashboardResponse {
    private List<DashboardKegiatan> chartKegiatan;
    private List<DashboardJadwal> table7Harian;
    private DashboardTotalProsedur totalProsedure;


    public List<DashboardKegiatan> getChartKegiatan() {
        return chartKegiatan;
    }

    public void setChartKegiatan(List<DashboardKegiatan> chartKegiatan) {
        this.chartKegiatan = chartKegiatan;
    }

    public List<DashboardJadwal> getTable7Harian() {
        return table7Harian;
    }

    public void setTable7Harian(List<DashboardJadwal> table7Harian) {
        this.table7Harian = table7Harian;
    }

    public DashboardTotalProsedur getTotalProsedure() {
        return totalProsedure;
    }

    public void setTotalProsedure(DashboardTotalProsedur totalProsedure) {
        this.totalProsedure = totalProsedure;
    }
}
