package com.indocyber.mobilek3.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Attachment extends RealmObject {
    @PrimaryKey
    private String id;
    private String url_gambar;
    private String title_gambar;
    private String subtitle_gambar;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl_gambar() {
        return url_gambar;
    }

    public void setUrl_gambar(String url_gambar) {
        this.url_gambar = url_gambar;
    }

    public String getTitle_gambar() {
        return title_gambar;
    }

    public void setTitle_gambar(String title_gambar) {
        this.title_gambar = title_gambar;
    }

    public String getSubtitle_gambar() {
        return subtitle_gambar;
    }

    public void setSubtitle_gambar(String subtitle_gambar) {
        this.subtitle_gambar = subtitle_gambar;
    }
}
