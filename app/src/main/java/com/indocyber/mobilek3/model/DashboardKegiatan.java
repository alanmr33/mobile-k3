package com.indocyber.mobilek3.model;

import com.google.gson.annotations.SerializedName;

public class DashboardKegiatan {
    @SerializedName("assets")
    private String nama_kegiatan;

    @SerializedName("finish")
    private int terselesaikan;

    @SerializedName("onprogress")
    private int belum_terselesaikan;

    public DashboardKegiatan(String nama_kegiatan, int terselesaikan, int belum_terselesaikan) {
        this.nama_kegiatan = nama_kegiatan;
        this.terselesaikan = terselesaikan;
        this.belum_terselesaikan = belum_terselesaikan;
    }

    public String getNama_kegiatan() {
        return nama_kegiatan;
    }

    public void setNama_kegiatan(String nama_kegiatan) {
        this.nama_kegiatan = nama_kegiatan;
    }

    public int getTerselesaikan() {
        return terselesaikan;
    }

    public void setTerselesaikan(int terselesaikan) {
        this.terselesaikan = terselesaikan;
    }

    public int getBelum_terselesaikan() {
        return belum_terselesaikan;
    }

    public void setBelum_terselesaikan(int belum_terselesaikan) {
        this.belum_terselesaikan = belum_terselesaikan;
    }
}
