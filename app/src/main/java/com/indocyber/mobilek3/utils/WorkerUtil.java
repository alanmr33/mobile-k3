package com.indocyber.mobilek3.utils;

import android.content.Context;
import android.util.Log;

import com.indocyber.mobilek3.worker.SendWorker;

import java.util.concurrent.TimeUnit;

import androidx.work.BackoffPolicy;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

public class WorkerUtil {

    public static void enqueueAssignmentRetry(Context context, String assignment_id)
    {

        //parsing body
        Data.Builder data = new Data.Builder();
        data.putString("ASN_ID", assignment_id);
        OneTimeWorkRequest.Builder oneTimeWorkRequestBuilder = new OneTimeWorkRequest
                .Builder(SendWorker.class)
                //.setInitialDelay(1000, TimeUnit.MILLISECONDS)
                .setInputData(data.build());
        //.setBackoffCriteria(BackoffPolicy.LINEAR, 30, TimeUnit.SECONDS);
        if(NetworkUtil.isConnected(context) == false)
        {
            Constraints constraints = new Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED).build();
            oneTimeWorkRequestBuilder.setConstraints(constraints);
        }
        WorkManager.getInstance()
                .enqueue(oneTimeWorkRequestBuilder.build());
    }

    public static void enqueueAssignment(String assignment_id)
    {
        //parsing body
        Data.Builder data = new Data.Builder();
        data.putString("ASN_ID", assignment_id);
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED).build();
        WorkManager.getInstance()
                .enqueue(new OneTimeWorkRequest
                        .Builder(SendWorker.class)
                        //.setInitialDelay(5, TimeUnit.SECONDS)
                        .setInputData(data.build())
                        .setConstraints(constraints)
                        .setBackoffCriteria(BackoffPolicy.LINEAR, 30, TimeUnit.SECONDS)
                        .build());

    }

}
