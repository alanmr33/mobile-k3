package com.indocyber.mobilek3.utils;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface WebService {
    @FormUrlEncoded
    @POST("oauth/token")
    Call<ResponseBody> doLogin(@Field("username") String username,
                                    @Field("password") String password,
                                    @Field("grant_type") String grant_type
    );
    @GET("me")
    Call<ResponseBody> getProfile();
    @GET("assignment/get")
    Call<ResponseBody> getAssignments();
    @POST("assignment/save")
    Call<ResponseBody> saveAssignment(@Body RequestBody assignment);
    @FormUrlEncoded
    @POST("request_forgot")
    Call<ResponseBody> requestNewPassword(@Field("username") String username);

    @GET("dashboard/ajax")
    Call<ResponseBody> getDashboardAjax();
}
