package com.indocyber.mobilek3.utils;

import android.app.Activity;

import com.indocyber.mobilek3.R;

public class AnimUtil {

    public static void openActivityFromRight(Activity activity) {
        activity.overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
    }

    /**
     * open new activity by slide from left
     */
    public static void openActivityFromLeft(Activity activity) {
        activity.overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
    }

    /**
     *
     */
    /*public static void openActivityfromBottom(Activity activity) {
        activity.overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
    }

    public static void openActivityFromTop(Activity activity) {
        activity.overridePendingTransition(R.anim.slide_down, R.anim.slide_up);
    }*/

    public static void openActivityFromBottom(Activity activity)
    {
        activity.overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_bottom);
    }
    public static void closeActivityFromBottom(Activity activity)
    {
        activity.overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_top);
    }

}
