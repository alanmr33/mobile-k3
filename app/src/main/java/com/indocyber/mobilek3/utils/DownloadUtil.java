package com.indocyber.mobilek3.utils;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

public class DownloadUtil {
    private DownloadManager dm;
    private Context context;
    private long enqueueNumber;
    private BroadcastReceiver receiver;
    public DownloadUtil(Context context)
    {
        this.context = context;
        dm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
    }

    public void enqueueDownload(String urlDownload, String fileName)
    {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(urlDownload));
        request.setTitle(fileName);
        request.setDescription(urlDownload);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.allowScanningByMediaScanner();
        request.setVisibleInDownloadsUi(true);
        //request.setShowRunningNotification(true);
        enqueueNumber = dm.enqueue(request);
        Toast.makeText(context, "Downloading "+fileName, Toast.LENGTH_SHORT).show();
    }

    public void showDownload()
    {
        Intent i = new Intent();
        i.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
        context.startActivity(i);
    }

    public void registerBroadcastReciver()
    {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(intent.getAction()))
                {
                    long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(enqueueNumber);
                    Cursor c = dm.query(query);
                    if(c.moveToFirst()){
                        int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
                        if(DownloadManager.STATUS_SUCCESSFUL == c.getInt( columnIndex))
                        {

                            Toast.makeText(context, "Download Completed", Toast.LENGTH_SHORT).show();
                            context.unregisterReceiver(this);
                            showDownload();
                        }
                    }
                }
            }
        };
        context.registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public void unregisterBroadcastReceiver()
    {
        if(receiver != null) { context.unregisterReceiver(receiver); }
    }
}
