package com.indocyber.mobilek3.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.util.Locale;

public class IntentUtil {

    public static void actionViewUrlIntent(Context context, String url)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(intent);
    }

    public static void mapNavigationIntent(Context context, double latitude, double longitude)
    {
        Uri gmmIntentUri = Uri.parse("google.navigation:q="+latitude+","+longitude);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        context.startActivity(mapIntent);
    }

    public static void mapViewIntent(Context context, double latitude, double longitude)
    {
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        context.startActivity(intent);
    }
}
