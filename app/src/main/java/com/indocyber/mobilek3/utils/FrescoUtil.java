package com.indocyber.mobilek3.utils;

import android.content.Context;
import android.os.AsyncTask;

import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.request.ImageRequest;
import com.indocyber.mobilek3.model.Assignment;
import com.indocyber.mobilek3.model.Attachment;
import com.indocyber.mobilek3.model.DetailPekerjaan;
import com.indocyber.mobilek3.model.Step;
import com.indocyber.mobilek3.model.Tool;
import com.indocyber.mobilek3.model.ToolSafety;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FrescoUtil {
    public void prefetchImageToDiskCache(String uri)
    {
        final DataSource<Void> ds =
                Fresco.getImagePipeline().prefetchToDiskCache(ImageRequest.fromUri(uri), null);

    }


    public void prefetchImageAllAssignment(Context applicationContext, List<Assignment> assignments)
    {
        for (Assignment assignment: assignments) {
            prefetchImageToDiskCache(assignment.getGambar());
            for(Attachment attachment : assignment.getDaftar_lampiran())
            {
                prefetchImageToDiskCache(attachment.getUrl_gambar());
            }
            for(Tool tool : assignment.getDaftar_perlengkapan())
            {
                prefetchImageToDiskCache(tool.getUrl_gambar());
            }
            for(ToolSafety toolSafety : assignment.getDaftar_perlengkapan_safety())
            {
                prefetchImageToDiskCache(toolSafety.getUrl_gambar());
            }
            for(Step step : assignment.getDaftar_langkah())
            {
                for (DetailPekerjaan detailPekerjaan : step.getGambar())
                {
                    prefetchImageToDiskCache(detailPekerjaan.getUrl_gambar());
                    Picasso.with(applicationContext).load(detailPekerjaan.getUrl_gambar()).fetch();
                }
            }
        }
    }
}
