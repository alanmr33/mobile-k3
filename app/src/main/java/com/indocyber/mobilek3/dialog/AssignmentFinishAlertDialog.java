package com.indocyber.mobilek3.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.activity.K3ProsedureActivity;
import com.indocyber.mobilek3.activity.Main;
import com.indocyber.mobilek3.activity.ToolSafetyActivity;
import com.indocyber.mobilek3.model.Assignment;
import com.indocyber.mobilek3.model.Attachment;
import com.indocyber.mobilek3.utils.WorkerUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;


public class AssignmentFinishAlertDialog implements View.OnClickListener{
    private String tanggalPengerjaan, tanggalSelesai;
    private Context context;
    private AlertDialog dialog;
    private Assignment assignment;
    public static AssignmentFinishAlertDialog newInstance(Context context,Assignment assignment, String tanggalPengerjaan, String tanggalSelesai)
    {
        return new AssignmentFinishAlertDialog(context,assignment, tanggalPengerjaan, tanggalSelesai);
    }
    public AssignmentFinishAlertDialog(Context context,Assignment assignment, String tanggalPengerjaan, String tanggalSelesai)
    {
        this.context = context;
        this.tanggalPengerjaan = tanggalPengerjaan;
        this.tanggalSelesai = tanggalSelesai;
        this.assignment = assignment;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View v = LayoutInflater.from(context).inflate(R.layout.dialog_assignment_finish, null, false);
        builder.setView(v);
        TextView textViewHariPengerjaan = v.findViewById(R.id.textViewHariPengerjaan);
        TextView textViewJamMulai = v.findViewById(R.id.textViewJamMulai);
        TextView textViewJamSelesai = v.findViewById(R.id.textViewJamSelesai);
        Button buttonOk = v.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(this);


        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
        SimpleDateFormat olddateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            textViewHariPengerjaan.setText(dateFormat.format(olddateFormat.parse(tanggalPengerjaan)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            textViewJamMulai.setText(timeFormat.format(olddateFormat.parse(tanggalPengerjaan)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            textViewJamSelesai.setText(timeFormat.format(olddateFormat.parse(tanggalSelesai)));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.buttonOk:
                dialog.dismiss();
                WorkerUtil.enqueueAssignment(assignment.getId());
                //TODO: Hilangkan comment ketika ingin  membuat assignment complete
                K3ProsedureActivity activity = ((K3ProsedureActivity)context);
                activity.getDb().beginTransaction();
                activity.getActiveAssignment().setStatus("COMPLETE");
                activity.getDb().commitTransaction();

                Intent main = new Intent(context, Main.class);
                main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(main);
                break;
        }
    }
}

