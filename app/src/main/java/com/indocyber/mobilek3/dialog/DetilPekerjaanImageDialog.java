package com.indocyber.mobilek3.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.model.Attachment;

public class DetilPekerjaanImageDialog extends DialogFragment implements View.OnClickListener {

    private final static String TITLE = "title";
    private final static String SUBTITLE = "subtitle";
    private final static String URL = "url";

    String title;
    String subtitle;
    String url;

    public static DetilPekerjaanImageDialog newInstance(Attachment attachment) {
        DetilPekerjaanImageDialog f = new DetilPekerjaanImageDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString(TITLE, attachment.getTitle_gambar());
        args.putString(SUBTITLE, attachment.getSubtitle_gambar());
        args.putString(URL, attachment.getUrl_gambar());
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title = getArguments().getString(TITLE);
        subtitle = getArguments().getString(SUBTITLE);
        url = getArguments().getString(URL);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_detilpekerjaan_preview_image, container, false);

        TextView text_title = v.findViewById(R.id.text_title);
        TextView text_subtitle = v.findViewById(R.id.text_subtitle);
        ImageView image_close = v.findViewById(R.id.image_close);
        image_close.setOnClickListener(this);
        SimpleDraweeView image_preview = v.findViewById(R.id.image_preview);

        text_title.setText(title);
        text_subtitle.setText(subtitle);
        image_preview.setImageURI(url);


        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.image_close:
                DetilPekerjaanImageDialog.this.dismiss();
                break;
        }
    }
}
