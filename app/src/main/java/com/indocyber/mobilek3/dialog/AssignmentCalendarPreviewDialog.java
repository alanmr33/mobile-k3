package com.indocyber.mobilek3.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.activity.Main;
import com.indocyber.mobilek3.adapter.AssignmentCalendarAdapter;
import com.indocyber.mobilek3.model.Assignment;
import com.indocyber.mobilek3.utils.DateUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.realm.RealmQuery;
import io.realm.RealmResults;


public class AssignmentCalendarPreviewDialog implements View.OnClickListener{
    private Date selectedDate;
    private Context context;
    private AlertDialog dialog;
    public static AssignmentCalendarPreviewDialog newInstance(Context context, Date selectedDate)
    {
        return new AssignmentCalendarPreviewDialog(context,selectedDate);
    }
    public AssignmentCalendarPreviewDialog(Context context, Date selectedDate)
    {
        this.context = context;
        this.selectedDate = selectedDate;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View v = LayoutInflater.from(context).inflate(R.layout.dialog_calendar_asignment_preview, null, false);
        builder.setView(v);
        TextView text_title = v.findViewById(R.id.text_title);
        ImageView image_close = v.findViewById(R.id.image_close);
        image_close.setOnClickListener(this);
        RecyclerView recyclerView = v.findViewById(R.id.recyclerView);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyyy");
        text_title.setText(simpleDateFormat.format(selectedDate));

        Calendar cal = Calendar.getInstance();
        cal.setTime(selectedDate);

        Calendar startDay = (Calendar) cal.clone();
        DateUtil.setFirstTimeOfDay(startDay);
        Calendar endDay = (Calendar) cal.clone();
        DateUtil.setLastTimeOfDay(endDay);


        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);


        RealmQuery<Assignment> assignmentRealmQuery = ((Main) context).getDb()
                .where(Assignment.class)
                .between("tanggal_assign",
                        startDay.getTime(),
                        endDay.getTime());
        RealmResults<Assignment> assignments = assignmentRealmQuery.findAll().sort("tanggal_assign");
        AssignmentCalendarAdapter assignmentAdapter = new AssignmentCalendarAdapter((Activity)context, assignments, AssignmentCalendarAdapter.TYPE_DIALOG);
        recyclerView.setAdapter(assignmentAdapter);


        dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.image_close:
                dialog.dismiss();
                break;
        }
    }
}
