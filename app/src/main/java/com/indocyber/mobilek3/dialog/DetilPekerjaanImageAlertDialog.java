package com.indocyber.mobilek3.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.model.Attachment;

public class DetilPekerjaanImageAlertDialog implements View.OnClickListener{
    private Attachment attachment;
    private Context context;
    private AlertDialog dialog;
    public static DetilPekerjaanImageAlertDialog newInstance(Context context, Attachment attachment)
    {
        return new DetilPekerjaanImageAlertDialog(context,attachment);
    }
    public DetilPekerjaanImageAlertDialog(Context context, Attachment attachment)
    {
        this.context = context;
        this.attachment = attachment;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View v = LayoutInflater.from(context).inflate(R.layout.dialog_detilpekerjaan_preview_image, null, false);
        builder.setView(v);
        TextView text_title = v.findViewById(R.id.text_title);
        TextView text_subtitle = v.findViewById(R.id.text_subtitle);
        ImageView image_close = v.findViewById(R.id.image_close);
        image_close.setOnClickListener(this);
        SimpleDraweeView image_preview = v.findViewById(R.id.image_preview);

        text_title.setText(attachment.getTitle_gambar());
        text_subtitle.setText(attachment.getSubtitle_gambar());
        image_preview.setImageURI(attachment.getUrl_gambar());

        dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.image_close:
                dialog.dismiss();
                break;
        }
    }
}
