package com.indocyber.mobilek3.service;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.activity.Main;

import java.util.Map;
import java.util.Random;

public class SafetyK3Service extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String body = (remoteMessage.getNotification() == null) ? "-" : remoteMessage.getNotification().getBody();
        String title = (remoteMessage.getNotification() == null) ? "-" : remoteMessage.getNotification().getTitle();
        Map<String, String> data = remoteMessage.getData();
        Intent intent = new Intent(getApplicationContext(), Main.class);
        final NotificationManager mNotifyManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel("all-safetyk3", "all-safetyk3", NotificationManager.IMPORTANCE_HIGH);
            assert mNotifyManager != null;
            mNotifyManager.createNotificationChannel(mChannel);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        if (body.equals("-")) {
            title = data.get("title");
            body = data.get("body");
            final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getBaseContext(),"all-safetyk3");
            mBuilder.setContentTitle(title)
                    .setContentText(body)
                    .setGroupSummary(true)
                    .setSound(alarmSound)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setSmallIcon(R.drawable.ic_notification);
            assert mNotifyManager != null;
            mNotifyManager.notify(new Random().nextInt(1000), mBuilder.build());
        }else{
            final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getBaseContext(),"all-safetyk3");
            mBuilder.setContentTitle(title)
                    .setContentText(body)
                    .setGroupSummary(true)
                    .setSound(alarmSound)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setSmallIcon(R.drawable.ic_notif);
            assert mNotifyManager != null;
            mNotifyManager.notify(new Random().nextInt(1000), mBuilder.build());
        }
    }
}
