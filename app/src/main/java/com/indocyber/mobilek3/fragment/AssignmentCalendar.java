package com.indocyber.mobilek3.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.exceptions.OutOfDateRangeException;
import com.applandeo.materialcalendarview.listeners.OnCalendarPageChangeListener;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.activity.Main;
import com.indocyber.mobilek3.adapter.AssignmentAdapter;
import com.indocyber.mobilek3.adapter.AssignmentCalendarAdapter;
import com.indocyber.mobilek3.model.Assignment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.RealmQuery;
import io.realm.RealmResults;


public class AssignmentCalendar extends Fragment {
    private CalendarView calendarViewAssignment;
    private RecyclerView recyclerViewAC;
    public AssignmentCalendar() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            View v=inflater.inflate(R.layout.fragment_calendar, container, false);
            calendarViewAssignment=v.findViewById(R.id.calendarViewAssignment);
            recyclerViewAC=v.findViewById(R.id.recyclerViewAssignments);
            calendarViewAssignment.setDate(Calendar.getInstance().getTime());
            calendarViewAssignment.setOnDayClickListener(new OnDayClickListener() {
                @Override
                public void onDayClick(EventDay eventDay) {

                }
            });
            calendarViewAssignment.setOnForwardPageChangeListener(new OnCalendarPageChangeListener() {
                @Override
                public void onChange() {
                    loadAssignment();
                }
            });
            calendarViewAssignment.setOnPreviousPageChangeListener(new OnCalendarPageChangeListener() {
                @Override
                public void onChange() {
                    loadAssignment();
                }
            });
            recyclerViewAC.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerViewAC.setItemAnimator(new DefaultItemAnimator());
            loadAssignment();
            return v;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public void loadAssignment(){
        try {
            SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM");
            SimpleDateFormat dateFormatEnd=new SimpleDateFormat("yyyy-MM-dd");
            String startDay=dateFormat
                    .format(calendarViewAssignment.getCurrentPageDate().getTime())+"-01";
            Calendar startCal=Calendar.getInstance();
            startCal.setTime(dateFormatEnd.parse(startDay));
            Calendar endCal=Calendar.getInstance();
            endCal.setTime(dateFormatEnd.parse(startDay));
            endCal.add(Calendar.MONTH,1);
            RealmQuery<Assignment> assignmentRealmQuery=((Main)getActivity()).getDb()
                    .where(Assignment.class)
                    .between("tanggal_assign",
                            startCal.getTime(),
                            endCal.getTime())
                    .and()
                    .equalTo("user_id", ((Main)getActivity()).getSession().getUserdata().getUser_id());
            RealmResults<Assignment> assignments=assignmentRealmQuery.findAll().sort("tanggal_assign");
            List<EventDay> events = new ArrayList<>();
            for (int i=0;i<assignments.size();i++){
                Date date =assignments.get(i).getTanggal_assign();
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                EventDay eventDay=new EventDay(cal, R.drawable.ic_assignment);
                events.add(eventDay);
            }
            calendarViewAssignment.setEvents(events);
            AssignmentCalendarAdapter assignmentAdapter=new AssignmentCalendarAdapter(getActivity(),assignments, AssignmentCalendarAdapter.TYPE_FRAGMENT);
            recyclerViewAC.setAdapter(assignmentAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
