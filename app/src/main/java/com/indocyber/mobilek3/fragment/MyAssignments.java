package com.indocyber.mobilek3.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.activity.K3Activity;
import com.indocyber.mobilek3.activity.Main;
import com.indocyber.mobilek3.adapter.AssignmentAdapter;
import com.indocyber.mobilek3.model.Assignment;
import com.indocyber.mobilek3.model.AssignmentHeader;
import com.indocyber.mobilek3.model.AssignmentHeaderProvider;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;



public class MyAssignments extends Fragment {
    private SwipeRefreshLayout pullToRefresh;
    public MyAssignments() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd MMMM yyyy");
        String today=dateFormat.format(Calendar.getInstance().getTime());
        Calendar yesterday=Calendar.getInstance();
        yesterday.add(Calendar.DATE,-1);
        String beforeToday=dateFormat.format(yesterday.getTime());
        SimpleDateFormat dateFormatWhere=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        RealmQuery<Assignment> assignmentRealmQuery= null;
        try {
            assignmentRealmQuery = ((Main)getActivity()).getDb()
                    .where(Assignment.class)

                    .equalTo("user_id", ((K3Activity)getActivity()).getSession().getUserdata().getUser_id())
                    .sort("tanggal_assign");
            RealmResults<Assignment> assignments=assignmentRealmQuery.findAll();
            View v=inflater.inflate(R.layout.fragment_my_assignments, container, false);
            pullToRefresh=v.findViewById(R.id.pullToRefresh);
            pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    pullToRefresh.setRefreshing(false);
                    ((Main)getActivity()).getData();
                }
            });
            RecyclerView assignmentList=v.findViewById(R.id.recyclerViewAssignments);
            assignmentList.setLayoutManager(new LinearLayoutManager(getActivity()));
            assignmentList.setItemAnimator(new DefaultItemAnimator());

            List<AssignmentHeaderProvider> assignmentsWithHeader = new ArrayList<>();

            if(assignments.size() > 0) {
                for (int i = 0; i < assignments.size(); i++) {
                    String dateCurrentAssignment = dateFormat.format(assignments.get(i).getTanggal_assign());
                    if(i==0)
                    {
                        if(dateCurrentAssignment.equals(today))
                        {
                            assignmentsWithHeader.add(new AssignmentHeader("Pekerjaan Hari Ini"));
                        }
                        else if(dateCurrentAssignment.equals(beforeToday)){
                            assignmentsWithHeader.add(new AssignmentHeader("Pekerjaan Kemarin"));
                        }
                        else
                        {
                            assignmentsWithHeader.add(new AssignmentHeader(dateCurrentAssignment));
                        }
                        assignmentsWithHeader.add(assignments.get(i));
                    }
                    else {
                        String datePrevAssignment = dateFormat.format(assignments.get(i - 1).getTanggal_assign());
                        if (dateCurrentAssignment.equals(datePrevAssignment)) {
                            assignmentsWithHeader.add(assignments.get(i));
                        } else {
                            assignmentsWithHeader.add(new AssignmentHeader(dateCurrentAssignment));
                            assignmentsWithHeader.add(assignments.get(i));
                        }
                    }
                }
            }

            AssignmentAdapter assignmentAdapter=new AssignmentAdapter(getActivity(),assignmentsWithHeader);
            assignmentList.setAdapter(assignmentAdapter);
            assignmentList.setNestedScrollingEnabled(false);
            return v;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }



}
