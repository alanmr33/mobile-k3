package com.indocyber.mobilek3.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.activity.K3Activity;
import com.indocyber.mobilek3.activity.Main;
import com.indocyber.mobilek3.adapter.AssignmentCalendarAdapter;
import com.indocyber.mobilek3.adapter.CalendarAdapter;
import com.indocyber.mobilek3.lib.GridSpacingDecoration;
import com.indocyber.mobilek3.model.Assignment;
import com.indocyber.mobilek3.model.CalendarDetail;
import com.indocyber.mobilek3.model.CalendarHeader;
import com.indocyber.mobilek3.utils.DateUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.RealmQuery;
import io.realm.RealmResults;


public class CalendarFragment extends Fragment implements View.OnClickListener {
    private RecyclerView recyclerViewCalendarAssignments;
    private RecyclerView recyclerViewAC;

    private TextView editTextTahun, editTextBulan;
    private Button buttonMencari;

    private List<String> months;
    private List<String> years;

    public CalendarFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            View v = inflater.inflate(R.layout.fragment_calendar_assignment, container, false);
            editTextBulan = v.findViewById(R.id.editTextBulan);
            editTextTahun = v.findViewById(R.id.editTextTahun);
            buttonMencari = v.findViewById(R.id.buttonMencari);
            buttonMencari.setOnClickListener(this);
            recyclerViewCalendarAssignments = v.findViewById(R.id.recyclerViewCalendarAssignments);
            recyclerViewAC = v.findViewById(R.id.recyclerViewAssignments);

            recyclerViewCalendarAssignments.setLayoutManager(new GridLayoutManager(getActivity(), 7));
            recyclerViewCalendarAssignments.setItemAnimator(new DefaultItemAnimator());
            recyclerViewCalendarAssignments.addItemDecoration(new GridSpacingDecoration(
                    getResources().getDimensionPixelSize(R.dimen.calendar_spaceing),
                    7));
            recyclerViewCalendarAssignments.setNestedScrollingEnabled(false);


            recyclerViewAC.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerViewAC.setItemAnimator(new DefaultItemAnimator());
            recyclerViewAC.setNestedScrollingEnabled(false);

            setupMonths();
            setupYears();
            setupMonthPicker();
            setupYearPicker();
            loadAssignment();
            return v;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void setupMonthPicker() {
        Calendar calendar = Calendar.getInstance();
        editTextBulan.setText(months.get(calendar.get(Calendar.MONTH)));
        editTextBulan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedPosition = 0;
                selectedPosition = months.indexOf(editTextBulan.getText());
                showDialogPicker(months.toArray(new CharSequence[months.size()]), selectedPosition, editTextBulan);
            }
        });
    }

    private void setupYearPicker() {
        Calendar calendar = Calendar.getInstance();
        editTextTahun.setText("" + calendar.get(Calendar.YEAR));

        editTextTahun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedPosition = 0;
                selectedPosition = years.indexOf(editTextTahun.getText());
                showDialogPicker(years.toArray(new CharSequence[years.size()]), selectedPosition, editTextTahun);
            }
        });
    }

    public void loadAssignment() {
        try {
            Calendar startMonth = Calendar.getInstance();
            startMonth.set(Integer.parseInt(editTextTahun.getText().toString()), months.indexOf(editTextBulan.getText()), 1);
            DateUtil.setFirstDayOfMonth(startMonth);
            DateUtil.setFirstTimeOfDay(startMonth);

            Calendar endMonth = (Calendar) startMonth.clone();
            DateUtil.setLastDayOfMonth(endMonth);
            DateUtil.setLastTimeOfDay(endMonth);


            RealmQuery<Assignment> assignmentRealmQuery = ((Main) getActivity()).getDb()
                    .where(Assignment.class)
                    .between("tanggal_assign",
                            startMonth.getTime(),
                            endMonth.getTime())
                    .equalTo("user_id", ((K3Activity) getActivity()).getSession().getUserdata().getUser_id());
            RealmResults<Assignment> assignments = assignmentRealmQuery.findAll().sort("tanggal_assign");
            AssignmentCalendarAdapter assignmentAdapter = new AssignmentCalendarAdapter(getActivity(), assignments, AssignmentCalendarAdapter.TYPE_FRAGMENT);
            recyclerViewAC.setAdapter(assignmentAdapter);


            List<CalendarDetail> calendarDetails = new ArrayList<>();
            calendarDetails.add(new CalendarHeader("MINGGU", true));
            calendarDetails.add(new CalendarHeader("SENIN", false));
            calendarDetails.add(new CalendarHeader("SELASA", false));
            calendarDetails.add(new CalendarHeader("RABU", false));
            calendarDetails.add(new CalendarHeader("KAMIS", false));
            calendarDetails.add(new CalendarHeader("JUMAT", false));
            calendarDetails.add(new CalendarHeader("SABTU", true));


            for (int i = Calendar.SUNDAY; i < startMonth.get(Calendar.DAY_OF_WEEK); i++) {
                CalendarDetail calendarDetail = new CalendarDetail();
                calendarDetail.setTanggal(0);
                calendarDetail.setLibur(false);
                calendarDetail.setTotal(0);
                calendarDetails.add(calendarDetail);
            }

            for (int i = startMonth.get(Calendar.DAY_OF_MONTH); i <= endMonth.get(Calendar.DAY_OF_MONTH); i++) {
                CalendarDetail calendarDetail = new CalendarDetail();
                calendarDetails.add(calendarDetail);

                Calendar cal = Calendar.getInstance();
                cal.setTime(startMonth.getTime());
                cal.add(Calendar.DAY_OF_MONTH, i - 1);

                Calendar startDay = (Calendar) cal.clone();
                DateUtil.setFirstTimeOfDay(startDay);
                Calendar endDay = (Calendar) cal.clone();
                DateUtil.setLastTimeOfDay(endDay);

                calendarDetail.setDate(startDay.getTime());
                calendarDetail.setTanggal(cal.get(Calendar.DAY_OF_MONTH));
                calendarDetail.setLibur(
                        cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
                                || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY);


                long total = ((Main) getActivity()).getDb()
                        .where(Assignment.class)

                        .between("tanggal_assign",
                                startDay.getTime(),
                                endDay.getTime())
                        .equalTo("user_id", ((K3Activity) getActivity()).getSession().getUserdata().getUser_id())
                        .count();


                calendarDetail.setTotal(total);
            }

            CalendarAdapter calendarAdapter = new CalendarAdapter(getActivity(), calendarDetails);
            recyclerViewCalendarAssignments.setAdapter(calendarAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonMencari:
                loadAssignment();
                break;
        }
    }

    private void setupMonths() {
        months = new ArrayList<>();
        months.add("Januari");
        months.add("Februari");
        months.add("Maret");
        months.add("April");
        months.add("Mei");
        months.add("Juni");
        months.add("Juli");
        months.add("Agustus");
        months.add("September");
        months.add("Oktober");
        months.add("November");
        months.add("Desember");
    }

    private void showDialogPicker(final CharSequence[] strings, final int selectedPosition, final TextView selectedEditText) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        builder.setItems(strings, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedEditText.setText(strings[which]);
                    }
                }
        );
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setupYears() {
        years = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        for (int i = year; i <= year + 1; i++) {
            years.add("" + i);
        }
    }


}
