package com.indocyber.mobilek3.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.gson.Gson;
import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.activity.Login;
import com.indocyber.mobilek3.activity.Main;
import com.indocyber.mobilek3.adapter.DashboardJadwalAdapter;
import com.indocyber.mobilek3.adapter.DashboardKegiatanAdapter;
import com.indocyber.mobilek3.adapter.DetilPekerjaanImageAdapter;
import com.indocyber.mobilek3.config.Global;
import com.indocyber.mobilek3.lib.GridSpacingDecoration;
import com.indocyber.mobilek3.model.DashboardJadwal;
import com.indocyber.mobilek3.model.DashboardKegiatan;
import com.indocyber.mobilek3.model.DashboardResponse;
import com.indocyber.mobilek3.model.DashboardTotalProsedur;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DashboardFragment extends Fragment {

    private String[] months = new String[]{
            "JAN", "FEB", "MAR", "APR", "MEI", "JUN", "JUL", "AGU", "SEP", "OKT", "NOV", "DES"
    };

    private int[] colors = new int[]{
            R.color.colorBackgroundBlueLighter,
            R.color.colorBlueLighter
    };
    private LineChart lineChartTotalProsedur;
    private ProgressDialog loading;

    private List<DashboardKegiatan> dashboardKegiatans;
    private RecyclerView recyclerViewDashboardKegiatan;
    private DashboardKegiatanAdapter dashboardKegiatanAdapter;

    private List<DashboardJadwal> dashboardJadwals;
    private RecyclerView recyclerViewDashboardJadwal;
    private DashboardJadwalAdapter dashboardJadwalAdapter;
    private SwipeRefreshLayout pullToRefresh;
    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_dashboard_assignment, container, false);

        loading = new ProgressDialog(getContext());
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loading.setTitle(Global.DIALOG_TITLE);
        loading.setMessage("Loading ...");
        loading.setCancelable(true);


        lineChartTotalProsedur = v.findViewById(R.id.lineChartTotalProsedur);
        lineChartTotalProsedur.getDescription().setEnabled(false);
        lineChartTotalProsedur.setPinchZoom(false);

        Legend l = lineChartTotalProsedur.getLegend();
        //l.setWordWrapEnabled(true);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);

        lineChartTotalProsedur.getAxisRight().setEnabled(false);


        XAxis xAxis = lineChartTotalProsedur.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(0f);
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return months[(int) value % months.length];
            }
        });

        dashboardKegiatans = new ArrayList<>();
        recyclerViewDashboardKegiatan = v.findViewById(R.id.recyclerViewDashboardKegiatan);
        recyclerViewDashboardKegiatan.setLayoutManager(
                new GridLayoutManager(this.getContext(), 3, LinearLayoutManager.VERTICAL, false));
        recyclerViewDashboardKegiatan.setItemAnimator(new DefaultItemAnimator());
        dashboardKegiatanAdapter = new DashboardKegiatanAdapter(this.getActivity(), dashboardKegiatans);
        recyclerViewDashboardKegiatan.addItemDecoration(new GridSpacingDecoration(
                getResources().getDimensionPixelSize(R.dimen.card_insets),
                3));
        recyclerViewDashboardKegiatan.setAdapter(dashboardKegiatanAdapter);
        recyclerViewDashboardKegiatan.setNestedScrollingEnabled(false);

        dashboardJadwals = new ArrayList<>();
        recyclerViewDashboardJadwal = v.findViewById(R.id.recyclerViewDashboardJadwal);
        recyclerViewDashboardJadwal.setLayoutManager(
                new LinearLayoutManager(this.getContext()));
        recyclerViewDashboardJadwal.setItemAnimator(new DefaultItemAnimator());
        dashboardJadwalAdapter = new DashboardJadwalAdapter(this.getActivity(), dashboardJadwals);

        recyclerViewDashboardJadwal.setAdapter(dashboardJadwalAdapter);
        recyclerViewDashboardJadwal.setNestedScrollingEnabled(false);
        pullToRefresh=v.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDashboardAjax();
            }
        });
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDashboardAjax();
    }

    private void setChartTotalProsedurData(DashboardTotalProsedur dashboardTotalProsedur) {

        ArrayList<Entry> entriesTahunSebelumnya = new ArrayList<Entry>();

        for (int i = 0; i < dashboardTotalProsedur.getTahun_sebelumnya().size(); i++) {
            entriesTahunSebelumnya.add(new Entry(i, dashboardTotalProsedur.getTahun_sebelumnya().get(i)));
        }

        LineDataSet lineDataSetTahunSebelumnya;
        lineDataSetTahunSebelumnya = new LineDataSet(entriesTahunSebelumnya, "Tahun Sebelumnya");

        lineDataSetTahunSebelumnya.setColor(getResources().getColor(colors[0]));
        lineDataSetTahunSebelumnya.setDrawValues(false);
        lineDataSetTahunSebelumnya.setLineWidth(1f);
        lineDataSetTahunSebelumnya.setDrawFilled(true);
        lineDataSetTahunSebelumnya.setFormLineWidth(1f);
        lineDataSetTahunSebelumnya.setFormSize(15.f);
        lineDataSetTahunSebelumnya.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetTahunSebelumnya.setFillColor(getResources().getColor(colors[0]));
        //set1.setAxisDependency(YAxis.AxisDependency.LEFT);


        ArrayList<Entry> entriesTahunSekarang = new ArrayList<Entry>();

        for (int i = 0; i < dashboardTotalProsedur.getTahun_sekarang().size(); i++) {
            entriesTahunSekarang.add(new Entry(i, dashboardTotalProsedur.getTahun_sekarang().get(i)));
        }

        LineDataSet lineDataSetTahunSekarang;
        lineDataSetTahunSekarang = new LineDataSet(entriesTahunSekarang, "Tahun Sekarang");
        lineDataSetTahunSekarang.setColor(getResources().getColor(colors[1]));
        lineDataSetTahunSekarang.setDrawValues(false);
        lineDataSetTahunSekarang.setLineWidth(1f);
        lineDataSetTahunSekarang.setDrawFilled(true);
        lineDataSetTahunSekarang.setFormLineWidth(1f);
        lineDataSetTahunSekarang.setFormSize(15.f);
        lineDataSetTahunSekarang.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSetTahunSekarang.setFillColor(getResources().getColor(colors[1]));

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(lineDataSetTahunSebelumnya); // add the datasets
        dataSets.add(lineDataSetTahunSekarang); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(dataSets);

        // set data
        lineChartTotalProsedur.setData(data);
        lineChartTotalProsedur.invalidate();

    }

    public Main getActivityMain() {
        return ((Main) getActivity());
    }

    public void getDashboardAjax() {
        loading.show();
        getActivityMain().getApp().getWebServiceWithToken().getDashboardAjax().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loading.dismiss();
                pullToRefresh.setRefreshing(false);
                if (response.isSuccessful()) {
                    try {
                        String body = response.body().string();
                        dashboardKegiatans.clear();
                        dashboardJadwals.clear();
                        DashboardResponse dashboardResponse = new Gson().fromJson(body, DashboardResponse.class);
                        dashboardKegiatans.addAll(dashboardResponse.getChartKegiatan());
                        dashboardKegiatanAdapter.notifyDataSetChanged();
                        dashboardJadwals.addAll(dashboardResponse.getTable7Harian());
                        dashboardJadwalAdapter.notifyDataSetChanged();
                        setChartTotalProsedurData(dashboardResponse.getTotalProsedure());
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), "Terjadi Kesalahan Sistem.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        String error = response.errorBody().string();
                        Log.e("Profile", error);
                        JSONObject object = new JSONObject(error);
                        if (object.getString("error").equals("invalid_grant")) {
                            AlertDialog dialog = new AlertDialog.Builder(getContext())
                                    .setTitle(Global.DIALOG_TITLE)
                                    .setMessage("Nama User/Kata Sandi Tidak Sesuai")
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).create();
                            dialog.show();
                        } else {
                            AlertDialog dialog = new AlertDialog.Builder(getContext())
                                    .setTitle(Global.DIALOG_TITLE)
                                    .setMessage(object.getString("error"))
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).create();
                            dialog.show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), "Cek Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(getContext(), "Cek Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
