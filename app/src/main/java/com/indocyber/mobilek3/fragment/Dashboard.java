package com.indocyber.mobilek3.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.indocyber.mobilek3.R;
import com.indocyber.mobilek3.activity.Main;
import com.indocyber.mobilek3.config.Global;

import im.delight.android.webview.AdvancedWebView;

public class Dashboard extends Fragment implements AdvancedWebView.Listener {
    private AdvancedWebView mWebView;
    private ProgressDialog dialog;
    public Dashboard() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_dashboard, container, false);
        mWebView=v.findViewById(R.id.webviewDashboard);
        dialog=new ProgressDialog(getActivity());
        dialog.setMessage("Loading ...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mWebView.setListener(getActivity(), this);
        mWebView.addHttpHeader("Authorization","Bearer "+((Main)getActivity()).getSession().getToken());
        mWebView.loadUrl(Global.URL+"dashboard");
        dialog.show();
        return v;
    }
    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();
        mWebView.onResume();
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        mWebView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mWebView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);
    }


    @Override
    public void onPageStarted(String url, Bitmap favicon) { }

    @Override
    public void onPageFinished(String url) {
        dialog.dismiss();
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        dialog.dismiss();
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) { }

    @Override
    public void onExternalPageRequest(String url) { }
}
