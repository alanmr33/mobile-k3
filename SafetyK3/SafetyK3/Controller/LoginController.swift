//
//  LoginController.swift
//  SafetyK3
//
//  Created by alan maulana on 11/01/19.
//  Copyright © 2019 alan maulana. All rights reserved.
//

import UIKit

class LoginController: UIViewController{
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func onClick(_ sender: Any) {
            performSegue(withIdentifier: "movetoHome", sender: self)
    }
    
}
