package com.indocyber.mobilek3api.model;

/**
 * Created by Indocyber on 10/09/2018.
 */
public class Tool {
    private String id;
    private String nama_perlengkapan;
    private String deskripsi;
    private int jumlah;
    private String url_gambar;

    public Tool() {
    }

    public Tool(String id, String nama_perlengkapan, String deskripsi, String url_gambar,int jumlah) {
        this.id = id;
        this.nama_perlengkapan = nama_perlengkapan;
        this.deskripsi = deskripsi;
        this.url_gambar = url_gambar;
        this.jumlah = jumlah;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_perlengkapan() {
        return nama_perlengkapan;
    }

    public void setNama_perlengkapan(String nama_perlengkapan) {
        this.nama_perlengkapan = nama_perlengkapan;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getUrl_gambar() {
        return url_gambar;
    }

    public void setUrl_gambar(String url_gambar) {
        this.url_gambar = url_gambar;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }
}
