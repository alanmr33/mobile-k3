package com.indocyber.mobilek3api.model;

/**
 * Created by Indocyber on 10/09/2018.
 */
public class ToolSafety {
    private String id;
    private String nama_perlengkapan;
    private String url_gambar;

    public ToolSafety(String id, String nama_perlengkapan, String url_gambar) {
        this.id = id;
        this.nama_perlengkapan = nama_perlengkapan;
        this.url_gambar = url_gambar;
    }

    public ToolSafety() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_perlengkapan() {
        return nama_perlengkapan;
    }

    public void setNama_perlengkapan(String nama_perlengkapan) {
        this.nama_perlengkapan = nama_perlengkapan;
    }

    public String getUrl_gambar() {
        return url_gambar;
    }

    public void setUrl_gambar(String url_gambar) {
        this.url_gambar = url_gambar;
    }
}
