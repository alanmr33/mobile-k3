package com.indocyber.mobilek3api.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Indocyber on 10/09/2018.
 */
public class PublicUser {
    private String user_id;
    private String username;
    private String fullname;
    private String imei;
    private String photo;
    private Set<Role> roles = new HashSet<Role>();

    public PublicUser(User user) {
        this.user_id=user.getId();
        this.username=user.getName();
        this.fullname=user.getLogin();
        this.roles=user.getRoles();
        this.imei=user.getImei();
        this.photo=user.getPhoto_url();
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
