package com.indocyber.mobilek3api.model;

import java.util.List;

/**
 * Created by Indocyber on 13/09/2018.
 */
public class DashboardResponse {
    private List<Object> chartKegiatan;
    private List<Object> table7Harian;
    private ChartTotalProsedure totalProsedure;

    public DashboardResponse() {
    }

    public List<Object> getChartKegiatan() {
        return chartKegiatan;
    }

    public void setChartKegiatan(List<Object> chartKegiatan) {
        this.chartKegiatan = chartKegiatan;
    }

    public List<Object> getTable7Harian() {
        return table7Harian;
    }

    public void setTable7Harian(List<Object> table7Harian) {
        this.table7Harian = table7Harian;
    }

    public ChartTotalProsedure getTotalProsedure() {
        return totalProsedure;
    }

    public void setTotalProsedure(ChartTotalProsedure totalProsedure) {
        this.totalProsedure = totalProsedure;
    }
}
