package com.indocyber.mobilek3api.model;

/**
 * Created by Indocyber on 10/09/2018.
 */
public class Message {
    private String id;
    private int no;
    private String message;

    public Message() {
    }

    public Message(String id, int no, String message) {
        this.id = id;
        this.no = no;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
