package com.indocyber.mobilek3api.model;

/**
 * Created by Indocyber on 10/09/2018.
 */
public class DetailPekerjaan {
    private String id;
    private String url_gambar;

    public DetailPekerjaan() {
    }

    public DetailPekerjaan(String id, String url_gambar) {
        this.id = id;
        this.url_gambar = url_gambar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl_gambar() {
        return url_gambar;
    }

    public void setUrl_gambar(String url_gambar) {
        this.url_gambar = url_gambar;
    }
}
