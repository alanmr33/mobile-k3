package com.indocyber.mobilek3api.model;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Indocyber on 10/09/2018.
 */
public class APIResponse {
    private boolean status;
    private String message;
    private List<Object> data;

    public APIResponse(boolean status, String message, List<Object> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public APIResponse() {
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }
}
