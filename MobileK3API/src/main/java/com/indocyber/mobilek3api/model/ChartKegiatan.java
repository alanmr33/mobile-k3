package com.indocyber.mobilek3api.model;

/**
 * Created by Indocyber on 13/09/2018.
 */
public class ChartKegiatan {
    private String assets;
    private Integer finish;
    private Integer onprogress;

    public ChartKegiatan() {
    }

    public String getAssets() {
        return assets;
    }

    public void setAssets(String assets) {
        this.assets = assets;
    }

    public Integer getFinish() {
        return finish;
    }

    public void setFinish(Integer finish) {
        this.finish = finish;
    }

    public Integer getOnprogress() {
        return onprogress;
    }

    public void setOnprogress(Integer onprogress) {
        this.onprogress = onprogress;
    }
}
