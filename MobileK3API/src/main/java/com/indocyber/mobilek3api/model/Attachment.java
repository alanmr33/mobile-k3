package com.indocyber.mobilek3api.model;

/**
 * Created by Indocyber on 10/09/2018.
 */
public class Attachment {
    private String id;
    private String url_gambar;
    private String title_gambar;
    private String subtitle_gambar;

    public Attachment(String id, String url_gambar, String title_gambar, String subtitle_gambar) {
        this.id = id;
        this.url_gambar = url_gambar;
        this.title_gambar = title_gambar;
        this.subtitle_gambar = subtitle_gambar;
    }

    public Attachment() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl_gambar() {
        return url_gambar;
    }

    public void setUrl_gambar(String url_gambar) {
        this.url_gambar = url_gambar;
    }

    public String getTitle_gambar() {
        return title_gambar;
    }

    public void setTitle_gambar(String title_gambar) {
        this.title_gambar = title_gambar;
    }

    public String getSubtitle_gambar() {
        return subtitle_gambar;
    }

    public void setSubtitle_gambar(String subtitle_gambar) {
        this.subtitle_gambar = subtitle_gambar;
    }
}
