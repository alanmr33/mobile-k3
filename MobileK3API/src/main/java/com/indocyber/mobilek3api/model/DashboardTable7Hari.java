package com.indocyber.mobilek3api.model;

/**
 * Created by Indocyber on 13/09/2018.
 */
public class DashboardTable7Hari {
    private String asset;
    private String tanggal;
    private String ditugaskan_oleh;

    public DashboardTable7Hari() {
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getDitugaskan_oleh() {
        return ditugaskan_oleh;
    }

    public void setDitugaskan_oleh(String ditugaskan_oleh) {
        this.ditugaskan_oleh = ditugaskan_oleh;
    }
}
