package com.indocyber.mobilek3api.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Indocyber on 21/09/2018.
 */
public class AssignmentSubmit {
    private String id;
    private String kode_asset;
    private String kode_prosedur;
    private String kode_area;
    private String tanggal_assign;
    private String tanggal_mulai;
    private String tanggal_selesai;
    private ArrayList<StepSubmit> stepSubmits;

    public AssignmentSubmit() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKode_asset() {
        return kode_asset;
    }

    public void setKode_asset(String kode_asset) {
        this.kode_asset = kode_asset;
    }

    public String getKode_prosedur() {
        return kode_prosedur;
    }

    public void setKode_prosedur(String kode_prosedur) {
        this.kode_prosedur = kode_prosedur;
    }

    public String getKode_area() {
        return kode_area;
    }

    public void setKode_area(String kode_area) {
        this.kode_area = kode_area;
    }

    public String getTanggal_assign() {
        return tanggal_assign;
    }

    public void setTanggal_assign(String tanggal_assign) {
        this.tanggal_assign = tanggal_assign;
    }

    public String getTanggal_mulai() {
        return tanggal_mulai;
    }

    public void setTanggal_mulai(String tanggal_mulai) {
        this.tanggal_mulai = tanggal_mulai;
    }

    public String getTanggal_selesai() {
        return tanggal_selesai;
    }

    public void setTanggal_selesai(String tanggal_selesai) {
        this.tanggal_selesai = tanggal_selesai;
    }

    public ArrayList<StepSubmit> getStepSubmits() {
        return stepSubmits;
    }

    public void setStepSubmits(ArrayList<StepSubmit> stepSubmits) {
        this.stepSubmits = stepSubmits;
    }
}
