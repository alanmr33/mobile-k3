package com.indocyber.mobilek3api.model;

import java.util.List;
import java.util.Date;

/**
 * Created by Indocyber on 10/09/2018.
 */
public class Assignment {
    private String id;
    private String judul;
    private String instalasi;
    private String pekerjaan;
    private String gambar;
    private String elektronik_id;
    private Date tanggal_assign;
    private Date tanggal_mulai;
    private Date tanggal_selesai;
    private String status;
    private String kode_asset;
    private String kode_prosedur;
    private String kode_area;
    private String area;
    private String ditugaskan_oleh;
    private String url_panduan;
    private String warepack;
    private String user_id;
    private List<Attachment> daftar_lampiran;
    private List<ToolSafety> daftar_perlengkapan_safety;
    private List<Tool> daftar_perlengkapan;
    private List<Step> daftar_langkah;
    private List<MessageLockOut> daftar_lockout;
    private List<Message> daftar_peringatan_bahaya;
    public Assignment() {
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getInstalasi() {
        return instalasi;
    }

    public void setInstalasi(String instalasi) {
        this.instalasi = instalasi;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public Date getTanggal_assign() {
        return tanggal_assign;
    }

    public void setTanggal_assign(Date tanggal_assign) {
        this.tanggal_assign = tanggal_assign;
    }

    public Date getTanggal_mulai() {
        return tanggal_mulai;
    }

    public void setTanggal_mulai(Date tanggal_mulai) {
        this.tanggal_mulai = tanggal_mulai;
    }

    public Date getTanggal_selesai() {
        return tanggal_selesai;
    }

    public void setTanggal_selesai(Date tanggal_selesai) {
        this.tanggal_selesai = tanggal_selesai;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKode_asset() {
        return kode_asset;
    }

    public void setKode_asset(String kode_asset) {
        this.kode_asset = kode_asset;
    }

    public String getKode_prosedur() {
        return kode_prosedur;
    }

    public void setKode_prosedur(String kode_prosedur) {
        this.kode_prosedur = kode_prosedur;
    }

    public String getKode_area() {
        return kode_area;
    }

    public void setKode_area(String kode_area) {
        this.kode_area = kode_area;
    }

    public String getDitugaskan_oleh() {
        return ditugaskan_oleh;
    }

    public void setDitugaskan_oleh(String ditugaskan_oleh) {
        this.ditugaskan_oleh = ditugaskan_oleh;
    }

    public String getUrl_panduan() {
        return url_panduan;
    }

    public void setUrl_panduan(String url_panduan) {
        this.url_panduan = url_panduan;
    }

    public String getWarepack() {
        return warepack;
    }

    public void setWarepack(String warepack) {
        this.warepack = warepack;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public List<Attachment> getDaftar_lampiran() {
        return daftar_lampiran;
    }

    public void setDaftar_lampiran(List<Attachment> daftar_lampiran) {
        this.daftar_lampiran = daftar_lampiran;
    }

    public List<ToolSafety> getDaftar_perlengkapan_safety() {
        return daftar_perlengkapan_safety;
    }

    public void setDaftar_perlengkapan_safety(List<ToolSafety> daftar_perlengkapan_safety) {
        this.daftar_perlengkapan_safety = daftar_perlengkapan_safety;
    }

    public List<Tool> getDaftar_perlengkapan() {
        return daftar_perlengkapan;
    }

    public void setDaftar_perlengkapan(List<Tool> daftar_perlengkapan) {
        this.daftar_perlengkapan = daftar_perlengkapan;
    }

    public List<Step> getDaftar_langkah() {
        return daftar_langkah;
    }

    public void setDaftar_langkah(List<Step> daftar_langkah) {
        this.daftar_langkah = daftar_langkah;
    }

    public List<MessageLockOut> getDaftar_lockout() {
        return daftar_lockout;
    }

    public void setDaftar_lockout(List<MessageLockOut> daftar_lockout) {
        this.daftar_lockout = daftar_lockout;
    }

    public List<Message> getDaftar_peringatan_bahaya() {
        return daftar_peringatan_bahaya;
    }

    public void setDaftar_peringatan_bahaya(List<Message> daftar_peringatan_bahaya) {
        this.daftar_peringatan_bahaya = daftar_peringatan_bahaya;
    }

    public String getElektronik_id() {
        return elektronik_id;
    }

    public void setElektronik_id(String elektronik_id) {
        this.elektronik_id = elektronik_id;
    }
}
