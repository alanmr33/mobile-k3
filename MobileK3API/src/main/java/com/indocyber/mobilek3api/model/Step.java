package com.indocyber.mobilek3api.model;

import java.util.Date;
import java.util.List;

/**
 * Created by Indocyber on 10/09/2018.
 */
public class Step {
    private String id;
    private int urutan;
    private String nama_pekerjaan;
    private String deskripsi_pekerjaan;
    private String perangkat;
    private Date start_date;
    private Date end_date;
    private String bukti_pekerjaan;
    private Boolean sudah_dikerjakan;
    private List<DetailPekerjaan> gambar;

    public Step() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getUrutan() {
        return urutan;
    }

    public void setUrutan(int urutan) {
        this.urutan = urutan;
    }

    public String getNama_pekerjaan() {
        return nama_pekerjaan;
    }

    public void setNama_pekerjaan(String nama_pekerjaan) {
        this.nama_pekerjaan = nama_pekerjaan;
    }

    public String getDeskripsi_pekerjaan() {
        return deskripsi_pekerjaan;
    }

    public void setDeskripsi_pekerjaan(String deskripsi_pekerjaan) {
        this.deskripsi_pekerjaan = deskripsi_pekerjaan;
    }

    public String getBukti_pekerjaan() {
        return bukti_pekerjaan;
    }

    public void setBukti_pekerjaan(String bukti_pekerjaan) {
        this.bukti_pekerjaan = bukti_pekerjaan;
    }

    public Boolean getSudah_dikerjakan() {
        return sudah_dikerjakan;
    }

    public void setSudah_dikerjakan(Boolean sudah_dikerjakan) {
        this.sudah_dikerjakan = sudah_dikerjakan;
    }

    public List<DetailPekerjaan> getGambar() {
        return gambar;
    }

    public void setGambar(List<DetailPekerjaan> gambar) {
        this.gambar = gambar;
    }

    public String getPerangkat() {
        return perangkat;
    }

    public void setPerangkat(String perangkat) {
        this.perangkat = perangkat;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }
}
