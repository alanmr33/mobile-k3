package com.indocyber.mobilek3api.model;

import java.util.List;

/**
 * Created by Indocyber on 13/09/2018.
 */
public class ChartTotalProsedure {
    private List<Integer> tahun_sekarang;
    private List<Integer> tahun_sebelumnya;

    public ChartTotalProsedure() {
    }

    public List<Integer> getTahun_sekarang() {
        return tahun_sekarang;
    }

    public void setTahun_sekarang(List<Integer> tahun_sekarang) {
        this.tahun_sekarang = tahun_sekarang;
    }

    public List<Integer> getTahun_sebelumnya() {
        return tahun_sebelumnya;
    }

    public void setTahun_sebelumnya(List<Integer> tahun_sebelumnya) {
        this.tahun_sebelumnya = tahun_sebelumnya;
    }
}
