package com.indocyber.mobilek3api.dao;

import com.indocyber.mobilek3api.model.APIResponse;
import com.indocyber.mobilek3api.model.User;

/**
 * Created by Indocyber on 17/09/2018.
 */
public interface UserDao {
    public User getUserByUsername(String username);
    public APIResponse requestNewPassword(String username);
    public APIResponse saveNewPassword(String password,String cpassword,String token);
}
