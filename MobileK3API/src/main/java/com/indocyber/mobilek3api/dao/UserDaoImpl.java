package com.indocyber.mobilek3api.dao;

import com.indocyber.mobilek3api.lib.MD5;
import com.indocyber.mobilek3api.lib.URLBuilder;
import com.indocyber.mobilek3api.model.APIResponse;
import com.indocyber.mobilek3api.model.Role;
import com.indocyber.mobilek3api.model.User;
import com.indocyber.mobilek3api.service.ConfigService;
import com.indocyber.mobilek3api.service.SendMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Indocyber on 17/09/2018.
 */
@Repository
public class UserDaoImpl implements UserDao {
    private JdbcTemplate db;
    private ConfigService configService;
    private SendMailService sender;
    @Autowired
    public void setSendMailService(SendMailService sender) {
        this.sender = sender;
    }
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }
    @Autowired
    public void setConfigService( ConfigService configService){
        this.configService=configService;
    }
    @Override
    public User getUserByUsername(String username) {
        User user=db.query("SELECT\n" +
                "mst_user_hdr.EMPLOYEE_CODE,\n" +
//                "mst_employee.FIRST_NAME,\n" +
//                "mst_employee.MIDDLE_NAME,\n" +
//                "mst_employee.POSITION_CODE,\n" +
//                "mst_employee.EMAIL,\n" +
                "mst_user_hdr.COMPANY_CODE,\n" +
                "mst_user_hdr.`PHOTO`,\n" +
                "mst_user_hdr.`USER_NAME`,\n" +
                "mst_user_hdr.`PASSWORD`\n" +
                "FROM\n" +
                "mst_user_hdr\n" +
//                "INNER JOIN mst_employee ON mst_user_hdr.EMPLOYEE_CODE = mst_employee.EMPLOYEE_CODE " +
                "WHERE EMPLOYEE_CODE='"+username+"' ", new ResultSetExtractor<User>() {
            @Override
            public User extractData(ResultSet userSet) throws SQLException, DataAccessException {
                if (userSet.next()){
                    URLBuilder urlBuilder=new URLBuilder(configService.getConfig().get("APP_URL"));
                    User toReturn=new User();
                    toReturn.setId(userSet.getString("EMPLOYEE_CODE"));
//                    toReturn.setName(userSet.getString("FIRST_NAME")+" "+userSet.getString("FIRST_NAME"));
                    toReturn.setName(userSet.getString("USER_NAME"));
                    toReturn.setLogin(userSet.getString("EMPLOYEE_CODE"));
                    toReturn.setPassword(userSet.getString("PASSWORD"));
                    toReturn.setCompany_code(userSet.getString("COMPANY_CODE"));
                    urlBuilder.setCompany(userSet.getString("COMPANY_CODE"));
                    if(userSet.getBytes("PHOTO")!=null){
                        toReturn.setPhoto_url(urlBuilder.generateImage("pp",toReturn.getId()));
                    }else{
                        toReturn.setPhoto_url(configService.getConfig().get("APP_URL")+"/img/nopic.jpg");
                    }
                    toReturn.setRoles(db.query("SELECT\n" +
                            "mst_user_hdr.EMPLOYEE_CODE,\n" +
                            "mst_role_hdr.ROLE_CODE,\n" +
                            "mst_role_hdr.ROLE_NAME\n" +
                            "FROM\n" +
                            "mst_user_hdr\n" +
                            "INNER JOIN mst_user_dtl ON mst_user_hdr.COMPANY_CODE = mst_user_dtl.COMPANY_CODE AND mst_user_hdr.EMPLOYEE_CODE = mst_user_dtl.EMPLOYEE_CODE\n" +
                            "INNER JOIN mst_role_hdr ON mst_user_dtl.ROLE_CODE = mst_role_hdr.ROLE_CODE AND mst_user_dtl.COMPANY_CODE = mst_role_hdr.COMPANY_CODE\n" +
                            "WHERE mst_user_hdr.EMPLOYEE_CODE='"+userSet.getString("EMPLOYEE_CODE")+"'", new ResultSetExtractor<Set<Role>>() {
                        @Override
                        public Set<Role> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                            Set<Role> roles=new HashSet<>();
                            while (resultSet.next()){
                                Role role=new Role();
                                role.setId(resultSet.getString("ROLE_CODE"));
                                role.setName(resultSet.getString("ROLE_NAME"));
                                roles.add(role);
                            }
                            return roles;
                        }
                    }));
                    return toReturn;
                }
                return null;
            }
        });
        return user;
    }

    @Override
    public APIResponse requestNewPassword(String username) {
        String code= MD5.enc(UUID.randomUUID().toString());
        String empCode=db.query("SELECT mst_user_hdr.EMPLOYEE_CODE \n" +
                        "FROM\n" +
                        "mst_user_hdr\n" +
                        "INNER JOIN mst_employee ON mst_user_hdr.EMPLOYEE_CODE = mst_employee.EMPLOYEE_CODE\n " +
                        "WHERE " +
                        "mst_employee.EMAIL='"+username+"'", new ResultSetExtractor<String>() {
            @Override
            public String extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()){
                    return  resultSet.getString("EMPLOYEE_CODE");
                }
                return null;
            }
        });
        if(empCode!=null){
            String url=configService.getConfig().get("APP_URL")+"forgot_password?code="+code;
            db.update("UPDATE mst_user_hdr SET VERIFICATION_CODE='"+code+"' \n" +
                    "WHERE " +
                    "mst_user_hdr.EMPLOYEE_CODE='"+empCode+"'");
            String body="Klik link berikut untuk reset kata sandi anda "+url+" .";
            sender.send(username,"Safety K3 | Reset Kata Sandi",body);
            APIResponse response=new APIResponse();
            response.setStatus(true);
            response.setMessage("Silahkan cek email anda.");
            return response;
        }else{
            APIResponse response=new APIResponse();
            response.setStatus(true);
            response.setMessage("User tidak ditemukan.");
            return response;
        }
    }

    @Override
    public APIResponse saveNewPassword(String password, String cpassword, String token) {
        db.update("UPDATE mst_user_hdr SET VERIFICATION_CODE='',PASSWORD='"+password+"' \n" +
                "WHERE " +
                "mst_user_hdr.VERIFICATION_CODE='"+token+"'");
        APIResponse response=new APIResponse();
        response.setStatus(true);
        response.setMessage("Kata sandi berhasil diubah");
        return response;
    }
}
