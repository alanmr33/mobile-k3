package com.indocyber.mobilek3api.dao;

import com.indocyber.mobilek3api.model.APIResponse;
import com.indocyber.mobilek3api.model.Assignment;
import com.indocyber.mobilek3api.model.AssignmentSubmit;
import com.indocyber.mobilek3api.model.User;

import java.util.List;

/**
 * Created by Indocyber on 10/09/2018.
 */
public interface AssignmentDao {
    public List<Object> getAssignments(User user);
    public APIResponse saveAssignment(User user,AssignmentSubmit assignment);
}
