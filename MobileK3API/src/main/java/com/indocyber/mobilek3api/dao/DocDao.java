package com.indocyber.mobilek3api.dao;



/**
 * Created by Indocyber on 21/09/2018.
 */
public interface DocDao {
    public byte[] getDocument(String company_code, String id);
}
