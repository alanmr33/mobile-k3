package com.indocyber.mobilek3api.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by Indocyber on 18/09/2018.
 */
@Repository("ConfigDao")
public class ConfigDaoImpl implements ConfigDao {
    private JdbcTemplate db;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }
    @Override
    public HashMap<String, String> getMobileConfig() {
        HashMap<String,String> configs=db.query("SELECT\n" +
                "mst_global_hse.COMPANY_CODE,\n" +
                "mst_global_hse.GLOBAL_CODE,\n" +
                "mst_global_hse.PARM_CODE,\n" +
                "mst_global_hse.PARM_DESC,\n" +
                "mst_global_hse.PARM_VALUE\n" +
                "FROM\n" +
                "mst_global_hse \n" +
                "WHERE \n" +
                "mst_global_hse.COMPANY_CODE='*' AND mst_global_hse.GLOBAL_CODE='MCONF' \n", new ResultSetExtractor<HashMap<String, String>>() {
            @Override
            public HashMap<String, String> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                HashMap<String,String> toReturns=new HashMap<>();
                while (resultSet.next()){
                    toReturns.put(resultSet.getString("PARM_CODE"),
                            resultSet.getString("PARM_DESC"));
                }
                return toReturns;
            }
        });
        return configs;
    }
}
