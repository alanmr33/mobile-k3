package com.indocyber.mobilek3api.dao;



/**
 * Created by Indocyber on 17/09/2018.
 */
public interface ImageDao {
    public byte[] getProfiePicture(String companyCode,String userID);
    public byte[] getProcedureImage(String companyCode,String proID);
    public byte[] getIsolationImage(String companyCode,String procedureCode,String isoID);
    public byte[] getIsolationLocationImage(String companyCode,String procedureCode,String isoID);
    public byte[] getAssetImage(String companyCode,String assetID,String UUID);
    public byte[] getAssetAreaImage(String companyCode,String assetID,String UUID);
    public byte[] getAreaImage(String companyCode,String areaID,String UUID);
    public byte[] getEquipmentImage(String companyCode,String equipmentID,String UUID);
    public byte[] getDeviceImage(String companyCode,String deviceID,String UUID);
    public byte[] getSourceHeaderImage(String companyCode,String source_code,String UUID);
    public byte[] getSourceDetailImage(String companyCode,String source_code,String source_no,String UUID);
}
