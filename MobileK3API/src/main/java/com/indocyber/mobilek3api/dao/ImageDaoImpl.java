package com.indocyber.mobilek3api.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Indocyber on 17/09/2018.
 */
@Repository("ImageDao")
public class ImageDaoImpl implements ImageDao {
    private JdbcTemplate db;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }

    @Override
    public byte[] getProfiePicture(String companyCode, String userID) {
        byte[] image=db.query("SELECT PHOTO FROM mst_user_hdr WHERE " +
                "EMPLOYEE_CODE='"+userID+"' AND COMPANY_CODE='"+companyCode+"'", new ResultSetExtractor<byte[]>() {
            @Override
            public byte[] extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()) {
                    return resultSet.getBytes("PHOTO");
                }else {
                    InputStream imageStream=getClass().getResourceAsStream("/static/img/nopic.jpg");
                    try {
                        byte[] targetArray = new byte[imageStream.available()];
                        imageStream.read(targetArray);
                        return targetArray;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }
        });
        return image;
    }

    @Override
    public byte[] getProcedureImage(String companyCode, String proID) {
        byte[] image=db.query("SELECT ISOLATION_LOCATION_IMAGE FROM mst_procedure WHERE " +
                "PROCEDURE_CODE='"+proID+"' AND COMPANY_CODE='"+companyCode+"'", new ResultSetExtractor<byte[]>() {
            @Override
            public byte[] extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()) {
                    return resultSet.getBytes("ISOLATION_LOCATION_IMAGE");
                }
                return null;
            }
        });
        return image;
    }

    @Override
    public byte[] getIsolationImage(String companyCode,String procedureCode,String isoID) {
        byte[] image=db.query("SELECT ISOLATION_IMAGE_M FROM mst_procedure_isolation WHERE " +
                "ISOLATION_CODE='"+isoID+"' AND COMPANY_CODE='"+companyCode+"' AND PROCEDURE_CODE='"+procedureCode+"'", new ResultSetExtractor<byte[]>() {
            @Override
            public byte[] extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()) {
                    return resultSet.getBytes("ISOLATION_IMAGE_M");
                }
                return null;
            }
        });
        return image;
    }

    @Override
    public byte[] getIsolationLocationImage(String companyCode,String procedureCode,String isoID) {
        byte[] image=db.query("SELECT ISOLATION_LOCATION_IMAGE_M FROM mst_procedure_isolation WHERE " +
                "ISOLATION_CODE='"+isoID+"' AND COMPANY_CODE='"+companyCode+"' AND PROCEDURE_CODE='"+procedureCode+"'", new ResultSetExtractor<byte[]>() {
            @Override
            public byte[] extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()) {
                    return resultSet.getBytes("ISOLATION_LOCATION_IMAGE_M");
                }
                return null;
            }
        });
        return image;
    }

    @Override
    public byte[] getAssetImage(String companyCode,String assetID,String UUID) {
        byte[] image=db.query("SELECT ASSET_IMAGE_M FROM mst_asset_hse WHERE " +
                "ASSET_CODE='"+assetID+"' AND COMPANY_CODE='"+companyCode+"' AND IMAGE_ASSET_NAME='"+UUID+"'", new ResultSetExtractor<byte[]>() {
            @Override
            public byte[] extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()) {
                    return resultSet.getBytes("ASSET_IMAGE_M");
                }
                return null;
            }
        });
        return image;
    }

    @Override
    public byte[] getAssetAreaImage(String companyCode,String assetID,String UUID) {
        byte[] image=db.query("SELECT ASSET_AREA_IMAGE_M FROM mst_asset_hse WHERE " +
                "ASSET_CODE='"+assetID+"' AND COMPANY_CODE='"+companyCode+"' AND IMAGE_ASSET_AREA_NAME='"+UUID+"'", new ResultSetExtractor<byte[]>() {
            @Override
            public byte[] extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()) {
                    return resultSet.getBytes("ASSET_AREA_IMAGE_M");
                }
                return null;
            }
        });
        return image;
    }

    @Override
    public byte[] getAreaImage(String companyCode,String areaID,String UUID) {
        byte[] image=db.query("SELECT AREA_IMAGE FROM mst_area_hse WHERE " +
                "AREA_CODE='"+areaID+"' AND COMPANY_CODE='"+companyCode+"' AND IMAGE_AREA_NAME='"+UUID+"'", new ResultSetExtractor<byte[]>() {
            @Override
            public byte[] extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()) {
                    return resultSet.getBytes("AREA_IMAGE");
                }
                return null;
            }
        });
        return image;
    }

    @Override
    public byte[] getEquipmentImage(String companyCode,String equipmentID,String UUID) {
        byte[] image=db.query("SELECT EQUIP_IMAGE_M FROM mst_equipment WHERE " +
                "EQUIP_CODE='"+equipmentID+"' AND COMPANY_CODE='"+companyCode+"' AND IMAGE_EQUIPMENT_NAME='"+UUID+"'", new ResultSetExtractor<byte[]>() {
            @Override
            public byte[] extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()) {
                    return resultSet.getBytes("EQUIP_IMAGE_M");
                }
                return null;
            }
        });
        return image;
    }

    @Override
    public byte[] getDeviceImage(String companyCode,String deviceID,String UUID) {
        byte[] image=db.query("SELECT DEVICE_IMAGE_M FROM mst_device WHERE " +
                "DEVICE_CODE='"+deviceID+"' AND COMPANY_CODE='"+companyCode+"' AND " +
                "IMAGE_DEVICE_NAME='"+UUID+"'", new ResultSetExtractor<byte[]>() {
            @Override
            public byte[] extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()) {
                    return resultSet.getBytes("DEVICE_IMAGE_M");
                }
                return null;
            }
        });
        return image;
    }

    @Override
    public byte[] getSourceHeaderImage(String companyCode,String source_code,String UUID) {
        byte[] image=db.query("SELECT SOURCE_IMAGE FROM mst_energy_source_hdr WHERE " +
                "SOURCE_CODE='"+source_code+"' AND COMPANY_CODE='"+companyCode+"' AND " +
                "IMAGE_SOURCE_NAME='"+UUID+"'", new ResultSetExtractor<byte[]>() {
            @Override
            public byte[] extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()) {
                    return resultSet.getBytes("SOURCE_IMAGE");
                }
                return null;
            }
        });
        return image;
    }

    @Override
    public byte[] getSourceDetailImage(String companyCode,String source_code,String source_no,String UUID) {
        byte[] image=db.query("SELECT SOURCE_IMAGE FROM mst_energy_source_dtl WHERE " +
                "SOURCE_CODE='"+source_code+"' AND SOURCE_NO='"+source_no+"' AND COMPANY_CODE='"+companyCode+"' AND " +
                "IMAGE_DETAIL_NAME='"+UUID+"'", new ResultSetExtractor<byte[]>() {
            @Override
            public byte[] extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()) {
                    return resultSet.getBytes("SOURCE_IMAGE");
                }
                return null;
            }
        });
        return  image;
    }
}
