package com.indocyber.mobilek3api.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Indocyber on 21/09/2018.
 */
@Repository("DocDao")
public class DocDaoImpl implements DocDao {
    private JdbcTemplate db;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }
    @Override
    public byte[] getDocument(String company_code, String id) {
        byte[] doc=db.query("SELECT PROCEDURE_DOCUMENT FROM mst_procedure WHERE" +
                " PROCEDURE_CODE='"+id+"' AND COMPANY_CODE='"+company_code+"'", new ResultSetExtractor<byte[]>() {
            @Override
            public byte[] extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()){
                    return resultSet.getBytes("PROCEDURE_DOCUMENT");
                }
                return new byte[0];
            }
        });
        return doc;
    }
}
