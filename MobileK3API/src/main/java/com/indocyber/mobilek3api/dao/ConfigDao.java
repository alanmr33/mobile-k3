package com.indocyber.mobilek3api.dao;

import java.util.HashMap;

/**
 * Created by Indocyber on 18/09/2018.
 */
public interface ConfigDao {
    public HashMap<String,String> getMobileConfig();
}
