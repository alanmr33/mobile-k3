package com.indocyber.mobilek3api.dao;

import com.indocyber.mobilek3api.lib.URLBuilder;
import com.indocyber.mobilek3api.model.*;
import com.indocyber.mobilek3api.service.ConfigService;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Indocyber on 10/09/2018.
 */
@Repository("AssignmentDao")
public class AssignmentDaoImpl implements AssignmentDao{
    private JdbcTemplate db;
    private ConfigService configService;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }
    @Autowired
    public void setConfigService( ConfigService configService){
        this.configService=configService;
    }
    @Override
    public List<Object> getAssignments(User user) {
        URLBuilder urlBuilder=new URLBuilder(configService.getConfig().get("APP_URL"));
        SimpleDateFormat dateFormat=new SimpleDateFormat("YYYY-MM-dd");
        System.out.println("SELECT\n" +
                "trx_loto_schedule.SCHEDULE_CODE,\n" +
                "trx_loto_schedule.EVENT_NAME,\n" +
                "mst_procedure.PROCEDURE_CODE,\n" +
                "mst_procedure.PROCEDURE_WARNING,\n" +
                "mst_procedure.PROCEDURE_NAME,\n" +
                "mst_procedure.PROCEDURE_OBJECTIVE,\n" +
                "mst_procedure.ELECTRONIC_ID,\n" +
                "mst_asset_hse.ASSET_CODE,\n" +
                "mst_asset_hse.ASSET_NAME,\n" +
                "mst_asset_hse.AREA_CODE,\n" +
                "trx_loto_schedule.SCHEDULE_STATUS,\n" +
                "trx_loto_schedule.APPROVED_BY,\n" +
                "trx_loto_schedule.ASSIGN_TO,\n" +
                "trx_loto_schedule.START_DATE,\n" +
                "trx_loto_schedule.END_DATE\n" +
                "FROM\n" +
                "trx_loto_schedule\n" +
                "INNER JOIN mst_procedure ON trx_loto_schedule.PROCEDURE_CODE = mst_procedure.PROCEDURE_CODE \n" +
                "AND trx_loto_schedule.COMPANY_CODE = mst_procedure.COMPANY_CODE\n" +
                "LEFT JOIN mst_asset_hse ON mst_procedure.COMPANY_CODE = mst_asset_hse.COMPANY_CODE \n" +
                "AND trx_loto_schedule.ASSET_CODE = mst_asset_hse.ASSET_CODE\n " +
                "WHERE trx_loto_schedule.SCHEDULE_STATUS!='F' AND " +
                "trx_loto_schedule.ASSIGN_TO='"+user.getId()+"' AND trx_loto_schedule.APPROVED_BY IS NOT NULL AND " +
                "trx_loto_schedule.APPROVED_DATE IS NOT NULL AND " +
                "trx_loto_schedule.START_DATE>='"+dateFormat.format(Calendar.getInstance().getTime())+" 00:00:00'");
        List<Object> assignments=db.query("SELECT\n" +
                "trx_loto_schedule.SCHEDULE_CODE,\n" +
                "trx_loto_schedule.EVENT_NAME,\n" +
                "mst_procedure.PROCEDURE_CODE,\n" +
                "mst_procedure.PROCEDURE_WARNING,\n" +
                "mst_procedure.PROCEDURE_NAME,\n" +
                "mst_procedure.PROCEDURE_OBJECTIVE,\n" +
                "mst_procedure.ELECTRONIC_ID,\n" +
                "mst_asset_hse.ASSET_CODE,\n" +
                "mst_asset_hse.ASSET_NAME,\n" +
                "mst_asset_hse.AREA_CODE,\n" +
                "mst_area_hse.AREA_NAME,\n" +
                "trx_loto_schedule.SCHEDULE_STATUS,\n" +
                "trx_loto_schedule.APPROVED_BY,\n" +
                "trx_loto_schedule.ASSIGN_TO,\n" +
                "trx_loto_schedule.START_DATE,\n" +
                "trx_loto_schedule.END_DATE\n" +
                "FROM\n" +
                "trx_loto_schedule\n" +
                "INNER JOIN mst_procedure ON trx_loto_schedule.PROCEDURE_CODE = mst_procedure.PROCEDURE_CODE \n" +
                "AND trx_loto_schedule.COMPANY_CODE = mst_procedure.COMPANY_CODE\n" +
                "LEFT JOIN mst_asset_hse ON mst_procedure.COMPANY_CODE = mst_asset_hse.COMPANY_CODE \n" +
                "AND trx_loto_schedule.ASSET_CODE = mst_asset_hse.ASSET_CODE\n " +
                "LEFT JOIN mst_area_hse ON mst_asset_hse.COMPANY_CODE = mst_area_hse.COMPANY_CODE \n" +
                "AND mst_asset_hse.AREA_CODE = mst_area_hse.AREA_CODE\n " +
                "WHERE trx_loto_schedule.SCHEDULE_STATUS!='F' AND " +
                "trx_loto_schedule.ASSIGN_TO='"+user.getId()+"' AND trx_loto_schedule.APPROVED_BY IS NOT NULL AND " +
                "trx_loto_schedule.APPROVED_DATE IS NOT NULL AND " +
                "trx_loto_schedule.START_DATE>='"+dateFormat.format(Calendar.getInstance().getTime())+" 00:00:00'", new RowMapper<Object>() {
            @Override
            public Assignment mapRow(ResultSet resultSet, int i) throws SQLException {
                String procedure_code=resultSet.getString("PROCEDURE_CODE");
                String schedule_code=resultSet.getString("SCHEDULE_CODE");
                String company_code=user.getCompany_code();
                urlBuilder.setCompany(company_code);
                Assignment assignment=new Assignment();
                assignment.setId(resultSet.getString("SCHEDULE_CODE"));
                assignment.setElektronik_id(resultSet.getString("ELECTRONIC_ID"));
                db.update("UPDATE trx_loto_schedule SET SCHEDULE_STATUS='D' " +
                        "WHERE trx_loto_schedule.SCHEDULE_CODE ='"+resultSet.getString("SCHEDULE_CODE")+"'");
                assignment.setJudul(resultSet.getString("PROCEDURE_NAME"));
                assignment.setInstalasi(resultSet.getString("ASSET_NAME"));
                assignment.setArea(resultSet.getString("AREA_NAME"));
                assignment.setPekerjaan(resultSet.getString("EVENT_NAME"));
                assignment.setGambar(urlBuilder.generateImage("procedure",
                        resultSet.getString("PROCEDURE_CODE")));
                Timestamp timestamp;
                timestamp=resultSet.getTimestamp("START_DATE");
                assignment.setTanggal_assign(new Date(timestamp.getTime()));
                timestamp=resultSet.getTimestamp("START_DATE");
                if(timestamp!=null) {
                    assignment.setTanggal_mulai(new Date(timestamp.getTime()));
                }
                timestamp = resultSet.getTimestamp("END_DATE");
                if(timestamp!=null) {
                    assignment.setTanggal_selesai(new Date(timestamp.getTime()));
                }
                assignment.setStatus("NEW");
                assignment.setKode_asset(resultSet.getString("ASSET_CODE"));
                assignment.setKode_prosedur(resultSet.getString("PROCEDURE_CODE"));
                assignment.setKode_area(resultSet.getString("AREA_CODE"));
                assignment.setDitugaskan_oleh(resultSet.getString("APPROVED_BY"));
                assignment.setUrl_panduan(urlBuilder.generatePdf(resultSet.getString("PROCEDURE_CODE")));
                assignment.setWarepack("");
                assignment.setUser_id(user.getId());
                List<Attachment> attachments=db.query("SELECT\n" +
                        "mst_procedure.COMPANY_CODE,\n" +
                        "mst_procedure.PROCEDURE_CODE,\n" +
                        "mst_procedure.PROCEDURE_NAME,\n" +
                        "mst_asset_hse.ASSET_CODE,\n" +
                        "mst_asset_hse.ASSET_NAME,\n" +
                        "mst_asset_hse.IMAGE_ASSET_NAME,\n" +
                        "mst_asset_hse.IMAGE_ASSET_AREA_NAME,\n" +
                        "mst_area_hse.AREA_CODE,\n" +
                        "mst_area_hse.AREA_NAME\n" +
                        "FROM\n" +
                        "mst_procedure\n" +
                        "INNER JOIN mst_asset_hse ON mst_procedure.COMPANY_CODE = mst_asset_hse.COMPANY_CODE AND mst_procedure.ASSET_CODE = mst_asset_hse.ASSET_CODE\n" +
                        "INNER JOIN mst_area_hse ON mst_asset_hse.COMPANY_CODE = mst_area_hse.COMPANY_CODE AND mst_asset_hse.AREA_CODE = mst_area_hse.AREA_CODE\n " +
                        "WHERE mst_procedure.PROCEDURE_CODE='"+procedure_code+"' AND " +
                        "mst_procedure.COMPANY_CODE='"+company_code+"'", new ResultSetExtractor<List<Attachment>>() {
                    @Override
                    public List<Attachment> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                        //LOCATION
                        ArrayList<Attachment> attachments = new ArrayList<>();
                        if (resultSet.next()) {
                            Attachment attachment = new Attachment();
                            attachment.setId(company_code +"-"+ schedule_code + "-A-"+resultSet.getString("ASSET_CODE")+"-01");
                            attachment.setTitle_gambar(resultSet.getString("ASSET_NAME"));
                            attachment.setSubtitle_gambar("asset");
                            attachment.setUrl_gambar(urlBuilder.generateImage("asset",
                                    resultSet.getString("ASSET_CODE")+"-SPLIT-")+resultSet.getString("IMAGE_ASSET_NAME"));
                            attachments.add(attachment);
                            //AREA LOCATION
                            Attachment attachment2 = new Attachment();
                            attachment2.setId(company_code+"-" + schedule_code + "-A-"+resultSet.getString("ASSET_CODE")+"-02");
                            attachment2.setTitle_gambar(resultSet.getString("ASSET_NAME"));
                            attachment2.setSubtitle_gambar(resultSet.getString("AREA_NAME"));
                            attachment2.setUrl_gambar(urlBuilder.generateImage("asset_area",
                                    resultSet.getString("ASSET_CODE")+"-SPLIT-")+resultSet.getString("IMAGE_ASSET_AREA_NAME"));
                            attachments.add(attachment2);
                        }
                        return attachments;
                    }
                });
                assignment.setDaftar_lampiran(attachments);
                List<ToolSafety> toolSafeties=db.query("SELECT\n" +
                        "mst_equipment.EQUIP_CODE,\n" +
                        "mst_equipment.IMAGE_EQUIPMENT_NAME,\n" +
                        "mst_equipment.EQUIP_NAME\n" +
                        "FROM\n" +
                        "mst_procedure_equipment\n" +
                        "INNER JOIN mst_equipment ON mst_procedure_equipment.COMPANY_CODE = mst_equipment.COMPANY_CODE \n" +
                        "AND mst_procedure_equipment.EQUIP_CODE = mst_equipment.EQUIP_CODE\n" +
                        "WHERE \n" +
                        "PROCEDURE_CODE='"+procedure_code+"' AND " +
                        "mst_equipment.COMPANY_CODE='"+company_code+"'\n", new ResultSetExtractor<List<ToolSafety>>() {
                    @Override
                    public List<ToolSafety> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                        ArrayList<ToolSafety> safeties=new ArrayList<>();
                        while (resultSet.next()){
                           ToolSafety toolSafety=new ToolSafety();
                           toolSafety.setId(company_code +"-"+ schedule_code + "-TS-"+resultSet.getString("EQUIP_CODE"));
                           toolSafety.setNama_perlengkapan(resultSet.getString("EQUIP_NAME"));
                           toolSafety.setUrl_gambar(urlBuilder.generateImage("equipment",
                                   resultSet.getString("EQUIP_CODE")+"-SPLIT-"+resultSet.getString("IMAGE_EQUIPMENT_NAME")));
                           safeties.add(toolSafety);
                        }
                        return safeties;
                    }
                });
                assignment.setDaftar_perlengkapan_safety(toolSafeties);
                List<Tool> tools=db.query("SELECT\n" +
                        "mst_procedure_isolation_device.COMPANY_CODE,\n" +
                        "mst_procedure_isolation_device.PROCEDURE_CODE,\n" +
                        "mst_procedure_isolation_device.ISOLATION_CODE,\n" +
                        "mst_procedure_isolation_device.DEVICE_CODE,\n" +
                        "mst_procedure_isolation_device.DEVICE_NAME,\n" +
                        "mst_device.IMAGE_DEVICE_NAME,\n" +
                        "mst_procedure_isolation_device.DEVICE_KEY,\n" +
                        "mst_procedure_isolation_device.KEY_NO,\n" +
                        "mst_procedure_isolation_device.DEVICE_QTY\n" +
                        "FROM\n" +
                        "mst_procedure_isolation_device\n" +
                        "LEFT JOIN mst_device ON mst_procedure_isolation_device.COMPANY_CODE=mst_device.COMPANY_CODE AND " +
                        "mst_procedure_isolation_device.DEVICE_CODE=mst_device.DEVICE_CODE " +
                        "WHERE\n" +
                        "mst_procedure_isolation_device.COMPANY_CODE='"+company_code+"' AND \n" +
                        "mst_procedure_isolation_device.PROCEDURE_CODE='"+procedure_code+"'", new ResultSetExtractor<List<Tool>>() {
                    @Override
                    public List<Tool> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                        ArrayList<Tool> tools=new ArrayList<>();
                        int a=0;
                        while (resultSet.next()){
                            Tool tool=new Tool();
                            tool.setId(company_code +"-"+ schedule_code + "-T-"+resultSet.getString("DEVICE_CODE")+"-"+a);
                            tool.setNama_perlengkapan(resultSet.getString("DEVICE_NAME"));
                            tool.setUrl_gambar(urlBuilder.generateImage("device",
                                    resultSet.getString("DEVICE_CODE")+"-SPLIT-"+
                                    resultSet.getString("IMAGE_DEVICE_NAME")));
                            if(resultSet.getInt("DEVICE_KEY")==1){
                                tool.setDeskripsi(resultSet.getString("KEY_NO"));
                            }
                            tool.setJumlah(resultSet.getInt("DEVICE_QTY"));
                            tools.add(tool);
                            a++;
                        }
                        return tools;
                    }
                });
                assignment.setDaftar_perlengkapan(tools);
                List<Step> steps=db.query("SELECT\n" +
                        "mst_procedure_isolation.COMPANY_CODE,\n" +
                        "mst_procedure_isolation.PROCEDURE_CODE,\n" +
                        "mst_procedure_isolation.ISOLATION_CODE,\n" +
                        "mst_procedure_isolation.ISOLATION_SEQ,\n" +
                        "mst_procedure_isolation.LOCKOUT_CHECK,\n" +
                        "mst_procedure_isolation.DEVICES_LIST,\n" +
                        "mst_procedure_isolation.ELECTRONIC_ID,\n" +
                        "mst_procedure_isolation.DANGERS_LIST,\n" +
                        "mst_procedure_isolation.ENERGY_SOURCE,\n" +
                        "mst_energy_source_hdr.SOURCE_NAME,\n" +
                        "mst_procedure_isolation.ISOLATION_LOCATION,\n" +
                        "mst_procedure_isolation.LOCKOUT_METHOD,\n" +
                        "mst_device.DEVICE_NAME,\n" +
                        "mst_global_hse.PARM_DESC as DANGER_NAME\n" +
                        "FROM\n" +
                        "mst_procedure_isolation\n" +
                        "LEFT JOIN mst_device ON mst_procedure_isolation.DEVICES_LIST = mst_device.DEVICE_CODE " +
                        "AND mst_procedure_isolation.COMPANY_CODE = mst_device.COMPANY_CODE\n" +
                        "LEFT JOIN mst_energy_source_hdr ON mst_procedure_isolation.ENERGY_SOURCE = mst_energy_source_hdr.SOURCE_CODE " +
                        "AND mst_procedure_isolation.COMPANY_CODE = mst_energy_source_hdr.COMPANY_CODE\n" +
                        "LEFT JOIN mst_global_hse ON mst_procedure_isolation.DANGERS_LIST = mst_global_hse.PARM_CODE AND mst_procedure_isolation.COMPANY_CODE = mst_global_hse.COMPANY_CODE\n" +
                        "WHERE  mst_procedure_isolation.PROCEDURE_CODE='"+procedure_code+"' AND " +
                        "mst_procedure_isolation.COMPANY_CODE='"+company_code+"' " +
                        "ORDER BY\n" +
                        "mst_procedure_isolation.ISOLATION_SEQ ASC\n", new ResultSetExtractor<List<Step>>() {
                    @Override
                    public List<Step> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                        ArrayList<Step> steps=new ArrayList<>();
                        int i = 0;
                        while (resultSet.next()){
                            Step step=new Step();
                            step.setId(company_code +"-"+ schedule_code + "-ST-"+resultSet.getString("ISOLATION_CODE"));
                            step.setUrutan(i+1);
                            //TODO: many key
                            step.setNama_pekerjaan(resultSet.getString("ISOLATION_CODE")+" "
                                    +resultSet.getString("SOURCE_NAME"));
                            String danger=resultSet.getString("DANGER_NAME");
                            if(danger==null){
                                danger="";
                            }
                            step.setDeskripsi_pekerjaan(
                                    resultSet.getString("ISOLATION_LOCATION")+"\n"+
                                    resultSet.getString("LOCKOUT_METHOD")+" \n"+
                                    "Pastikan "+resultSet.getString("LOCKOUT_CHECK")+"."
                                    +"Waspadai bahaya "+danger+" !");
                            step.setSudah_dikerjakan(false);
                            ArrayList<DetailPekerjaan> gambars=new ArrayList<>();
                            DetailPekerjaan detailPekerjaan=new DetailPekerjaan();
                            detailPekerjaan.setId(company_code+"-"+schedule_code+"-"
                                    +resultSet.getString("ISOLATION_CODE")+i+"-D01");
                            detailPekerjaan.setUrl_gambar(urlBuilder.generateImage("isolation",
                                    procedure_code+"-SPLIT-"+resultSet.getString("ISOLATION_CODE")));
                            gambars.add(detailPekerjaan);
                            DetailPekerjaan detailPekerjaan2=new DetailPekerjaan();
                            detailPekerjaan2.setId(company_code+"-"+schedule_code+"-"
                                    +resultSet.getString("ISOLATION_CODE")+i+"-D02");
                            detailPekerjaan2.setUrl_gambar(urlBuilder.generateImage("isolation_location",
                                    procedure_code+"-SPLIT-"+resultSet.getString("ISOLATION_CODE")));
                            gambars.add(detailPekerjaan2);
                            step.setGambar(gambars);
                            String device=resultSet.getString("DEVICE_NAME");
                            if(device==null){
                                device="";
                            }
                            String[] devices=device.split(",");
                            String formated="";
                            for (int a=0;a<devices.length;a++){
                                formated+="Perangkat : <b>"+devices[a]+"</b> <br/> ";
                            }
                            step.setPerangkat(formated);
                            steps.add(step);
                            i++;
                        }
                        return steps;
                    }
                });
                assignment.setDaftar_langkah(steps);
                List<MessageLockOut> lockOuts=db.query("SELECT\n" +
                        "mst_global_hse.PARM_CODE,\n" +
                        "mst_global_hse.PARM_DESC,\n" +
                        "mst_procedure.PROCEDURE_CODE\n" +
                        "FROM\n" +
                        "mst_procedure\n" +
                        "INNER JOIN mst_global_hse ON mst_procedure.COMPANY_CODE = mst_global_hse.COMPANY_CODE \n" +
                        "AND mst_procedure.PROCEDURE_TYPE = mst_global_hse.GLOBAL_CODE\n " +
                        "WHERE mst_procedure.PROCEDURE_CODE='"+procedure_code+"' AND mst_procedure.COMPANY_CODE='"+company_code+"'", new ResultSetExtractor<List<MessageLockOut>>() {
                    @Override
                    public List<MessageLockOut> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                        ArrayList<MessageLockOut> lockOuts=new ArrayList<>();
                        int i=0;
                        while (resultSet.next()){
                            MessageLockOut lockOut=new MessageLockOut();
                            lockOut.setId(company_code +"-"+ schedule_code + "-ML-"+
                                    resultSet.getString("PARM_CODE"));
                            lockOut.setNo(i+1);
                            lockOut.setMessage(resultSet.getString("PARM_DESC"));
                            lockOuts.add(lockOut);
                            i++;
                        }
                        return lockOuts;
                    }
                });
                assignment.setDaftar_lockout(lockOuts);
                ArrayList<Message> messages=new ArrayList<>();
                String warning=resultSet.getString("PROCEDURE_WARNING");
                Message message=new Message();
                message.setId(company_code +"-"+ schedule_code + "-MD-1");
                message.setNo(1);
                message.setMessage(warning);
                messages.add(message);
                assignment.setDaftar_peringatan_bahaya(messages);
                return assignment;
            }
        });
        return assignments;
    }

    @Override
    @Transactional
    public APIResponse saveAssignment(User user,AssignmentSubmit assignment) {
        SimpleDateFormat dateFormat=new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        db.update("UPDATE trx_loto_schedule SET SCHEDULE_STATUS='F', " +
                "ACTUAL_START_DATE='"+assignment.getTanggal_mulai()+"'," +
                "ACTUAL_END_DATE='"+assignment.getTanggal_selesai()+"'," +
                "SCHEDULE_REMARK='Sukses'," +
                "UPDATED_BY='"+user.getName()+"'," +
                "UPDATED_DATE='"+dateFormat.format(Calendar.getInstance().getTime())+"' WHERE " +
                "COMPANY_CODE='"+user.getCompany_code()+"' " +
                "AND SCHEDULE_CODE='"+assignment.getId()+"'");
        int i=1;
        for (StepSubmit stepSubmit:assignment.getStepSubmits()){
            String[] ids=stepSubmit.getId().split("-ST-");
            int finalI = i;
            db.query("SELECT ISOLATION_START_DATE FROM trx_loto_schedule_dtl " +
                    "WHERE  COMPANY_CODE='"+user.getCompany_code()+"' " +
                            "AND SCHEDULE_CODE='"+assignment.getId()+"' AND" +
                            " ISOLATION_CODE='"+ids[1]+"'", new ResultSetExtractor<Object>() {
                @Override
                public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    if(resultSet.next()){
                       db.update("UPDATE trx_loto_schedule_dtl SET " +
                               "ISOLATION_START_DATE=?," +
                               "ISOLATION_END_DATE=?," +
                               "ISOLATION_IMAGE=?," +
                               "UPDATED_BY=?," +
                               "UPDATED_DATE=?" +
                               "WHERE " +
                               "COMPANY_CODE=? AND SCHEDULE_CODE=? AND ISOLATION_CODE=?",
                               stepSubmit.getStart_date(),
                               stepSubmit.getEnd_date(),
                               Base64.decode(stepSubmit.getBukti_pekerjaan()),
                               user.getName(),
                               dateFormat.format(Calendar.getInstance().getTime()),
                               user.getCompany_code(),
                               assignment.getId(),
                               ids[1]);
                    }else{
                        db.update("INSERT INTO trx_loto_schedule_dtl " +
                                        "(COMPANY_CODE," +
                                        "SCHEDULE_CODE," +
                                        "ISOLATION_CODE," +
                                        "ISOLATION_SEQ," +
                                        "ISOLATION_START_DATE," +
                                        "ISOLATION_END_DATE," +
                                        "ISOLATION_IMAGE," +
                                        "CREATED_BY," +
                                        "CREATED_DATE)" +
                                        "VALUES" +
                                        "(?,?,?,?,?,?,?,?,?)",
                                user.getCompany_code(),
                                assignment.getId(),
                                ids[1],
                                finalI,
                                stepSubmit.getStart_date(),
                                stepSubmit.getEnd_date(),
                                Base64.decode(stepSubmit.getBukti_pekerjaan()),
                                user.getName(),
                                dateFormat.format(Calendar.getInstance().getTime())
                        );
                    }
                    return null;
                }
            });
            i++;
        }
        APIResponse apiResponse=new APIResponse();
        apiResponse.setStatus(true);
        apiResponse.setMessage("Data Pekerjaan Berhasil Disimpan");
        return apiResponse;
    }
}
