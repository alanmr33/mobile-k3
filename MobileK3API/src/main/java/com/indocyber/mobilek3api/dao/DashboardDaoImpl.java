package com.indocyber.mobilek3api.dao;

import com.indocyber.mobilek3api.model.ChartKegiatan;
import com.indocyber.mobilek3api.model.ChartTotalProsedure;
import com.indocyber.mobilek3api.model.DashboardTable7Hari;
import com.indocyber.mobilek3api.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Indocyber on 13/09/2018.
 */
@Repository("DashboardDao")
public class DashboardDaoImpl implements DashboardDao {
    private JdbcTemplate db;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }

    @Override
    public List<Object> getChartKegiatan(User user) {
        SimpleDateFormat dateFormat=new SimpleDateFormat("YYYY-MM-dd");
        Calendar start=Calendar.getInstance();
        List<Object> dataKegiatan=db.query("SELECT DISTINCT mst_procedure.PROCEDURE_TYPE,\n" +
                "getDone('"+user.getId()
                +"','"+user.getCompany_code()+"','"+dateFormat.format(start.getTime())+" 00:00:00',mst_procedure.PROCEDURE_TYPE) as  finish,\n" +
                "getUnDone('"+user.getId()
                +"','"+user.getCompany_code()+"','"+dateFormat.format(start.getTime())+" 00:00:00',mst_procedure.PROCEDURE_TYPE) as  onprogress FROM trx_loto_schedule \n" +
                "INNER JOIN mst_procedure ON trx_loto_schedule.COMPANY_CODE = mst_procedure.COMPANY_CODE \n" +
                "AND trx_loto_schedule.PROCEDURE_CODE = mst_procedure.PROCEDURE_CODE \n" +
                "WHERE trx_loto_schedule.ASSIGN_TO='"+user.getId()+"'\n" +
                "AND  trx_loto_schedule.COMPANY_CODE='"+user.getCompany_code()+"' AND \n" +
                "trx_loto_schedule.APPROVED_DATE IS NOT NULL AND \n" +
                "START_DATE>='"+dateFormat.format(start.getTime())+" 00:00:00'", new RowMapper<Object>() {
            @Override
            public ChartKegiatan mapRow(ResultSet resultSet, int i) throws SQLException {
                ChartKegiatan chartKegiatan=new ChartKegiatan();
                chartKegiatan.setAssets(resultSet.getString("PROCEDURE_TYPE"));
                chartKegiatan.setFinish(resultSet.getInt("finish"));
                chartKegiatan.setOnprogress(resultSet.getInt("onprogress"));
                return chartKegiatan;
            }
        });
        return dataKegiatan;
    }

    @Override
    public List<Object> getTable7Harian(User user) {
        SimpleDateFormat dateFormat=new SimpleDateFormat("YYYY-MM-dd");
        Calendar now=Calendar.getInstance();
        Calendar dayAdd7=Calendar.getInstance();
        dayAdd7.add(Calendar.DATE,7);
        List<Object> data=db.query("SELECT\n" +
                "mst_asset_hse.ASSET_NAME AS kegiatan,\n" +
                "DATE_FORMAT(trx_loto_schedule.START_DATE,'%d %b %Y') AS tanggal,\n" +
                "trx_loto_schedule.APPROVED_BY AS ditugaskan_oleh \n" +
                "FROM\n" +
                "trx_loto_schedule\n" +
                "INNER JOIN " +
                "mst_asset_hse ON mst_asset_hse.ASSET_CODE=trx_loto_schedule.ASSET_CODE " +
                "WHERE \n" +
                "ASSIGN_TO='"+user.getId()+"' AND trx_loto_schedule.COMPANY_CODE='"+user.getCompany_code()+"' AND " +
                "SCHEDULE_STATUS!='F' \n" +
                "AND APPROVED_DATE IS NOT NULL AND \n" +
                "(START_DATE BETWEEN '"+dateFormat.format(now.getTime())+" 00:00:00"+"'  AND '"+
                dateFormat.format(dayAdd7.getTime())+" 23:55:55"+"')\n" +
                "ORDER BY START_DATE ASC\n", new RowMapper<Object>() {

            @Override
            public DashboardTable7Hari mapRow(ResultSet resultSet, int i) throws SQLException {
                DashboardTable7Hari dashboardTable7Hari=new DashboardTable7Hari();
                dashboardTable7Hari.setAsset(resultSet.getString("kegiatan"));
                dashboardTable7Hari.setTanggal(resultSet.getString("tanggal"));
                dashboardTable7Hari.setDitugaskan_oleh(resultSet.getString("ditugaskan_oleh"));
                return dashboardTable7Hari;
            }
        });
        return data;
    }

    @Override
    public ChartTotalProsedure getChartTotalProsedure(User user) {
        ChartTotalProsedure chartTotalProsedure=new ChartTotalProsedure();
        Calendar calendar=Calendar.getInstance();
        int year=calendar.get(Calendar.YEAR);
        int lastYear=year-1;
        int nextYear=year+1;
        chartTotalProsedure.setTahun_sekarang(db.query("SELECT \n" +
                "\tgetDSYear('"+year+"-01-01','"+year+"-02-01','"+user.getCompany_code()+"','"+user.getId()+"') as jan,\n" +
                "\tgetDSYear('"+year+"-02-01','"+year+"-03-01','"+user.getCompany_code()+"','"+user.getId()+"') as feb,\n" +
                "\tgetDSYear('"+year+"-03-01','"+year+"-04-01','"+user.getCompany_code()+"','"+user.getId()+"') as mar,\n" +
                "\tgetDSYear('"+year+"-04-01','"+year+"-05-01','"+user.getCompany_code()+"','"+user.getId()+"') as apr,\n" +
                "\tgetDSYear('"+year+"-05-01','"+year+"-06-01','"+user.getCompany_code()+"','"+user.getId()+"') as mei,\n" +
                "\tgetDSYear('"+year+"-06-01','"+year+"-07-01','"+user.getCompany_code()+"','"+user.getId()+"') as jun,\n" +
                "\tgetDSYear('"+year+"-07-01','"+year+"-08-01','"+user.getCompany_code()+"','"+user.getId()+"') as jul,\n" +
                "\tgetDSYear('"+year+"-08-01','"+year+"-09-01','"+user.getCompany_code()+"','"+user.getId()+"') as agus,\n" +
                "\tgetDSYear('"+year+"-09-01','"+year+"-10-01','"+user.getCompany_code()+"','"+user.getId()+"') as sep,\n" +
                "\tgetDSYear('"+year+"-10-01','"+year+"-11-01','"+user.getCompany_code()+"','"+user.getId()+"') as okt,\n" +
                "\tgetDSYear('"+year+"-11-01','"+year+"-12-01','"+user.getCompany_code()+"','"+user.getId()+"') as nov,\n" +
                "\tgetDSYear('"+year+"-12-01','"+nextYear+"-01-01','"+user.getCompany_code()+"','"+user.getId()+"') as des\n", new ResultSetExtractor<List<Integer>>() {
            @Override
            public List<Integer> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                ArrayList<Integer> datas=new ArrayList<>();
                if(resultSet.next()){
                    datas.add(resultSet.getInt("jan"));
                    datas.add(resultSet.getInt("feb"));
                    datas.add(resultSet.getInt("mar"));
                    datas.add(resultSet.getInt("apr"));
                    datas.add(resultSet.getInt("mei"));
                    datas.add(resultSet.getInt("jun"));
                    datas.add(resultSet.getInt("jul"));
                    datas.add(resultSet.getInt("agus"));
                    datas.add(resultSet.getInt("sep"));
                    datas.add(resultSet.getInt("okt"));
                    datas.add(resultSet.getInt("sep"));
                    datas.add(resultSet.getInt("nov"));
                    datas.add(resultSet.getInt("des"));
                }
                return datas;
            }
        }));
        chartTotalProsedure.setTahun_sebelumnya(db.query("SELECT \n" +
                "\tgetDSYear('"+lastYear+"-01-01','"+lastYear+"-02-01','"+user.getCompany_code()+"','"+user.getId()+"') as jan,\n" +
                "\tgetDSYear('"+lastYear+"-02-01','"+lastYear+"-03-01','"+user.getCompany_code()+"','"+user.getId()+"') as feb,\n" +
                "\tgetDSYear('"+lastYear+"-03-01','"+lastYear+"-04-01','"+user.getCompany_code()+"','"+user.getId()+"') as mar,\n" +
                "\tgetDSYear('"+lastYear+"-04-01','"+lastYear+"-05-01','"+user.getCompany_code()+"','"+user.getId()+"') as apr,\n" +
                "\tgetDSYear('"+lastYear+"-05-01','"+lastYear+"-06-01','"+user.getCompany_code()+"','"+user.getId()+"') as mei,\n" +
                "\tgetDSYear('"+lastYear+"-06-01','"+lastYear+"-07-01','"+user.getCompany_code()+"','"+user.getId()+"') as jun,\n" +
                "\tgetDSYear('"+lastYear+"-07-01','"+lastYear+"-08-01','"+user.getCompany_code()+"','"+user.getId()+"') as jul,\n" +
                "\tgetDSYear('"+lastYear+"-08-01','"+lastYear+"-09-01','"+user.getCompany_code()+"','"+user.getId()+"') as agus,\n" +
                "\tgetDSYear('"+lastYear+"-09-01','"+lastYear+"-10-01','"+user.getCompany_code()+"','"+user.getId()+"') as sep,\n" +
                "\tgetDSYear('"+lastYear+"-10-01','"+lastYear+"-11-01','"+user.getCompany_code()+"','"+user.getId()+"') as okt,\n" +
                "\tgetDSYear('"+lastYear+"-11-01','"+lastYear+"-12-01','"+user.getCompany_code()+"','"+user.getId()+"') as nov,\n" +
                "\tgetDSYear('"+lastYear+"-12-01','"+year+"-01-01','"+user.getCompany_code()+"','"+user.getId()+"') as des\n", new ResultSetExtractor<List<Integer>>() {
            @Override
            public List<Integer> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                ArrayList<Integer> datas=new ArrayList<>();
                if(resultSet.next()){
                    datas.add(resultSet.getInt("jan"));
                    datas.add(resultSet.getInt("feb"));
                    datas.add(resultSet.getInt("mar"));
                    datas.add(resultSet.getInt("apr"));
                    datas.add(resultSet.getInt("mei"));
                    datas.add(resultSet.getInt("jun"));
                    datas.add(resultSet.getInt("jul"));
                    datas.add(resultSet.getInt("agus"));
                    datas.add(resultSet.getInt("sep"));
                    datas.add(resultSet.getInt("okt"));
                    datas.add(resultSet.getInt("sep"));
                    datas.add(resultSet.getInt("nov"));
                    datas.add(resultSet.getInt("des"));
                }
                return datas;
            }
        }));
        return chartTotalProsedure;
    }
}
