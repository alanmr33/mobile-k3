package com.indocyber.mobilek3api.dao;

import com.indocyber.mobilek3api.model.ChartTotalProsedure;
import com.indocyber.mobilek3api.model.User;

import java.util.List;

/**
 * Created by Indocyber on 13/09/2018.
 */
public interface DashboardDao {
    public List<Object> getChartKegiatan(User user);
    public List<Object> getTable7Harian(User user);
    public ChartTotalProsedure getChartTotalProsedure(User user);
}
