package com.indocyber.mobilek3api.lib;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;

/**
 * Created by Indocyber on 24/09/2018.
 */
public abstract class MD5 {
    public static String enc(java.lang.String text){
        try {
            return DigestUtils.md5Hex(text.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
