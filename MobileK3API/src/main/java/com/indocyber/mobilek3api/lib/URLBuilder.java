package com.indocyber.mobilek3api.lib;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Indocyber on 19/09/2018.
 */
public class URLBuilder {
    private String base="http://127.0.0.1:8080";
    private String company="";
    public URLBuilder(String base) {
        this.base = base;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String generateImage(String image, String id){
        try {
            return base+"/image?company_code="+ URLEncoder.encode(company,"UTF-8")+"&image_name="+URLEncoder.encode(image,"UTF-8")
                    +"&id="+URLEncoder.encode(id,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return base+"/image?company_code="+ company+"&image_name="+image
                +"&id="+id;
    }
    public String generatePdf(String id){
        return base+"/pdf?company_code="+company+"&id="+id;
    }
}
