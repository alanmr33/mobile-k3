package com.indocyber.mobilek3api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Indocyber on 24/09/2018.
 */
@Controller
public class ForgotPasswordController {

    @RequestMapping("/forgot_password")
    public String indexPassword(@RequestParam String code,
                                Model model) {
        model.addAttribute("code",code);
        return "lupa_password";
    }
}
