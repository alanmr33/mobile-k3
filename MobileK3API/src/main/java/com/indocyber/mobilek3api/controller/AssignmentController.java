package com.indocyber.mobilek3api.controller;

import com.indocyber.mobilek3api.model.*;
import com.indocyber.mobilek3api.service.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created by Indocyber on 10/09/2018.
 */
@RestController
public class AssignmentController {
    @Autowired
    private AssignmentService assignmentService;
    @RequestMapping("/assignment/get")
    public APIResponse getAssignment(@AuthenticationPrincipal User user) {
        return assignmentService.getAssignments(user);
    }
    @RequestMapping(value = "/assignment/save",method = RequestMethod.POST)
    public APIResponse saveData(@AuthenticationPrincipal User user,@RequestBody AssignmentSubmit assignment) {
        return assignmentService.saveAssignment(user,assignment);
    }
}
