package com.indocyber.mobilek3api.controller;

import com.indocyber.mobilek3api.model.User;
import com.indocyber.mobilek3api.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Indocyber on 12/09/2018.
 */
@Controller
public class DashboardController {
    @RequestMapping("/dashboard")
    public String dashboard(@AuthenticationPrincipal User user,
                            @RequestHeader(value="Authorization") String Authorization,
                            Model model) {
        model.addAttribute("Authorization",Authorization);
        return "dashboard";
    }
}
