package com.indocyber.mobilek3api.controller;

import com.indocyber.mobilek3api.model.DashboardResponse;
import com.indocyber.mobilek3api.model.User;
import com.indocyber.mobilek3api.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Indocyber on 13/09/2018.
 */
@RestController
public class DashboardAjaxController {
    @Autowired
    private DashboardService dashboardService;
    @RequestMapping("/dashboard/ajax")
    public DashboardResponse dashboardAjax(@AuthenticationPrincipal User user) {
        return dashboardService.generateDashboard(user);
    }
}
