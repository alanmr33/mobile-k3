package com.indocyber.mobilek3api.controller;

import com.indocyber.mobilek3api.service.DocService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Indocyber on 21/09/2018.
 */
@RestController
public class DocumentController {
    @Autowired
    private DocService docService;
    @RequestMapping("/pdf")
    public ResponseEntity<byte[]> getImage(
            @RequestParam String company_code,
            @RequestParam String id) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type","application/pdf");
        responseHeaders.set("Pragma","public");
        responseHeaders.set("Cache-Control","max-age=86400");
        return ResponseEntity
                .ok()
                .headers(responseHeaders).body(docService.getDocument(company_code,id));
    }
}
