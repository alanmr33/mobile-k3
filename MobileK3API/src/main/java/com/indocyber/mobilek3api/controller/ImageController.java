package com.indocyber.mobilek3api.controller;

import com.indocyber.mobilek3api.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by Indocyber on 17/09/2018.
 */
@RestController
public class ImageController {
    @Autowired
    private ImageService imageService;
    @RequestMapping("/image")
    public ResponseEntity<byte[]> getImage(
                                           @RequestParam String company_code,
                                           @RequestParam String image_name,
                                           @RequestParam String id) {
        try {
            company_code= URLDecoder.decode(company_code,"UTF-8");
            image_name= URLDecoder.decode(image_name,"UTF-8");
            id= URLDecoder.decode(id,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type","image/jpeg");
        responseHeaders.set("Pragma","public");
        responseHeaders.set("Cache-Control","max-age=86400");
        return ResponseEntity
                .ok()
                .headers(responseHeaders)
                .body(imageService
                        .getImage(company_code,image_name,id));
    }
}
