package com.indocyber.mobilek3api.controller;
import com.indocyber.mobilek3api.model.APIResponse;
import com.indocyber.mobilek3api.model.PublicUser;
import com.indocyber.mobilek3api.model.User;
import com.indocyber.mobilek3api.service.ForgotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
	@Autowired
	private ForgotService forgotService;
	@RequestMapping("/me")
	public PublicUser getProfile(@AuthenticationPrincipal User user) {
		return new PublicUser(user);
	}
	@RequestMapping(value ="/request_forgot",method = RequestMethod.POST)
	public APIResponse requestForgot(@RequestParam String username) {
		return forgotService.requestNewPassword(username);
	}
	@RequestMapping(value = "/submit_forgot",method = RequestMethod.POST)
	public APIResponse submitForgot(@RequestParam String password,
									@RequestParam String cpassword,
									@RequestParam String token) {
		return forgotService.saveNewPassword(password,cpassword,token);
	}
}
