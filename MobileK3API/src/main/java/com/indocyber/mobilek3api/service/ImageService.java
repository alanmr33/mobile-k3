package com.indocyber.mobilek3api.service;


/**
 * Created by Indocyber on 17/09/2018.
 */
public interface ImageService {
    public byte[] getImage(String company_code, String image_name, String id);
}
