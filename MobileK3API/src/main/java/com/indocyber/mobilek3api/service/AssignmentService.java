package com.indocyber.mobilek3api.service;

import com.indocyber.mobilek3api.model.APIResponse;
import com.indocyber.mobilek3api.model.Assignment;
import com.indocyber.mobilek3api.model.AssignmentSubmit;
import com.indocyber.mobilek3api.model.User;

import java.util.ArrayList;

/**
 * Created by Indocyber on 10/09/2018.
 */
public interface AssignmentService {
    public APIResponse getAssignments(User user);
    public APIResponse saveAssignment(User user,AssignmentSubmit assignment);
}
