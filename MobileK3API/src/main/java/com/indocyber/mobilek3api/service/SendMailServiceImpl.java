package com.indocyber.mobilek3api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 * Created by Indocyber on 24/09/2018.
 */
@Service
public class SendMailServiceImpl implements SendMailService {
    @Autowired
    private JavaMailSender sender;
    @Override
    public void send(String to, String title, String body) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(title);
        message.setText(body);
        sender.send(message);
    }
}
