package com.indocyber.mobilek3api.service;

/**
 * Created by Indocyber on 21/09/2018.
 */
public interface DocService {
    public byte[] getDocument(String company_code, String id);
}
