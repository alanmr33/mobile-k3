package com.indocyber.mobilek3api.service;

import com.indocyber.mobilek3api.dao.AssignmentDao;
import com.indocyber.mobilek3api.model.APIResponse;
import com.indocyber.mobilek3api.model.AssignmentSubmit;
import com.indocyber.mobilek3api.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by Indocyber on 10/09/2018.
 */
@Service
public class AssignmentServiceImpl implements AssignmentService {
    @Autowired
    private AssignmentDao assignmentDao;
    @Override
    public APIResponse getAssignments(User user) {
        APIResponse apiResponse=new APIResponse(true,"assignment found",assignmentDao.getAssignments(user));
        return apiResponse;
    }

    @Override
    public APIResponse saveAssignment(User user,AssignmentSubmit assignment) {
        return assignmentDao.saveAssignment(user,assignment);
    }
}
