package com.indocyber.mobilek3api.service;

import com.indocyber.mobilek3api.model.DashboardResponse;
import com.indocyber.mobilek3api.model.User;

/**
 * Created by Indocyber on 13/09/2018.
 */
public interface DashboardService {
    public DashboardResponse generateDashboard(User user);
}
