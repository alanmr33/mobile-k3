package com.indocyber.mobilek3api.service;

import com.indocyber.mobilek3api.dao.ImageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Indocyber on 17/09/2018.
 */
@Service
public class ImageServiceImpl implements ImageService {
    @Autowired
    private ImageDao imageDao;

    @Override
    public byte[] getImage(String company_code, String image_name, String id) {
        byte[] image = new byte[0];
        switch (image_name){
            default:
                break;
            case "procedure" :
                image=imageDao.getProcedureImage(company_code,id);
                break;
            case "pp" :
                image=imageDao.getProfiePicture(company_code,id);
                break;
            case "area" :
                String[] ids6=id.split("-SPLIT-");
                image=imageDao.getAreaImage(company_code,ids6[0],ids6[1]);
                break;
            case "isolation" :
                String[] ids=id.split("-SPLIT-");
                image=imageDao.getIsolationImage(company_code,ids[0],ids[1]);
                break;
            case "isolation_location" :
                String[] ids2=id.split("-SPLIT-");
                image=imageDao.getIsolationLocationImage(company_code,ids2[0],ids2[1]);
                break;
            case "asset" :
                String[] ids3=id.split("-SPLIT-");
                image=imageDao.getAssetImage(company_code,ids3[0],ids3[1]);
                break;
            case "asset_area" :
                String[] ids4=id.split("-SPLIT-");
                image=imageDao.getAssetAreaImage(company_code,ids4[0],ids4[1]);
                break;
            case "equipment" :
                String[] ids5=id.split("-SPLIT-");
                image=imageDao.getEquipmentImage(company_code,ids5[0],ids5[1]);
                break;
            case "device"    :
                String[] ids7=id.split("-SPLIT-");
                image=imageDao.getDeviceImage(company_code,ids7[0],ids7[1]);
                break;
            case "source_header"     :
                String[] ids8=id.split("-SPLIT-");
                image=imageDao.getSourceHeaderImage(company_code,ids8[0],ids8[1]);
                break;
            case  "source_detail"     :
                String[] ids0=id.split("-SPLIT-");
                image=imageDao.getSourceDetailImage(company_code,ids0[0],ids0[1],ids0[2]);
                break;
        }
        return image;
    }
}
