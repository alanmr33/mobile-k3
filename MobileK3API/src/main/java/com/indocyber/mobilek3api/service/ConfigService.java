package com.indocyber.mobilek3api.service;


import java.util.HashMap;

/**
 * Created by Indocyber on 18/09/2018.
 */
public interface ConfigService {
    public HashMap<String,String> getConfig();
}
