package com.indocyber.mobilek3api.service;

import com.indocyber.mobilek3api.dao.DocDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Indocyber on 21/09/2018.
 */
@Service
public class DocDaoImpl implements DocService {
    @Autowired
    private DocDao docDao;

    @Override
    public byte[] getDocument(String company_code, String id) {
        return docDao.getDocument(company_code,id);
    }
}
