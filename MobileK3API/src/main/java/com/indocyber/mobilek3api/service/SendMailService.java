package com.indocyber.mobilek3api.service;



/**
 * Created by Indocyber on 24/09/2018.
 */
public interface SendMailService {
   public void send(String to, String title, String body);
}
