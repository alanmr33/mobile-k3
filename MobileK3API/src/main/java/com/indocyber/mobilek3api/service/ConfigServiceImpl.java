package com.indocyber.mobilek3api.service;

import com.indocyber.mobilek3api.dao.ConfigDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * Created by Indocyber on 18/09/2018.
 */
@Service
public class ConfigServiceImpl  implements ConfigService {
    @Autowired
    private ConfigDao configDao;
    @Override
    public HashMap<String, String> getConfig() {
        return configDao.getMobileConfig();
    }
}
