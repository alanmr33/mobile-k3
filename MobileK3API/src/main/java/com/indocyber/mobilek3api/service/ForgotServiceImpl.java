package com.indocyber.mobilek3api.service;

import com.indocyber.mobilek3api.dao.UserDao;
import com.indocyber.mobilek3api.model.APIResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Indocyber on 24/09/2018.
 */
@Service
public class ForgotServiceImpl implements ForgotService{
    @Autowired
    private UserDao userDao;
    @Override
    public APIResponse requestNewPassword(String username) {
        return userDao.requestNewPassword(username);
    }

    @Override
    public APIResponse saveNewPassword(String password, String cpassword, String token) {
        return userDao.saveNewPassword(password,cpassword,token);
    }
}
