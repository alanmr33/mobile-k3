package com.indocyber.mobilek3api.service;

import com.indocyber.mobilek3api.model.APIResponse;

/**
 * Created by Indocyber on 24/09/2018.
 */
public interface ForgotService {
    public APIResponse requestNewPassword(String username);
    public APIResponse saveNewPassword(String password,String cpassword,String token);
}
