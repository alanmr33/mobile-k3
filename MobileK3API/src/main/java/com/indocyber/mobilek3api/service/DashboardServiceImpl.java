package com.indocyber.mobilek3api.service;

import com.indocyber.mobilek3api.dao.DashboardDao;
import com.indocyber.mobilek3api.model.DashboardResponse;
import com.indocyber.mobilek3api.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Indocyber on 13/09/2018.
 */
@Service
public class DashboardServiceImpl implements DashboardService {
    @Autowired
    private DashboardDao dashboardDao;
    @Override
    public DashboardResponse generateDashboard(User user) {
        DashboardResponse response=new DashboardResponse();
        response.setChartKegiatan(dashboardDao.getChartKegiatan(user));
        response.setTable7Harian(dashboardDao.getTable7Harian(user));
        response.setTotalProsedure(dashboardDao.getChartTotalProsedure(user));
        return response;
    }
}
