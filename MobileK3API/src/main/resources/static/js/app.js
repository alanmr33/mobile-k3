/**
 * Created by Indocyber on 13/09/2018.
 */
window.onerror = function(msg, url, linenumber) {
    alert('Error message: '+msg+'\nURL: '+url+'\nLine Number: '+linenumber);
    return true;
}
$.ajax({
    url : 'dashboard/ajax',
    beforeSend: function(xhr){
        xhr.setRequestHeader('Authorization',$('#token').html());
    },
    success : function (response) {
        var kegiatan=[];
        var kegiatanHTML="";
        if (response.chartKegiatan!=null) {
            $.each(response.chartKegiatan, function (k, v) {
                kegiatan[k] = [v.finish, v.onprogress];
                kegiatanHTML+='<div class="col-4"> ' +
                    '<h6 id="asset'+k+'">'+v.assets+'</h6> ' +
                    '<canvas id="kegiatan'+k+'" style="height: 70px;width: 100%;"></canvas> ' +
                    '<h6 id="finish'+k+'">Terselesaikan : '+v.finish+'</h6> ' +
                    '</div>'
            });
            $('#kegiatan').html(kegiatanHTML);
            var data = {};
            for (var i = 0; i < response.chartKegiatan.length; i++) {
                if (typeof  kegiatan[i] != "undefined") {
                    data = {
                        datasets: [{
                            data: kegiatan[i],
                            backgroundColor: [
                                '#00a9b9',
                                '#eeeeee'
                            ]
                        }],
                        labels: [
                            'Selesai',
                            'On Progress'
                        ]
                    };
                    // And for a doughnut chart
                    var kegiatan1 = document.getElementById("kegiatan" + i);
                    var chart = new Chart(kegiatan1, {
                        type: 'doughnut',
                        data: data,
                        options: {
                            responsive: true,
                            title: {
                                display: false
                            },
                            legend: {
                                display: false
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            }
                        }
                    });
                }
            }
        }
        var html="";
        if (response.table7Harian!=null) {
            for (var a = 0; a < response.table7Harian.length; a++) {
                html += "<tr>" +
                    "<td>" + response.table7Harian[a].asset + "</td>" +
                    "<td>" + response.table7Harian[a].tanggal + "</td>" +
                    "<td>" + response.table7Harian[a].ditugaskan_oleh + "</td>" +
                    "</tr>";
            }
        }
        $("#data7hari").html(html);
        var config = {
            type: 'line',
            data: {
                labels: ['January', 'February', 'March'],
                datasets: [{
                    label: 'Tahun Sekarang',
                    borderColor: '#00b3ed',
                    backgroundColor:  '#00b3ed',
                    data: [
                        10,
                        2,
                        10
                    ],
                }, {
                    label: 'Tahun Sebelumnya',
                    borderColor: '#0261ed',
                    backgroundColor: '#0261ed',
                    data: [
                        20,
                        20,
                        5
                    ],
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: false,
                },
                tooltips: {
                    mode: 'index',
                },
                hover: {
                    mode: 'index'
                },
                scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        stacked: false,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        }
                    }]
                }
            }
        };
        var kegiatantahun = document.getElementById("kegiatantahun");
        var chart4= new Chart(kegiatantahun, config);
    }
});