/**
 * Created by Indocyber on 24/09/2018.
 */
$(document).ready(function () {
    $('#reset-password').on('click',function (e) {
        var password=$('#password').val();
        var cpassword=$('#cpassword').val();
        if (password==''){
            swal('Safety K3','Isi kata sandi terlebih dahulu');
            return;
        }
        if (cpassword!=password){
            swal('Safety K3','Kata sandi tidak sama');
            return;
        }
        $.ajax({
            url : '../../submit_forgot',
            data : {
                password : $('#password').val(),
                cpassword : $('#cpassword').val(),
                token : $('#code').val()
            },
            type : 'POST',
            success : function (response) {
                if (response.status){
                    swal('Safety K3','Kata sandi berhasil diubah','success').then(function (value) {
                        location.href='/'
                    });
                }else{
                    swal('Safety K3','Kata sandi gagal diubah','danger');
                }
            },
            failure : function (error) {
                swal('Safety K3','Cek koneksi anda');
            }
        })
    });
});