/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : mobile_k3

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-09-12 16:01:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mobile_assignments
-- ----------------------------
DROP TABLE IF EXISTS `mobile_assignments`;
CREATE TABLE `mobile_assignments` (
  `id` varchar(32) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `instalasi` varchar(30) NOT NULL,
  `pekerjaan` varchar(30) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `tanggal_assign` datetime NOT NULL,
  `tanggal_mulai` datetime DEFAULT NULL,
  `tanggal_selesai` datetime DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `kode_asset` varchar(16) DEFAULT NULL,
  `kode_prosedur` varchar(16) DEFAULT NULL,
  `kode_area` varchar(10) DEFAULT NULL,
  `ditugaskan_oleh` varchar(50) DEFAULT NULL,
  `url_panduan` varchar(255) DEFAULT NULL,
  `warepack` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mobile_assignments
-- ----------------------------
INSERT INTO `mobile_assignments` VALUES ('1', 'Proin bibendum et dui in mattis.', 'Lorem', 'Buka Kunci', 'https://picsum.photos/300/300/?random', '2018-09-12 10:35:16', null, null, 'DOWNLOADED', 'AS01', 'P01', 'AR01', 'John Doe', 'https://nimoz.pl/files/publications/5/loremipsum.pdf', null, '2');
INSERT INTO `mobile_assignments` VALUES ('2', 'Proin bibendum et dui in mattis.', 'Lorem', 'Tarik Tuas', 'https://picsum.photos/300/300/?image=160', '2018-09-12 14:35:16', null, null, 'DOWNLOADED', 'AS01', 'P01', 'AR01', 'John Doe', 'https://nimoz.pl/files/publications/5/loremipsum.pdf', '', '2');

-- ----------------------------
-- Table structure for mobile_attachments
-- ----------------------------
DROP TABLE IF EXISTS `mobile_attachments`;
CREATE TABLE `mobile_attachments` (
  `id` varchar(32) NOT NULL,
  `url_gambar` varchar(100) DEFAULT NULL,
  `title_gambar` varchar(100) DEFAULT NULL,
  `subtitle_gambar` varchar(100) DEFAULT NULL,
  `assignment_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mobile_attachments
-- ----------------------------
INSERT INTO `mobile_attachments` VALUES ('1', 'https://picsum.photos/300/300/?image=300', 'Sinatus Beneptus', 'Lorem Ipsum', '1');
INSERT INTO `mobile_attachments` VALUES ('2', 'https://picsum.photos/300/300/?image=302', 'Lakarias', 'Dolor', '1');
INSERT INTO `mobile_attachments` VALUES ('3', 'https://picsum.photos/300/300/?image=300', 'Sinatus Beneptus', 'Lorem Ipsum', '2');
INSERT INTO `mobile_attachments` VALUES ('4', 'https://picsum.photos/300/300/?image=302', 'Lakarias', 'Dolor', '2');

-- ----------------------------
-- Table structure for mobile_prosedure_messages
-- ----------------------------
DROP TABLE IF EXISTS `mobile_prosedure_messages`;
CREATE TABLE `mobile_prosedure_messages` (
  `id` varchar(32) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `message` longtext,
  `is_lockout` bit(1) DEFAULT NULL,
  `assignment_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mobile_prosedure_messages
-- ----------------------------
INSERT INTO `mobile_prosedure_messages` VALUES ('1', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ac ligula quis sem cursus semper a at nibh. Donec volutpat leo eget diam condimentum auctor. Curabitur vel dictum libero. Aliquam sed quam vehicula, venenatis erat sit amet, ornare mi. Nunc sit amet orci eu odio rutrum imperdiet. ', '\0', '1');
INSERT INTO `mobile_prosedure_messages` VALUES ('2', '2', 'Etiam tristique efficitur metus, et egestas massa interdum cursus. Proin convallis nunc id augue tincidunt, eu ornare mauris rutrum. ', '\0', '1');
INSERT INTO `mobile_prosedure_messages` VALUES ('4', '1', 'Etiam tristique efficitur metus, et egestas massa interdum cursus. Proin convallis nunc id augue tincidunt, eu ornare mauris rutrum. ', '', '1');
INSERT INTO `mobile_prosedure_messages` VALUES ('5', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ac ligula quis sem cursus semper a at nibh. Donec volutpat leo eget diam condimentum auctor. Curabitur vel dictum libero. Aliquam sed quam vehicula, venenatis erat sit amet, ornare mi. Nunc sit amet orci eu odio rutrum imperdiet. ', '', '2');
INSERT INTO `mobile_prosedure_messages` VALUES ('6', '2', 'Etiam tristique efficitur metus, et egestas massa interdum cursus. Proin convallis nunc id augue tincidunt, eu ornare mauris rutrum. ', '\0', '2');

-- ----------------------------
-- Table structure for mobile_safety_tools
-- ----------------------------
DROP TABLE IF EXISTS `mobile_safety_tools`;
CREATE TABLE `mobile_safety_tools` (
  `id` varchar(32) NOT NULL,
  `nama_perlengkapan` varchar(100) DEFAULT NULL,
  `dipakai` bit(1) DEFAULT NULL,
  `url_gambar` varchar(100) DEFAULT NULL,
  `assignment_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mobile_safety_tools
-- ----------------------------
INSERT INTO `mobile_safety_tools` VALUES ('1', 'Tools 1', null, 'https://picsum.photos/300/300/?image=200', '1');
INSERT INTO `mobile_safety_tools` VALUES ('2', 'Tools 2', null, 'https://picsum.photos/300/300/?image=100', '1');
INSERT INTO `mobile_safety_tools` VALUES ('3', 'Tools 3', null, 'https://picsum.photos/300/300/?image=455', '1');
INSERT INTO `mobile_safety_tools` VALUES ('4', 'Tools 1', null, 'https://picsum.photos/300/300/?image=201', '2');

-- ----------------------------
-- Table structure for mobile_steps
-- ----------------------------
DROP TABLE IF EXISTS `mobile_steps`;
CREATE TABLE `mobile_steps` (
  `id` varchar(32) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `nama_pekerjaan` varchar(255) DEFAULT NULL,
  `perangkat` varchar(50) DEFAULT NULL,
  `nomor_kunci` varchar(16) DEFAULT NULL,
  `deskripsi_pekerjaan` longtext,
  `bukti_pekerjaan` varchar(100) DEFAULT NULL,
  `sudah_dikerjakan` bit(1) DEFAULT NULL,
  `assignment_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mobile_steps
-- ----------------------------
INSERT INTO `mobile_steps` VALUES ('1', '1', 'Maecenas ultrices velit mi', 'Kunci', 'K1', 'Suspendisse et magna eros. Phasellus non ligula a dui pellentesque tempor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent pellentesque eu dui non dignissim.', null, '\0', '1');
INSERT INTO `mobile_steps` VALUES ('2', '2', 'Aenean bibendum eros metus', 'Box', 'K2', 'enean purus sem, consectetur non urna vel, luctus blandit mi.', null, '\0', '1');
INSERT INTO `mobile_steps` VALUES ('3', '1', 'Maecenas ultrices velit mi', 'Sekring', 'K3', 'Suspendisse et magna eros. Phasellus non ligula a dui pellentesque tempor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent pellentesque eu dui non dignissim.', null, '\0', '2');

-- ----------------------------
-- Table structure for mobile_step_attachments
-- ----------------------------
DROP TABLE IF EXISTS `mobile_step_attachments`;
CREATE TABLE `mobile_step_attachments` (
  `id` varchar(32) NOT NULL,
  `url_gambar` varchar(100) DEFAULT NULL,
  `step_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mobile_step_attachments
-- ----------------------------
INSERT INTO `mobile_step_attachments` VALUES ('1', 'https://picsum.photos/300/300/?image=301', '1');
INSERT INTO `mobile_step_attachments` VALUES ('2', 'https://picsum.photos/300/300/?image=123', '1');
INSERT INTO `mobile_step_attachments` VALUES ('3', 'https://picsum.photos/300/300/?image=156', '1');
INSERT INTO `mobile_step_attachments` VALUES ('4', 'https://picsum.photos/300/300/?image=378', '1');
INSERT INTO `mobile_step_attachments` VALUES ('5', 'https://picsum.photos/300/300/?image=455', '2');
INSERT INTO `mobile_step_attachments` VALUES ('6', 'https://picsum.photos/300/300/?image=400', '2');
INSERT INTO `mobile_step_attachments` VALUES ('7', 'https://picsum.photos/300/300/?image=400', '3');

-- ----------------------------
-- Table structure for mobile_tools
-- ----------------------------
DROP TABLE IF EXISTS `mobile_tools`;
CREATE TABLE `mobile_tools` (
  `id` varchar(32) NOT NULL,
  `nama_perlengkapan` varchar(100) DEFAULT NULL,
  `digunakan` bit(1) DEFAULT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `url_gambar` varchar(100) DEFAULT NULL,
  `assignment_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mobile_tools
-- ----------------------------
INSERT INTO `mobile_tools` VALUES ('1', 'Maecenas  1', null, 'Suspendisse et magna eros. ', 'https://picsum.photos/300/300/?image=199', '1');
INSERT INTO `mobile_tools` VALUES ('2', 'Maecenas  1', null, 'Suspendisse et magna eros. ', 'https://picsum.photos/300/300/?image=187', '1');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'ROLE_USER');
INSERT INTO `role` VALUES ('2', 'ROLE_ADMIN');
INSERT INTO `role` VALUES ('3', 'ROLE_GUEST');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `login` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `imei` varchar(255) DEFAULT NULL,
  `photo_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'Admin', 'admin', '1234', null, null);
INSERT INTO `user` VALUES ('2', 'User', 'user', '1234', null, 'https://picsum.photos/300/300/?image=108');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('1', '1');
INSERT INTO `user_role` VALUES ('1', '2');
INSERT INTO `user_role` VALUES ('2', '1');
